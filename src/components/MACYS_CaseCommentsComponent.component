<!-- ******************************************************************************************
* Create By     :     Deloitte Consulting.
* Create Date   :     8/23/2016         
* Description   :     Component used in MACYS_CaseCommentsRelatedList page.

*     Modification Log:
*    -----------------------------------------------------------------------------
*    * Developer                   Date               Description        Modification ID
*    * ----------------------------------------------------------------------------                 
*    * Puneet Jalota              8/23/2016           Initial version.    M-001
******************************************************************************************-->
<apex:component controller="MACYS_CaseCommentsComponentController" allowDML="true">
    <head>
        <link rel="stylesheet" href="{!URLFOR($Resource.MACYS_BootStrapCSS)}" />
        <link rel="stylesheet" media="print" href="{!URLFOR($Resource.MACYS_BootStrapCSS)}" />
        <script src="{!URLFOR($Resource.MACYS_JQUERYJS)}"></script>
        <script src="{!URLFOR($Resource.MACYS_BootStrapJS)}"></script>
    </head>
    <!-- Attribute Definition -->
    <apex:attribute name="CurrentCaseId" description="Salesforce Id of the Case whose Case Comments needs to be rendered" type="Id" required="true" assignTo="{!caseId}" />

    <!-- Component Body -->
    <apex:componentBody >
        <apex:form id="comments">
            <apex:pageMessages ></apex:pageMessages>
            <apex:pageBlock >
                <apex:pageBlockButtons location="top">
                    <!--<apex:commandButton value="New" onclick="openCaseComment();"/>-->
                    <button type="button" class="btn btn-lg" data-toggle="modal" data-target="#myModal">New</button>
                </apex:pageBlockButtons>
                <div class="modal fade" id="myModal" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Add New Comment</h4>
                            </div>
                            <div class="modal-body">
                                <p><apex:outputPanel >
                                <apex:inputTextarea style="width: 100%;" value="{!newComments}"/>
                                </apex:outputPanel></p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal" onclick="openCaseComment();">Save</button>
                            </div>
                        </div>

                    </div>
                </div>

                <apex:pageBlockTable value="{!Comments}" var="comment" style="font-size:85%;"> 
                    <apex:column headerValue="Comments">
                    <apex:outputText escape="false" value="{!comment.commentText}"/>
                    </apex:column>
                </apex:pageBlockTable>
            </apex:pageBlock> 
            <apex:actionFunction name="SaveComments" action="{!NewComment}"/>

        </apex:form>

    <Script>
        function openCaseComment(){
        SaveComments();
        }

    </Script>    
    </apex:componentBody>
</apex:component>