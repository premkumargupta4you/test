/****************************************************************************************
* Create By       :     Deloitte Consulting
* Create Date     :     03/25/2016
* Description     :     Trigger for Case object
* Modification Log:
*   -----------------------------------------------------------------------------
*   * Developer             Date                Modification ID     Description
*   * ----------------------------------------------------------------------------                 
*   * PKARADI               03/25/2016                              Initial version.                          
*   * PKARADI               04/06/2016                              Called the Case Milestone logic      
*   * Prem                  04/06/2016           M-001              Pre-populate the Step2 those fields which are part of Step 1.It will be visible for ES person when Employee want to escalate the Case by Phone             
*   * Prem                  04/14/2016           M-002              For SIS forms if user has select "May we send your claim  to your HR resource Manager", then send the email.
*   * Puneet                04/20/2016           M-003              To restrict the update once case is closed.
*   * Prem                  05/05/2016           M-004              To restrict if the Case record type is Report Request and an open Activity/Task exist. 
*   * Prem                  13/06/2016           M-005              Add logic to dynamically populate 'priority' based on custom object.
*   * Prashant              06/21/2016           M-006              Added logic for SLA calculation 
*   * Prashant              07/11/2016           M-007              Added logic to restrict users from Accepting case from queue
*****************************************************************************************/
trigger MACYS_Case on Case (after insert, after update, before insert, before update) 
{
    try
    {
        list<Case> lstCse;
        list<Case> lstCseAfterSecongFollowUp;
        list<Case> stepTwoCasesToAutopopulateFields=new list<Case>();          //List for cases whose fields needs to populate from contact.

        MACYS_CaseSLAMappingWithPriority__c  custSetObj= MACYS_CaseSLAMappingWithPriority__c.getOrgDefaults(); 

        //M-005
        if(trigger.isBefore && (trigger.isInsert || trigger.isUpdate))
        {
            Map<String, MACYS_CasePriorityMapping__c> mapOfCustomObject = MACYS_CasePriorityMapping__c.getAll();
            Map<String,String> latestMap = new Map<String,String>();
            String catComb='';
            String priority='';
            // creating a map from custom setting to check with trigger.new
            for(String fieldName : mapOfCustomObject.keySet())
            {
                catComb = mapOfCustomObject.get(fieldName).MACYS_CategoryCombination__c ;
                priority = mapOfCustomObject.get(fieldName).MACYS_Priority__c ;
                latestMap.put(catComb,priority);
            }
            
            List<Case> insertedCaseList = Trigger.new;
            String urlAddress=String.valueOF(URL.getCurrentRequestUrl());
            for(Case c:insertedCaseList)
            {
                // adding for TKT-000628. Intake channel restriction.
                if(urlAddress.contains(Label.MACYS_PortalIndentifier)){
                    if((c.Origin!='Self-Service' && trigger.isInsert) || (trigger.isUpdate && c.Origin!='Self-Service' && trigger.oldMap.get(c.Id).Origin!=c.Origin)){
                        c.addError(Label.MACYS_InTakeErrorForPortal);
                    }
                }
                else{
                    if((c.Origin=='Self-Service' && trigger.isInsert)|| (trigger.isUpdate && c.Origin=='Self-Service' && trigger.oldMap.get(c.Id).Origin!=c.Origin)){
                        c.addError(Label.MACYS_InTakeErrorForInternal);
                    }
                }
                //end of TKT-000628. 
            }
           
            // in insert checking if the match of case which is inserting w.r.t 'case cat:case sub cat1:case sub cat2' exist in custom setting
            // if yes, taking the priority from custom setting and putting it on the case priority
            if(trigger.isInsert)
            {   
                for(Case caseInsert:insertedCaseList)
                {
                    String caseCatComb='';
                    if(caseInsert.MACYS_CaseSubCategory2__c==null)
                    {
                        caseCatComb = caseInsert.MACYS_CaseCategory__c+':'+caseInsert.MACYS_CaseSubCategory1__c+':';
                    }
                    else
                    {
                        caseCatComb = caseInsert.MACYS_CaseCategory__c+':'+caseInsert.MACYS_CaseSubCategory1__c +':'+caseInsert.MACYS_CaseSubCategory2__c;
                    }
                    
                    if(latestMap.get(caseCatComb)!=null)
                    {
                        caseInsert.Priority = latestMap.get(caseCatComb);
                    }
                }
                
            }
            // in update checking if the match w.r.t 'case cat:case sub cat1:case sub cat2' is modified.
            // if yes checking the match of case which is inserting w.r.t 'case cat:case sub cat1:case sub cat2' exist in custom setting
            // if yes, taking the priority from custom setting and putting it on the case priority
            if(trigger.isUpdate)
            {
                List<Case> updatedCaseList = Trigger.old;
                List<String> caseUpdateCatCombList = new List<String>();
                
                for(Case cse:insertedCaseList)
                {
                    if(cse.MACYS_CaseCategory__c!= trigger.oldMap.get(cse.Id).MACYS_CaseCategory__c||cse.MACYS_CaseSubCategory1__c!= trigger.oldMap.get(cse.Id).MACYS_CaseSubCategory1__c||cse.MACYS_CaseSubCategory2__c!= trigger.oldMap.get(cse.Id).MACYS_CaseSubCategory2__c)
                    { 
                        String caseUpdateCatComb='';
                        
                        if(cse.MACYS_CaseSubCategory2__c==null)
                        {
                            caseUpdateCatComb = cse.MACYS_CaseCategory__c+':'+cse.MACYS_CaseSubCategory1__c+':';
                        }
                        else
                        {
                            caseUpdateCatComb = cse.MACYS_CaseCategory__c+':'+cse.MACYS_CaseSubCategory1__c +':'+cse.MACYS_CaseSubCategory2__c;
                        }
                        
                        if(latestMap.get(caseUpdateCatComb)!=null && cse.Priority == trigger.oldMap.get(cse.Id).Priority)
                        {
                            //system.debug('before update  priority ------'+caseInsert.Priority);
                            cse.Priority = latestMap.get(caseUpdateCatComb);
                            //system.debug('after update priority ------'+caseInsert.Priority);
                        }
                    }
                }
             }
        }
        
        //Get the Case which need to be assigned the entitlements
        if(trigger.isBefore && trigger.isInsert)
        {
            Id loggedinUserID=userinfo.getUserId();   // getting ID of loggedin User
            Id loggedinUserProfileID=userinfo.getProfileId();   // getting Profile ID of loggedin User
            Contact contactAssociatedToLoggedinUSer=new Contact();

            Profile profileName = [Select Name from Profile where Id =: loggedinUserProfileID limit 1];

            try{
                if(profileName.name==custSetObj.MACYS_LoggedinUserProfileName__c)
                {
                    contactAssociatedToLoggedinUSer=[select id from contact where MACYS_User__c=:loggedinUserID limit 1];  //getting contact associated to loggedin user       
                    
                }
            }
            catch(Exception ex){
                trigger.new[0].addError(Label.MACYS_NoContactErrorMessage);
            }
            
            lstCse = new list<Case>();
            //Database.DMLOptions dmlOpts = new Database.DMLOptions();
            //dmlOpts.assignmentRuleHeader.useDefaultRule = true;

            for(Case cse: trigger.new)
            {
                if(cse.ContactId==null && cse.origin!=Label.MACYS_EmailfollowUpSent)
                {    
                    cse.ContactId=contactAssociatedToLoggedinUSer.id;
                }
                
                //Below IF condition is to update Anonymous Contact and nullify the contact if case is created as Anonymous

                if(cse.MACYS_IsAnonymous__c==true)
                {
                    cse.MACYS_AnonymousContact__c=cse.Contactid;
                    //cse.Contactid=null;
                }
            }
            
            
            for(Case cse: trigger.new)
            {
                if(cse.contactid!=null && cse.MACYS_RelatedCase__c==null && cse.MACYS_CaseCategory__c=='Solutions InSTORE'){
                    stepTwoCasesToAutopopulateFields.add(cse);
                    system.debug('came here or not');
                }
            }
           
            //Populating SIS fields from Contact if related case is null.  Modified for SIS formula fields change to normal fields.
            
            if(!stepTwoCasesToAutopopulateFields.isEmpty()){
                MACYS_CaseTriggerHelper.Step2FieldPopulationIfCreatedDirectly(stepTwoCasesToAutopopulateFields);
                system.debug('came here or not at method call');
            }
        }  
        if(trigger.isBefore && (trigger.isInsert || trigger.isUpdate))
        {
             //M-001  starts. Populating SIS fields from realted case if fields are empty. Modified for SIS formula fields change to normal fields.
            if(trigger.isInsert){
                list<Case> lsRelatedList = new list<Case>();
                Set<ID> relatedCases=new Set<ID>();
                for(Case cse: trigger.new)
                {
                    if(cse.MACYS_RelatedCase__c!=null){
                        lsRelatedList.add(cse);
                        relatedCases.add(cse.MACYS_RelatedCase__c);
                    }
                }
                
                //sending list of cases to helper class to update subject
                if(!lsRelatedList.isEmpty())
                {
                   MACYS_CaseTriggerHelper.setSISFormData(lsRelatedList,relatedCases);
                } //M-001  end
            }
            //M-006 Start
            list<Case> lstCaseForSLA = new list<Case>();
            if(trigger.isInsert)
            {
                MACYS_CaseTriggerHelper.CalculateSLA(trigger.new, new map<id, Case>());
            }
             if(trigger.isUpdate)
            {
                for(Case cse: trigger.new)
                {
                    if(String.valueOf(trigger.oldmap.get(cse.id).OwnerId).startsWith('00G') && String.valueOf(cse.OwnerId).startswith('005') ||
                       (Label.MACYS_PendingStatus.contains(cse.status) && trigger.oldmap.get(cse.id).status != cse.status) && !Label.MACYS_PendingStatus.contains(trigger.oldmap.get(cse.id).status) ||
                       (!Label.MACYS_PendingStatus.contains(cse.status) && trigger.oldmap.get(cse.id).status != cse.status) || 
                        cse.status == 'Closed' ||
                        (cse.BusinessHoursId != trigger.oldmap.get(cse.id).BusinessHoursId  && !Label.MACYS_PendingStatus.contains(cse.status)) ||
                        (cse.priority != trigger.oldmap.get(cse.id).priority && !Label.MACYS_PendingStatus.contains(cse.status)) ||
                        (cse.MACYS_CaseCategory__c != trigger.oldmap.get(cse.id).MACYS_CaseCategory__c && !Label.MACYS_PendingStatus.contains(cse.status)) ||
                        (cse.MACYS_CaseSubCategory1__c != trigger.oldmap.get(cse.id).MACYS_CaseSubCategory1__c && !Label.MACYS_PendingStatus.contains(cse.status)) ||
                        (cse.MACYS_CaseSubCategory2__c != trigger.oldmap.get(cse.id).MACYS_CaseSubCategory2__c) && !Label.MACYS_PendingStatus.contains(cse.status))
                    {
                        lstCaseForSLA.add(cse);
                    }
                }
                
                if(!lstCaseForSLA.isEmpty())
                {
                    MACYS_CaseTriggerHelper.CalculateSLA(lstCaseForSLA, trigger.oldmap);
                }
            }
            
            //M-006 End
            
            //Set the followup dates
            list<case> lstFollUpCse = new list<Case>();
            for(Case cse: trigger.new)
            {
                //M-007
                if(trigger.isBefore && trigger.isUpdate)
                {
                    if(String.valueOf(trigger.oldmap.get(cse.id).OwnerId).startsWith('00G') && String.valueOf(cse.OwnerId).startsWith('005') &&
                        custSetObj.MACYS_AgentRoleId__c.Contains(String.valueOf(UserInfo.getUserRoleId()).left(15)))
                    {
                        cse.addError(Label.MACYS_AcceptError);
                    }
                }
                
                if((trigger.isBefore && trigger.isInsert && Label.MACYS_PendingStatus.contains(cse.status))
                    || (trigger.isBefore && trigger.isUpdate && Label.MACYS_PendingStatus.contains(cse.status) && cse.status != trigger.oldMap.get(cse.Id).status))
                {
                    lstFollUpCse.add(cse);
                }
                
            }

            if(!lstFollUpCse.isEmpty())
            {   
                MACYS_CaseTriggerHelper.updateFollowUpDates(lstFollUpCse);
            }
        } 

        //Add Contact and OnBehalf Contact as team member
        if(trigger.isafter && trigger.isInsert)
        {
            List<Case> casesToAdd=new List<Case>();
            map<ID,Set<ID>> casesWithContacts=new map<ID,Set<ID>>();
            
            for(Case cse:trigger.new){
                Set<ID> conIds=new Set<ID>();
                if(cse.RecordTypeId != custSetObj.MACYS_Step1RORecordType__c && cse.RecordTypeId != custSetObj.MACYS_Step2RORecordType__c && cse.RecordTypeId != custSetObj.MACYS_Step3RORecordType__c && cse.RecordTypeId != custSetObj.MACYS_Step4RORecordType__c && cse.RecordTypeId != custSetObj.MACYS_Step1RecordType__c && cse.RecordTypeId != custSetObj.MACYS_Step2RecordType__c && cse.RecordTypeId != custSetObj.MACYS_Step3RecordType__c && cse.RecordTypeId != custSetObj.MACYS_Step4RecordType__c && cse.Origin != Label.MACYS_IntakeChannelCaseImport && cse.Origin != Label.MACYS_IntakeChannelInternal && !cse.MACYS_IsConfidential__c){
                       casesToAdd.add(cse);
                       if(cse.contactid!=null)
                           conIds.add(cse.contactid);
                       if(cse.MACYS_OnBehalfOf__c!=null)
                           conIds.add(cse.MACYS_OnBehalfOf__c);
                       casesWithContacts.put(cse.id,conIds);
                   }
            }
            if(!casesToAdd.isEmpty()){
                MACYS_CaseTriggerHelper.addCaseCreator(casesToAdd,casesWithContacts);
            }
        }
        //End of-Add Contact and OnBehalf Contact as team member
        
        // adding code to check if the login profile is of Community and Case if of SIS step3
        // take the Facilitators and pass the contact to helper class which add that Contact's user to Case team  RW
        if(trigger.isafter && (trigger.isInsert || trigger.isUpdate))
        {
            Set<Id> setConId = new Set<Id>();
            lstCse = new list<Case>();
            map<Id, Id> mapOldConIdCseId = new map<Id, Id>();
            
            if(trigger.isInsert)
            {
                for(Case cse: trigger.new)
                {
                    if(cse.MACYS_FacilitatorName__c != null)
                    {
                        lstCse.add(cse);
                        setConId.add(cse.MACYS_FacilitatorName__c);
                    }
                }
                
                if(!lstCse.isEmpty())
                {
                    MACYS_CaseTriggerHelper.addCaseFaciltator(lstCse, setConId, null);
                }
            }
            
            if(trigger.isUpdate)
            {
                for(Case cse: trigger.new)
                {
                    if(trigger.oldMap.get(cse.id).MACYS_FacilitatorName__c != cse.MACYS_FacilitatorName__c)
                    {
                        lstCse.add(cse);
                        setConId.add(cse.MACYS_FacilitatorName__c);
                    }
                    
                    if(trigger.oldMap != null && trigger.oldMap.get(cse.id).MACYS_FacilitatorName__c != null && 
                        trigger.oldMap.get(cse.id).MACYS_FacilitatorName__c != cse.MACYS_FacilitatorName__c)
                    {
                        mapOldConIdCseId.put(cse.Id, trigger.oldMap.get(cse.id).MACYS_FacilitatorName__c);
                    }
                }
                
                if(!lstCse.isEmpty())
                {
                    MACYS_CaseTriggerHelper.addCaseFaciltator(lstCse, setConId, mapOldConIdCseId);
                }
            //PKARADI:08/01: Revoke RW access to creator, as part of Advice and Counsel change    
            List<Case> casesToAdd=new List<Case>();
            map<ID,Set<ID>> casesWithContacts=new map<ID,Set<ID>>();
            List<ID> casesToRemoveTeamMember=new List<ID>();
            List<Case> casesToModifyTeamMember=new List<Case>();
            for(Case cse: trigger.new){
                Set<ID> conIds=new Set<ID>();
                //If Intake channel change from Internal or case import
                if(cse.RecordTypeId != custSetObj.MACYS_Step1RORecordType__c && cse.RecordTypeId != custSetObj.MACYS_Step2RORecordType__c && cse.RecordTypeId != custSetObj.MACYS_Step3RORecordType__c && cse.RecordTypeId != custSetObj.MACYS_Step4RORecordType__c && cse.status!='Closed' && cse.RecordTypeId != custSetObj.MACYS_Step1RecordType__c && cse.RecordTypeId != custSetObj.MACYS_Step2RecordType__c && cse.RecordTypeId != custSetObj.MACYS_Step3RecordType__c && cse.RecordTypeId != custSetObj.MACYS_Step4RecordType__c && cse.Origin != Label.MACYS_IntakeChannelCaseImport && cse.Origin != Label.MACYS_IntakeChannelInternal && !cse.MACYS_IsConfidential__c&&(trigger.oldMap.get(cse.id).Origin==Label.MACYS_IntakeChannelCaseImport||trigger.oldMap.get(cse.id).Origin==Label.MACYS_IntakeChannelInternal)){
                           casesToAdd.add(cse);
                           if(cse.contactid!=null)
                               conIds.add(cse.contactid);
                           if(cse.MACYS_OnBehalfOf__c!=null)
                               conIds.add(cse.MACYS_OnBehalfOf__c);
                           casesWithContacts.put(cse.id,conIds);
                }
                //If Intake channel change to Internal or case import
                if(cse.RecordTypeId != custSetObj.MACYS_Step1RORecordType__c && cse.RecordTypeId != custSetObj.MACYS_Step2RORecordType__c && cse.RecordTypeId != custSetObj.MACYS_Step3RORecordType__c && cse.RecordTypeId != custSetObj.MACYS_Step4RORecordType__c &&cse.RecordTypeId != custSetObj.MACYS_Step1RecordType__c && cse.RecordTypeId != custSetObj.MACYS_Step2RecordType__c && cse.RecordTypeId != custSetObj.MACYS_Step3RecordType__c && cse.RecordTypeId != custSetObj.MACYS_Step4RecordType__c && (cse.Origin == Label.MACYS_IntakeChannelCaseImport || cse.Origin == Label.MACYS_IntakeChannelInternal) && !cse.MACYS_IsConfidential__c&&(trigger.oldMap.get(cse.id).Origin!=Label.MACYS_IntakeChannelCaseImport && trigger.oldMap.get(cse.id).Origin!=Label.MACYS_IntakeChannelInternal)){
                       
                       casesToRemoveTeamMember.add(cse.id);
                       
                }
                //If Status changes to Closed or OnBehalf get modify.
                if(((cse.status=='Closed'&&trigger.oldMap.get(cse.id).Status!='Closed')||cse.MACYS_OnBehalfOf__c!=trigger.oldMap.get(cse.id).MACYS_OnBehalfOf__c) && cse.RecordTypeId != custSetObj.MACYS_Step1RORecordType__c && cse.RecordTypeId != custSetObj.MACYS_Step2RORecordType__c && cse.RecordTypeId != custSetObj.MACYS_Step3RORecordType__c && cse.RecordTypeId != custSetObj.MACYS_Step4RORecordType__c && cse.RecordTypeId != custSetObj.MACYS_Step1RecordType__c && cse.RecordTypeId != custSetObj.MACYS_Step2RecordType__c && cse.RecordTypeId != custSetObj.MACYS_Step3RecordType__c && cse.RecordTypeId != custSetObj.MACYS_Step4RecordType__c && cse.Origin != Label.MACYS_IntakeChannelCaseImport && cse.Origin != Label.MACYS_IntakeChannelInternal && !cse.MACYS_IsConfidential__c){
                       casesToModifyTeamMember.add(cse);
                }
            }
            
            if(!casesToAdd.isEmpty()){
                MACYS_CaseTriggerHelper.addCaseCreator(casesToAdd,casesWithContacts);
            }
            if(!casesToRemoveTeamMember.isEmpty()){
                List<case> blankList=new List<case>();
                MACYS_CaseTriggerHelper.removeCaseTeamMember(casesToRemoveTeamMember,blankList);
            }
            if(!casesToModifyTeamMember.isEmpty()){
                List<ID> blankID=new List<ID>();
                MACYS_CaseTriggerHelper.removeCaseTeamMember(blankID,casesToModifyTeamMember);
            }
           }
        
        }

        //Update Case owner to owners manager
        if(trigger.isBefore && trigger.isUpdate)
        {
            lstCse = new list<Case>();
            set<Id> setUserId = new set<Id>();

            lstCseAfterSecongFollowUp=new list<Case>();  //this list will contain cases with pending Cutomer action after second follow up
            set<Id> setOwnerId = new set<Id>();          // it will contain Owner id of above cases

            for(Case cse: trigger.new)
            {
                if(cse.MACYS_CaseSubStatus__c== Label.MACYS_CaseSubStatusResolutionSLAViolated && cse.MACYS_CaseSubStatus__c!= trigger.oldMap.get(cse.Id).MACYS_CaseSubStatus__c)
                {
                    lstCse.add(cse);
                    setUserId.add(cse.OwnerId);
                }

                if(cse.MACYS_CaseSubStatus__c==Label.MACYS_CaseSubStatusPendingActionViolated && cse.MACYS_CaseSubStatus__c!= trigger.oldMap.get(cse.Id).MACYS_CaseSubStatus__c)
                {
                    lstCseAfterSecongFollowUp.add(cse);
                    setOwnerId.add(cse.OwnerId);
                }
            }

            if(!lstCse.isEmpty())
            {   
                MACYS_CaseTriggerHelper.updateCaseOwner(lstCse, setUserId);
            }

            if(!lstCseAfterSecongFollowUp.isEmpty())
            {
                MACYS_CaseTriggerHelper.updateCaseOwner(lstCseAfterSecongFollowUp, setOwnerId);
            }
        }

        //Update the business hours
        if(trigger.isBefore && trigger.isUpdate)
        {
            lstCse = new list<Case>();
            set<Id> setUserId = new set<Id>();
            for(Case cse: trigger.new)
            {
                if(cse.OwnerId != trigger.oldMap.get(cse.Id).OwnerId)
                {
                    lstCse.add(cse);
                    setUserId.add(cse.OwnerId);
                }
            }

            if(!lstCse.isEmpty())
            {   
                MACYS_CaseTriggerHelper.updateCaseBusinessHours(lstCse, setUserId);
            }       
        }

        // M-004
        if(trigger.isBefore && trigger.isUpdate)
        {   
            List<String> listCaseNumber = new List<String>();
            for(Case cse: trigger.new)
            {
                system.debug('Case recordtypeid----'+cse.recordtypeid);
                system.debug('Case recordtypeid from custom setting----'+custSetObj.MACYS_CaseReportRequestID__c);
                if(cse.recordtypeid == custSetObj.MACYS_CaseReportRequestID__c && cse.status== System.Label.MACYS_Closed)
                {
                    listCaseNumber.add(cse.Casenumber);
                }
            }
            if(listCaseNumber.size()>0)
            {
                String[] pendingStatus = Label.MACYS_PendingStatus.split(',');
                
                system.debug('--------report query --------');
                List<Case> listCase = [SELECT  (SELECT Id,status FROM OpenActivities where status != :System.Label.MACYS_StatusComplete) activity FROM Case where status in:pendingStatus and MACYS_CaseCategory__c=:System.Label.MACYS_ReportRequests and Casenumber =:listCaseNumber]; //Label.MACYS_PendingCustomerAction
                system.debug('--------report query --->'+listCase);
                for(case c :listCase)
                {
                    if(c.OpenActivities!=null && c.OpenActivities.size()>0){
                        trigger.new[0].addError(System.Label.MACYS_ErrorForOpenCase); //"Report Request" Case cannot be closed if Open Activities exist.Please complete the Activities.
                    }
                }
            }
        }
        // add for TKT-000282
        if(trigger.isAfter && trigger.isInsert){
            List<Case> casesToAddComments=new List<Case>();  //part of copying comments from previous step to current step.
            
            for(Case cse: trigger.new)
            {
                //Taking comments from previous step to current Step in SIS.
                if(!String.IsEmpty(cse.MACYS_SISStep__c) && cse.MACYS_RelatedCase__c!=null){
                    casesToAddComments.add(cse);
                }
            }
            
            //Sending list of cases to add comments.
            If(!casesToAddComments.isEmpty()){
                MACYS_CaseTriggerHelper.addingCommentsFromPreviousStep(casesToAddComments);
            }
        }
        
    }
    catch(Exception ex)
    {
        System.debug('Exception Caught'); throw ex;
        MACYS_Exception.createException('Case Trigger',ex.getMessage(), ex.getStackTraceString(),ex.getTypeName(),null,null);
    }
}