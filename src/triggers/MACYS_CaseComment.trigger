/****************************************************************************************
* Create By       :     Deloitte Consulting
* Create Date     :     06/13/2016
* Description     :     Trigger for Case Comment
* Modification Log:
*   -----------------------------------------------------------------------------
*   * Developer             Date                Modification ID     Description
*   * ----------------------------------------------------------------------------                 
*   * Puneet Jalota       06/13/2016                               Initial version.                           
*****************************************************************************************/
trigger MACYS_CaseComment on CaseComment (before update){

    // AS per TKT-000065, None can modify existing 'Case comment', but can add new case comments in any state of Case.
    if(Trigger.isUpdate) {
         
        for (CaseComment t: Trigger.new){
          t.addError(Label.MACYS_CaseCommentsOnUpdateCaseError);    
          break; 
        }       
      }      
}