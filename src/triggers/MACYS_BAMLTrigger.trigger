/****************************************************************************************
* Create By       :     Deloitte Consulting
* Create Date     :    06/09/2016
* Description     :     Trigger for BAML Record object
* Modification Log:
*   -----------------------------------------------------------------------------
*   * Developer             Date                Modification ID     Description
*   * ----------------------------------------------------------------------------                 
*   * Puneet Jalota       06/09/2016                              Initial version.                          
 
*****************************************************************************************/
trigger MACYS_BAMLTrigger on MACYS_BMLRecord__c (before insert) {
public List<Id> caseIds = new List<Id>();
    
    if(trigger.isBefore && trigger.isInsert){
        //creating instance of Helper class and sending BAML inserted records to the Case creation method of that class for furthur processing.
        
       // MACYS_creatingCaseRecordForBAMLInsert creatingCaseRecordForBAMlInsertedRecordsInstance=new MACYS_creatingCaseRecordForBAMLInsert();
       // caseIds = creatingCaseRecordForBAMlInsertedRecordsInstance.creatingCase(trigger.new);
       MACYS_CaseImport caseImportObj=new MACYS_CaseImport();
       caseImportObj.creatingCase(trigger.new);
       
    }


}