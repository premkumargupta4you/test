<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>MACYS_CaseClosureNotification</fullName>
        <description>Case Closure Notification</description>
        <protected>false</protected>
        <recipients>
            <field>MACYS_EmailID__c</field>
            <type>email</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>MACYs_EmailTemplates/MACYS_CaseCloseNotification</template>
    </alerts>
    <alerts>
        <fullName>MACYS_CaseClosureNotificationToOnBehalfContact</fullName>
        <description>Case Closure Notification To OnBehalf Contact</description>
        <protected>false</protected>
        <recipients>
            <field>MACYS_OnBehalfOf__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>MACYs_EmailTemplates/MACYS_CaseCloseNotificationToOnBehalfContact</template>
    </alerts>
    <alerts>
        <fullName>MACYS_CaseCreationNotification</fullName>
        <description>Case Creation Notification</description>
        <protected>false</protected>
        <recipients>
            <field>MACYS_EmailID__c</field>
            <type>email</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>MACYs_EmailTemplates/MACYS_CaseOpenNotification</template>
    </alerts>
    <alerts>
        <fullName>MACYS_CaseCreationNotificationToOnBehalfContact</fullName>
        <description>Case Creation Notification To OnBehalf Contact</description>
        <protected>false</protected>
        <recipients>
            <field>MACYS_OnBehalfOf__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>MACYs_EmailTemplates/MACYS_CaseOpenNotificationToonBehalfContact</template>
    </alerts>
    <alerts>
        <fullName>MACYS_CaseCustomerActionRequiredNotification</fullName>
        <description>Case Customer Action Required Notification</description>
        <protected>false</protected>
        <recipients>
            <field>MACYS_EmailID__c</field>
            <type>email</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>MACYs_EmailTemplates/MACYS_CaseCustomerActionNotification</template>
    </alerts>
    <alerts>
        <fullName>MACYS_CaseReportRequestApprovedCloseNotification</fullName>
        <description>Case Report Request Approved Close Notification</description>
        <protected>false</protected>
        <recipients>
            <field>MACYS_EmailID__c</field>
            <type>email</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>MACYs_EmailTemplates/MACYS_ReportRequestApprovedCaseCloseNotification</template>
    </alerts>
    <alerts>
        <fullName>MACYS_CaseReportRequestApprovedCloseNotificationOnBehalf</fullName>
        <description>Case Report Request Approved Close Notification On Behalf</description>
        <protected>false</protected>
        <recipients>
            <field>MACYS_OnBehalfOf__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>MACYs_EmailTemplates/MACYS_ReportRequestApprovedCaseCloseNotificationOnBehalf</template>
    </alerts>
    <alerts>
        <fullName>MACYS_CaseSISSLAWarningNotificationToOwner</fullName>
        <description>Case SIS SLA warning notification to Owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>MACYs_EmailTemplates/MACYS_CaseSLAWarningNotification</template>
    </alerts>
    <alerts>
        <fullName>MACYS_CaseSLAViolationNotificationToOwnersManager</fullName>
        <description>Case SLA Violation notification to Owners Manager</description>
        <protected>false</protected>
        <recipients>
            <field>MACYS_CaseOwnersManagerEmail__c</field>
            <type>email</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>MACYs_EmailTemplates/MACYS_CaseSLAViolationNotification</template>
    </alerts>
    <alerts>
        <fullName>MACYS_CaseSLAWarningNotificationToOwner</fullName>
        <description>Case SLA warning notification to Owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>MACYs_EmailTemplates/MACYS_CaseNonSISSLAWarningNotification</template>
    </alerts>
    <alerts>
        <fullName>MACYS_CaseSLAWarningNotificationToOwnerCommunity</fullName>
        <description>Case SLA warning notification to Owner for Community</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>MACYs_EmailTemplates/MACYS_CaseSLAWarningNotificationForCommunity</template>
    </alerts>
    <alerts>
        <fullName>MACYS_CaseSecondFollowupNotification</fullName>
        <description>Case second followup notification</description>
        <protected>false</protected>
        <recipients>
            <field>MACYS_EmailID__c</field>
            <type>email</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>MACYs_EmailTemplates/MACYS_CustomerAutoCloseNotification</template>
    </alerts>
    <alerts>
        <fullName>MACYS_CloserNotificationForRejectedReportRequest</fullName>
        <description>Closer Notification For Rejected Report Request</description>
        <protected>false</protected>
        <recipients>
            <field>MACYS_EmailID__c</field>
            <type>email</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>MACYs_EmailTemplates/MACYS_CaseCloseNotificationWithRejectionNotes</template>
    </alerts>
    <alerts>
        <fullName>MACYS_CloserNotificationForRejectedReportRequestToOnBehalf</fullName>
        <description>Closer Notification For Rejected Report Request To OnBehalf</description>
        <protected>false</protected>
        <recipients>
            <field>MACYS_OnBehalfOf__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>MACYs_EmailTemplates/MACYS_CaseCloseNotificationWithRejectionNotesForOnBehalf</template>
    </alerts>
    <alerts>
        <fullName>MACYS_EmailAlertforCaseviolationtotheCaseOwner</fullName>
        <description>Email Alert for Case violation to the Case Owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>MACYs_EmailTemplates/MACYS_CaseSLAViolationNotificationtoOwner</template>
    </alerts>
    <alerts>
        <fullName>MACYS_EmailNotificationOnCreationOfReportRequest</fullName>
        <description>Email Notification On Creation Of Report Request</description>
        <protected>false</protected>
        <recipients>
            <field>MACYS_EmailID__c</field>
            <type>email</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>MACYs_EmailTemplates/MACYS_CaseOpenNotificationWithImportantNote</template>
    </alerts>
    <alerts>
        <fullName>MACYS_EmailNotificationOnCreationOfReportRequestToOnBehalf</fullName>
        <description>Email Notification On Creation Of Report Request To OnBahalf</description>
        <protected>false</protected>
        <recipients>
            <field>MACYS_OnBehalfOf__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>MACYs_EmailTemplates/MACYS_CaseOpenNotificationWithImportantNoteToOnBehalf</template>
    </alerts>
    <alerts>
        <fullName>MACYS_EmailtoCaseCreator</fullName>
        <description>Email Alert for Case violation to the Case Creator</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>MACYs_EmailTemplates/MACYS_CaseSLAViolationNotificationToCreator</template>
    </alerts>
    <alerts>
        <fullName>MACYS_PendingCustomer</fullName>
        <description>Notify Employee about pending customer action followups</description>
        <protected>false</protected>
        <recipients>
            <field>MACYS_EmailID__c</field>
            <type>email</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>MACYs_EmailTemplates/MACYS_CasePendingCustomerFollowUpNotification</template>
    </alerts>
    <alerts>
        <fullName>MACYS_PendingCustomerToManager</fullName>
        <description>Notify Employee&apos;s Manager about pending customer action followups</description>
        <protected>false</protected>
        <recipients>
            <field>MACYS_HRRepEmailId__c</field>
            <type>email</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>MACYs_EmailTemplates/MACYS_CasePendingCustomerFollowUpNotificationToManager</template>
    </alerts>
    <alerts>
        <fullName>MACYS_PriorityChangeNotificationToEmployee</fullName>
        <description>Priority Change Notification To Employee</description>
        <protected>false</protected>
        <recipients>
            <field>MACYS_EmailID__c</field>
            <type>email</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>MACYs_EmailTemplates/MACYS_PriorityChangeNotificationToEmployee</template>
    </alerts>
    <alerts>
        <fullName>MACYS_PriorityChangeNotificationToOnBehalfContact</fullName>
        <description>Priority Change Notification To OnBehalf Contact</description>
        <protected>false</protected>
        <recipients>
            <field>MACYS_OnBehalfOf__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>MACYs_EmailTemplates/MACYS_PriorityChangeNotificationToOnBehalfContact</template>
    </alerts>
    <alerts>
        <fullName>MACYS_PriorityChangeToManager</fullName>
        <description>Send email to the manager of the ES agent within Employee Services needs to be notified</description>
        <protected>false</protected>
        <recipients>
            <field>MACYS_CaseOwnersManagerEmail__c</field>
            <type>email</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>MACYs_EmailTemplates/MACYS_CaseManagerNotification</template>
    </alerts>
    <alerts>
        <fullName>MACYS_SendEmailToHR</fullName>
        <description>When ES or Portal User has select &quot;May we send your claim  to your HR resource Manager&quot;, then send the email with data</description>
        <protected>false</protected>
        <recipients>
            <field>MACYS_HRRepEmailId__c</field>
            <type>email</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>MACYs_EmailTemplates/MACYS_EmailToHR</template>
    </alerts>
    <fieldUpdates>
        <fullName>CaseCategoryUpdation</fullName>
        <field>MACYS_CaseCategory__c</field>
        <literalValue>General Inquiry</literalValue>
        <name>CaseCategoryUpdation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MACYS_AdhocReportUpdatePriorityCritical</fullName>
        <description>Update the priority to critical when its a legal request or requested for GVP or above</description>
        <field>Priority</field>
        <literalValue>Critical</literalValue>
        <name>Adhoc Report Update Priority as Critical</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MACYS_CaseOriginPopulation</fullName>
        <field>Origin</field>
        <literalValue>Self-Service</literalValue>
        <name>Case Origin Population</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MACYS_CaseOwnersManagerEmailUpdate</fullName>
        <field>MACYS_CaseOwnersManagerEmail__c</field>
        <formula>Owner:User.Manager.Email</formula>
        <name>CaseOwnersManagerEmailUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MACYS_CasePriorityUpdate</fullName>
        <field>Priority</field>
        <literalValue>Critical</literalValue>
        <name>MACYS_CasePriorityUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MACYS_CaseRecordTypeChangeToDeathClaim</fullName>
        <field>RecordTypeId</field>
        <lookupValue>MACYS_DeathClaims</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>CaseRecordTypeChangeToDeathClaim</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MACYS_CaseRecordTypeChangeToStep2</fullName>
        <description>If the Step# is changed to Step 2 from Step 1 , then Change the record type of the case of Step2</description>
        <field>RecordTypeId</field>
        <lookupValue>MACYS_SISStep2</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>CaseRecordTypeChangeToStep2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MACYS_CaseRecordTypeChangeToStep3</fullName>
        <description>If the Step# is changed to Step 3 from Step 1 or Step 2 , then Change the record type of the case of Step3</description>
        <field>RecordTypeId</field>
        <lookupValue>MACYS_SISStep3</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>CaseRecordTypeChangeToStep3</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MACYS_CaseRecordTypeChangeToStep4</fullName>
        <description>If the Step# is changed to Step 4 from any  Steps , then Change the record type of the case of Step4</description>
        <field>RecordTypeId</field>
        <lookupValue>MACYS_SISStep4</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>CaseRecordTypeChangeToStep4</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MACYS_CaseStatusUpdate</fullName>
        <description>Making Status to Draft for Death Claim</description>
        <field>Status</field>
        <literalValue>Draft</literalValue>
        <name>Case Status Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MACYS_CaseStatusUpdatetoCurrentStep</fullName>
        <description>Change the Step status to &apos;Draft&apos; .</description>
        <field>Status</field>
        <literalValue>Draft</literalValue>
        <name>Case Status Update to Current Step</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MACYS_ChangeStatusForPaperWorkSent</fullName>
        <description>When field &apos;paperwork sent&apos; is checked, change the status to &apos;Pending Customer Action&apos;</description>
        <field>Status</field>
        <literalValue>Pending Customer Action</literalValue>
        <name>Change Status for PaperWork Sent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MACYS_ChangeStatusto_InProgressFU</fullName>
        <field>Status</field>
        <literalValue>In-Progress</literalValue>
        <name>MACYS Change Status to InProgress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MACYS_DeathClaimRecordTypeUpdate</fullName>
        <field>RecordTypeId</field>
        <lookupValue>MACYS_DeathClaimsRO</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>DeathClaimRecordTypeUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MACYS_HRManagerEmailUpdate</fullName>
        <description>When ES or Portal User has select &quot;May we send your claim to your HR resource Manager&quot;, this field will update from  workflow . This will be use in SIS forms.</description>
        <field>MACYS_HRRepEmailId__c</field>
        <formula>Contact.MACYS_HRManager__r.Email</formula>
        <name>HR Manager Email Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MACYS_HavyouspokentoManagementValueUpdat</fullName>
        <field>MACYS_SupervisorOrManagerCommunication__c</field>
        <literalValue>No</literalValue>
        <name>HaveyouspokentoManagementValueUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MACYS_HowdidyouhearaboutSISValueUpdate</fullName>
        <field>MACYS_InStoreSolutionAwarenenss__c</field>
        <literalValue>Letter</literalValue>
        <name>How did you hear about SIS Value Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MACYS_MayweinformHRRepValueUpdate</fullName>
        <field>MACYS_HRManagerConfirmation__c</field>
        <literalValue>No</literalValue>
        <name>May we inform HR Rep Value Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MACYS_RecordTypeChangeForGIClosedCase</fullName>
        <description>When the status us change to close for GI cases, the record type will change to &quot;General Inquiry RO Close&quot;.</description>
        <field>RecordTypeId</field>
        <lookupValue>Macys_GenInqROClose</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>RecordType Change For GI Close Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MACYS_RecordTypeChangeForHRCommCloseCase</fullName>
        <description>When the status us change to close for GI cases, the record type will change to &quot;HR Communication/Content Update  RO Layout&quot;.</description>
        <field>RecordTypeId</field>
        <lookupValue>MACYS_ContentCreationUpdateRequestRO</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>RecordType Change For HR Comm Close Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MACYS_RecordTypeChangeForSIS1ClosedCase</fullName>
        <description>When the status us change to close for SIS 1 cases, the record type will change to &quot;SIS Step 1 RO Close&quot;.</description>
        <field>RecordTypeId</field>
        <lookupValue>MACYS_SISStep1RoClose</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>RecordType Change For SIS 1 Close Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MACYS_RecordTypeChangeForSIS2ClosedCase</fullName>
        <description>When the status us change to close for SIS 2 cases, the record type will change to &quot;SIS Step 2 RO Close&quot;.</description>
        <field>RecordTypeId</field>
        <lookupValue>MACYS_SISStep2RoClose</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>RecordType Change For SIS 2 Close Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MACYS_RecordTypeChangeForSIS3ClosedCase</fullName>
        <description>When the status us change to close for SIS 3 cases, the record type will change to &quot;SIS Step 3 RO Close&quot;.</description>
        <field>RecordTypeId</field>
        <lookupValue>MACYS_SISStep3RoClose</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>RecordType Change For SIS 3 Close Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MACYS_RecordTypeChangeForSIS4ClosedCase</fullName>
        <description>When the status us change to close for SIS 4 cases, the record type will change to &quot;SIS Step 4 RO Close&quot;.</description>
        <field>RecordTypeId</field>
        <lookupValue>MACYS_SISStep4RoClose</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>RecordType Change For SIS 4 Close Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MACYS_ReoprtRequestRO</fullName>
        <field>RecordTypeId</field>
        <lookupValue>MACYS_ReportRequestRO</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>ReoprtRequestRO</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MACYS_ResetStoppedField</fullName>
        <description>This will update the stopped field to false</description>
        <field>IsStopped</field>
        <literalValue>0</literalValue>
        <name>Reset Stopped Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MACYS_SettingCaseSubStatusToFired</fullName>
        <description>This field update will trigger the case owner change to Owner&apos;s manager.</description>
        <field>MACYS_CaseSubStatus__c</field>
        <literalValue>Pending Action Violated</literalValue>
        <name>Setting Case Sub Status To Fired</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MACYS_SystemSupportRO</fullName>
        <field>RecordTypeId</field>
        <lookupValue>MACYS_HRSystemSupportRO</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>SystemSupportRO</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MACYS_UpdateCaseStatusClosed</fullName>
        <description>Set the status of the Case to closed</description>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>Update Case Status Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MACYS_UpdateEmailId</fullName>
        <description>Update the EmailId based on the preferred mode of contact</description>
        <field>MACYS_EmailID__c</field>
        <formula>IF( ISPICKVAL( MACYS_PreferredModeOfContact__c , &apos;Work Email&apos;) ,Contact.Email ,   IF(ISPICKVAL( MACYS_PreferredModeOfContact__c , &apos;Other Email&apos;), Contact.MACYS_OtherEmailAddress__c, if(ISPICKVAL( MACYS_PreferredModeOfContact__c , &apos;Other&apos;),if(CONTAINS(MACYS_PreferredModeOfContactInformation__c, &apos;@&apos;),MACYS_PreferredModeOfContactInformation__c,&apos;&apos;),&apos;&apos;))  )</formula>
        <name>Update EmailId</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MACYS_UpdateFirstCallResolution</fullName>
        <description>Update the first call resolution checkbox when the the status is changed from Open to Closed</description>
        <field>MACYS_IsFirstCallResolution__c</field>
        <literalValue>1</literalValue>
        <name>MACYS Update First Call Resolution</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MACYS_UpdatePreferredModeOfContactInform</fullName>
        <description>Updates the preferred mode of contact information</description>
        <field>MACYS_PreferredModeOfContactInformation__c</field>
        <formula>SUBSTITUTE(SUBSTITUTE(SUBSTITUTE(SUBSTITUTE(IF( 
	ISPICKVAL( MACYS_PreferredModeOfContact__c , &apos;Other Phone&apos;) , Contact.OtherPhone, 
	IF(ISPICKVAL( MACYS_PreferredModeOfContact__c , &apos;Work Email&apos;) ,Contact.Email, 
		IF(ISPICKVAL( MACYS_PreferredModeOfContact__c , &apos;Other Email&apos;), Contact.MACYS_OtherEmailAddress__c,
			IF( ISPICKVAL( MACYS_PreferredModeOfContact__c , &apos;Office Phone&apos;), Contact.Phone, &apos;&apos;)
		)
	)
	), &apos;(&apos;,&apos;&apos;), &apos;)&apos;,&apos;&apos;),&apos;-&apos;,&apos;&apos;),&apos; &apos;,&apos;&apos;)</formula>
        <name>MACYS_UpdatePreferredModeOfContactInform</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MACYS_UpdatePriorityEmpCompliance</fullName>
        <description>This field update will set the priority to High for employment compliance cases</description>
        <field>Priority</field>
        <literalValue>High</literalValue>
        <name>MACYS_UpdatePriorityEmpCompliance</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MACYS_UpdateSISCaseNumber</fullName>
        <description>Update the SIS Case Number from Case Number based on SIS Step</description>
        <field>MACYS_SISCaseNumber__c</field>
        <formula>IF(ISPICKVAL(MACYS_SISStep__c, &apos;Step-1&apos;) ,  CaseNumber  &amp; &quot;-1&quot;, IF(ISPICKVAL(MACYS_SISStep__c, &apos;Step-2&apos;),  MACYS_RelatedCase__r.CaseNumber  &amp; &quot;-2&quot;, IF(ISPICKVAL(MACYS_SISStep__c, &apos;Step-3&apos;),  MACYS_RelatedCase__r.MACYS_RelatedCase__r.CaseNumber   &amp; &quot;-3&quot;,  MACYS_RelatedCase__r.MACYS_RelatedCase__r.MACYS_RelatedCase__r.CaseNumber   &amp; &quot;-4&quot;)))</formula>
        <name>Update SIS Case Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MACYS_UpdateSISCaseNumberForStep1</fullName>
        <field>MACYS_SISCaseNumber__c</field>
        <formula>CaseNumber &amp; &quot;-1&quot;</formula>
        <name>Update SIS Case Number For Step1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MACYS_UpdateSISCaseNumberForStep2</fullName>
        <field>MACYS_SISCaseNumber__c</field>
        <formula>IF(ISBLANK(MACYS_RelatedCase__c), CaseNumber &amp; &quot;-2&quot;,MACYS_RelatedCase__r.CaseNumber &amp; &quot;-2&quot;)</formula>
        <name>Update SIS Case Number For Step2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MACYS_UpdateSISCaseNumberForStep3</fullName>
        <field>MACYS_SISCaseNumber__c</field>
        <formula>IF(ISBLANK(MACYS_RelatedCase__c), CaseNumber &amp; &quot;-3&quot;,LEFT(MACYS_RelatedCase__r.MACYS_SISCaseNumber__c, LEN(MACYS_RelatedCase__r.MACYS_SISCaseNumber__c)-2) &amp; &quot;-3&quot;)</formula>
        <name>Update SIS Case Number For Step3</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MACYS_UpdateSISCaseNumberForStep4</fullName>
        <field>MACYS_SISCaseNumber__c</field>
        <formula>IF(ISBLANK(MACYS_RelatedCase__c), CaseNumber &amp; &quot;-4&quot;,LEFT(MACYS_RelatedCase__r.MACYS_SISCaseNumber__c, LEN(MACYS_RelatedCase__r.MACYS_SISCaseNumber__c)-2) &amp; &quot;-4&quot;)</formula>
        <name>Update SIS Case Number For Step4</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MACYS_UpdateSLAViolation</fullName>
        <description>This will update the Case status with &quot;Resolution SLA Violated&quot; when the resolution SLA will be violated</description>
        <field>MACYS_CaseSubStatus__c</field>
        <literalValue>Resolution SLA Violated</literalValue>
        <name>Update SLA Violation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>MACYS Case Priority Change</fullName>
        <actions>
            <name>MACYS_PriorityChangeToManager</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>If an agent changes a case priority to “low, medium or high” i.e originally marked as “critical,” or if an agent changes a case priority from “low, medium, or high” to “critical,” the manager of the ES agent within Employee Services needs to be notify</description>
        <formula>IF(    AND(ISCHANGED(Priority),         NOT(ISPICKVAL((Status),&apos;Draft&apos;)),         OR( 	      ISPICKVAL(PRIORVALUE(Priority),&apos;Critical&apos;), 	      ISPICKVAL(PRIORVALUE(Priority),&apos;High&apos;), 	      ISPICKVAL(PRIORVALUE(Priority),&apos;Medium&apos;),               ISPICKVAL(PRIORVALUE(Priority),&apos;Low&apos;) 	)    )     ,true, false )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>MACYS Case Priority Change Contact</fullName>
        <actions>
            <name>MACYS_PriorityChangeNotificationToEmployee</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>If an agent changes a case priority to “low, medium or high” i.e originally marked as “critical,” or if an agent changes a case priority from “low, medium, or high” to “critical,” the manager of the ES agent within Employee Services needs to be notify</description>
        <formula>IF(    
		AND( 
			Contact.MACYS_HR_Flag__c==&apos;Y&apos;, 	
			ISCHANGED(Priority),         
			NOT(ISPICKVAL((Status),&apos;Draft&apos;)),         
			OR( 	      
				ISPICKVAL(PRIORVALUE(Priority),&apos;Critical&apos;), 	      
				ISPICKVAL(PRIORVALUE(Priority),&apos;High&apos;), 	      
				ISPICKVAL(PRIORVALUE(Priority),&apos;Medium&apos;),               
				ISPICKVAL(PRIORVALUE(Priority),&apos;Low&apos;) 	
			),
			NOT(OR( 
					ISPICKVAL( Origin ,  &apos;Internal&apos;),
					ISPICKVAL( Origin ,  &apos;Case Import&apos;)
				))
			),true, false )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>MACYS Case Priority Change OnBehalfContact</fullName>
        <actions>
            <name>MACYS_PriorityChangeNotificationToOnBehalfContact</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>If an agent changes a case priority to “low, medium or high” i.e originally marked as “critical,” or if an agent changes a case priority from “low, medium, or high” to “critical,” the manager of the ES agent within Employee Services needs to be notify</description>
        <formula>IF(    AND(  MACYS_OnBehalfOf__r.MACYS_HR_Flag__c ==&apos;Y&apos;, 	ISCHANGED(Priority),         NOT(ISPICKVAL((Status),&apos;Draft&apos;)),         OR( 	      ISPICKVAL(PRIORVALUE(Priority),&apos;Critical&apos;), 	      ISPICKVAL(PRIORVALUE(Priority),&apos;High&apos;), 	      ISPICKVAL(PRIORVALUE(Priority),&apos;Medium&apos;),               ISPICKVAL(PRIORVALUE(Priority),&apos;Low&apos;),
			NOT(OR( 
					ISPICKVAL( Origin ,  &apos;Internal&apos;),
					ISPICKVAL( Origin ,  &apos;Case Import&apos;)
				)) 	)    )     ,true, false )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>MACYS Case Priority Change To Owner Manager</fullName>
        <actions>
            <name>MACYS_PriorityChangeToManager</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>equals</operation>
            <value>Critical</value>
        </criteriaItems>
        <description>If an agent changes a case priority to “critical” the manager of the ES agent within Employee Services needs to be notify.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MACYS SLA Notifications</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.MACYS_CaseCategory__c</field>
            <operation>notEqual</operation>
            <value>Solutions InSTORE</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MACYS_ActualCompletionTime__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Cancelled</value>
        </criteriaItems>
        <description>Based on the custom calculations, the notifications are sent</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>MACYS_CaseSLAWarningNotificationToOwner</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.MACYS_SLAWarningTime__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>MACYS_CaseSLAViolationNotificationToOwnersManager</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>MACYS_EmailAlertforCaseviolationtotheCaseOwner</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>MACYS_UpdateSLAViolation</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.MACYS_ResolutionTime__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>MACYS Solutions InStore SLA Notification</fullName>
        <active>true</active>
        <description>This workflow rule will send notifications to SIS case owners when the SLA is nearing for console</description>
        <formula>AND ( OR( 	RecordTypeId =  $Setup.MACYS_CaseSLAMappingWithPriority__c.MACYS_Step2RecordType__c, 	RecordTypeId =  $Setup.MACYS_CaseSLAMappingWithPriority__c.MACYS_Step3RecordType__c    ),   OR( 	ISPICKVAL(Status , &apos;Draft&apos;), 	ISPICKVAL(Status , &apos;Escalated&apos;), 	ISPICKVAL(Status , &apos;In-Progress&apos;), 	ISPICKVAL(Status , &apos;On Hold&apos;), 	ISPICKVAL(Status , &apos;Open&apos;), 	ISPICKVAL(Status , &apos;Not Initiated&apos;)   ),   Owner:User.ProfileId &lt;&gt;  $Setup.MACYS_CaseSLAMappingWithPriority__c.MACYS_HRManagerProfileId__c   )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>MACYS_CaseSISSLAWarningNotificationToOwner</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.MACYS_DatePaperworkReceived__c</offsetFromField>
            <timeLength>40</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>MACYS Solutions InStore SLA Notification for Community</fullName>
        <active>true</active>
        <description>This workflow rule will send notifications to SIS case owners when the SLA is nearing for community</description>
        <formula>AND ( OR( 	RecordTypeId =  $Setup.MACYS_CaseSLAMappingWithPriority__c.MACYS_Step2RecordType__c, 	RecordTypeId =  $Setup.MACYS_CaseSLAMappingWithPriority__c.MACYS_Step3RecordType__c    ),   OR( 	ISPICKVAL(Status , &apos;Draft&apos;), 	ISPICKVAL(Status , &apos;Escalated&apos;), 	ISPICKVAL(Status , &apos;In-Progress&apos;), 	ISPICKVAL(Status , &apos;On Hold&apos;), 	ISPICKVAL(Status , &apos;Open&apos;), 	ISPICKVAL(Status , &apos;Not Initiated&apos;)   ),   Owner:User.ProfileId =  $Setup.MACYS_CaseSLAMappingWithPriority__c.MACYS_HRManagerProfileId__c   )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>MACYS_CaseSLAWarningNotificationToOwnerCommunity</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.MACYS_DatePaperworkReceived__c</offsetFromField>
            <timeLength>40</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>MACYS_CaseCategoryUpdationOnEmailToCase</fullName>
        <actions>
            <name>CaseCategoryUpdation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Email</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Draft</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>MACYS_CaseClosureNotification</fullName>
        <actions>
            <name>MACYS_CaseClosureNotification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6 AND 7</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MACYS_CaseCategory__c</field>
            <operation>notEqual</operation>
            <value>Misdirects</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MACYS_IsFirstCallResolution__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MACYS_PreferredModeOfContact__c</field>
            <operation>equals</operation>
            <value>Work Email,Other Email,Other</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Solutions InSTORE Step 1,Solutions InSTORE Step 2,Solutions InSTORE Step 3,Solutions InSTORE Step 4,Report Request RO,Ad Hoc Report Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MACYS_HR_Flag__c</field>
            <operation>equals</operation>
            <value>Y</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>notEqual</operation>
            <value>Case Import,Internal</value>
        </criteriaItems>
        <description>The workflow rule is used to trigger the notification to the contact associated to the Case about its closure</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MACYS_CaseClosureNotificationForReportRequestApproved</fullName>
        <actions>
            <name>MACYS_CaseReportRequestApprovedCloseNotification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6 AND 7 AND 8</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MACYS_CaseCategory__c</field>
            <operation>notEqual</operation>
            <value>Misdirects</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MACYS_IsFirstCallResolution__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MACYS_PreferredModeOfContact__c</field>
            <operation>equals</operation>
            <value>Work Email,Other Email,Other</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Report Request RO,Ad Hoc Report Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MACYS_Accepted__c</field>
            <operation>equals</operation>
            <value>Approve</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MACYS_HR_Flag__c</field>
            <operation>equals</operation>
            <value>Y</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>notEqual</operation>
            <value>Case Import,Internal</value>
        </criteriaItems>
        <description>The workflow rule is used to trigger the notification to the contact associated to the Case about its closure</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MACYS_CaseClosureNotificationForReportRequestApprovedOnBehalf</fullName>
        <actions>
            <name>MACYS_CaseReportRequestApprovedCloseNotificationOnBehalf</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>The workflow rule is used to trigger the notification to the contact associated to the Case about its closure</description>
        <formula>AND(
	ISPICKVAL(Status , &apos;Closed&apos;),  
	NOT(ISPICKVAL(MACYS_CaseCategory__c , &apos;Misdirects&apos;)),  
	MACYS_IsFirstCallResolution__c==False,  
	OR(ISPICKVAL(MACYS_PreferredModeOfContact__c, &apos;Work Email&apos;),  
	ISPICKVAL(MACYS_PreferredModeOfContact__c, &apos;Other Email&apos;),  
	ISPICKVAL(MACYS_PreferredModeOfContact__c, &apos;Other&apos;)),  
	MACYS_OnBehalfOf__r.MACYS_HR_Flag__c ==&apos;Y&apos;,  
	OR(RecordTypeId ==$Setup.MACYS_CaseSLAMappingWithPriority__c.MACYS_CaseReportRequestID__c,  RecordTypeId ==$Setup.MACYS_CaseSLAMappingWithPriority__c.MACYS_CaseReportRequestROID__c ),  ISPICKVAL(MACYS_Accepted__c, &apos;Approve&apos;), 
	NOT(OR( 
			ISPICKVAL( Origin ,  &apos;Internal&apos;),
			ISPICKVAL( Origin ,  &apos;Case Import&apos;)
			)
		)
	)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MACYS_CaseClosureNotificationForReportRequestRejected</fullName>
        <actions>
            <name>MACYS_CloserNotificationForRejectedReportRequest</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6 AND 7 AND 8</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MACYS_CaseCategory__c</field>
            <operation>notEqual</operation>
            <value>Misdirects</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MACYS_IsFirstCallResolution__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MACYS_PreferredModeOfContact__c</field>
            <operation>equals</operation>
            <value>Work Email,Other Email,Other</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Report Request RO,Ad Hoc Report Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MACYS_Accepted__c</field>
            <operation>equals</operation>
            <value>Deny</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MACYS_HR_Flag__c</field>
            <operation>equals</operation>
            <value>Y</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>notEqual</operation>
            <value>Case Import,Internal</value>
        </criteriaItems>
        <description>The workflow rule is used to trigger the notification to the contact associated to the Case about its closure</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MACYS_CaseClosureNotificationForReportRequestRejectedOnBehalf</fullName>
        <actions>
            <name>MACYS_CloserNotificationForRejectedReportRequestToOnBehalf</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>The workflow rule is used to trigger the notification to the contact associated to the Case about its closure</description>
        <formula>AND(ISPICKVAL(Status , &apos;Closed&apos;),  NOT(ISPICKVAL(MACYS_CaseCategory__c , &apos;Misdirects&apos;)),  MACYS_IsFirstCallResolution__c==False,  OR(ISPICKVAL(MACYS_PreferredModeOfContact__c, &apos;Work Email&apos;),  ISPICKVAL(MACYS_PreferredModeOfContact__c, &apos;Other Email&apos;),  ISPICKVAL(MACYS_PreferredModeOfContact__c, &apos;Other&apos;)),  MACYS_OnBehalfOf__r.MACYS_HR_Flag__c ==&apos;Y&apos;,  OR(RecordTypeId ==$Setup.MACYS_CaseSLAMappingWithPriority__c.MACYS_CaseReportRequestID__c,  RecordTypeId ==$Setup.MACYS_CaseSLAMappingWithPriority__c.MACYS_CaseReportRequestROID__c ),  ISPICKVAL(MACYS_Accepted__c, &apos;Deny&apos;),
NOT(OR( 
					ISPICKVAL( Origin ,  &apos;Internal&apos;),
					ISPICKVAL( Origin ,  &apos;Case Import&apos;)
				))  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MACYS_CaseClosureNotificationOnBehalf</fullName>
        <actions>
            <name>MACYS_CaseClosureNotificationToOnBehalfContact</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>The workflow rule is used to trigger the notification to the contact associated to the Case about its closure</description>
        <formula>AND(ISPICKVAL(Status , &apos;Closed&apos;),  NOT(ISPICKVAL(MACYS_CaseCategory__c  , &apos;Misdirects&apos;)), MACYS_IsFirstCallResolution__c==False, OR(ISPICKVAL(MACYS_PreferredModeOfContact__c, &apos;Work Email&apos;), ISPICKVAL(MACYS_PreferredModeOfContact__c, &apos;Other Email&apos;), ISPICKVAL(MACYS_PreferredModeOfContact__c, &apos;Other&apos;)),  MACYS_OnBehalfOf__r.MACYS_HR_Flag__c ==&apos;Y&apos;, RecordTypeId &lt;&gt; $Setup.MACYS_CaseSLAMappingWithPriority__c.MACYS_Step1RecordType__c,  RecordTypeId &lt;&gt; $Setup.MACYS_CaseSLAMappingWithPriority__c.MACYS_Step2RecordType__c , RecordTypeId &lt;&gt; $Setup.MACYS_CaseSLAMappingWithPriority__c.MACYS_Step3RecordType__c, RecordTypeId &lt;&gt;$Setup.MACYS_CaseSLAMappingWithPriority__c.MACYS_Step4RecordType__c , RecordTypeId &lt;&gt;$Setup.MACYS_CaseSLAMappingWithPriority__c.MACYS_CaseReportRequestID__c,  RecordTypeId &lt;&gt;$Setup.MACYS_CaseSLAMappingWithPriority__c.MACYS_CaseReportRequestROID__c, NOT(OR( 
					ISPICKVAL( Origin ,  &apos;Internal&apos;),
					ISPICKVAL( Origin ,  &apos;Case Import&apos;)
				))  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MACYS_CaseCreationNotification</fullName>
        <actions>
            <name>MACYS_CaseCreationNotification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6 AND 7</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Open</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MACYS_CaseCategory__c</field>
            <operation>notEqual</operation>
            <value>Misdirects</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MACYS_IsFirstCallResolution__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MACYS_PreferredModeOfContact__c</field>
            <operation>equals</operation>
            <value>Work Email,Other Email,Other</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Solutions InSTORE Step 1,Solutions InSTORE Step 2,Solutions InSTORE Step 3,Solutions InSTORE Step 4,Ad Hoc Report Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MACYS_HR_Flag__c</field>
            <operation>equals</operation>
            <value>Y</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>notEqual</operation>
            <value>Case Import,Internal</value>
        </criteriaItems>
        <description>The workflow rule is used to trigger the notification to the contact associated to the Case about its creation.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>MACYS_CaseCreationNotificationForReportRequest</fullName>
        <actions>
            <name>MACYS_EmailNotificationOnCreationOfReportRequest</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6 AND 7</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Open</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MACYS_CaseCategory__c</field>
            <operation>notEqual</operation>
            <value>Misdirects</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MACYS_IsFirstCallResolution__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MACYS_PreferredModeOfContact__c</field>
            <operation>equals</operation>
            <value>Work Email,Other Email,Other</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Ad Hoc Report Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MACYS_HR_Flag__c</field>
            <operation>equals</operation>
            <value>Y</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>notEqual</operation>
            <value>Case Import,Internal</value>
        </criteriaItems>
        <description>The workflow rule is used to trigger the notification to the contact associated to the Case about its creation.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>MACYS_CaseCreationNotificationForReportRequestOnBehalf</fullName>
        <actions>
            <name>MACYS_EmailNotificationOnCreationOfReportRequestToOnBehalf</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>The workflow rule is used to trigger the notification to the contact associated to the Case about its creation. Separated Contact and On Behalf as per HR/Non HR logic</description>
        <formula>AND(   ISPICKVAL(Status, &apos;Open&apos;),   NOT(ISPICKVAL( MACYS_CaseCategory__c , &apos;Misdirects&apos;)),   NOT(MACYS_IsFirstCallResolution__c),   OR(  ISPICKVAL( MACYS_PreferredModeOfContact__c ,&apos;Work Email&apos;),  ISPICKVAL( MACYS_PreferredModeOfContact__c ,&apos;Other Email&apos;),  ISPICKVAL( MACYS_PreferredModeOfContact__c ,&apos;Other&apos;)           ),  RecordTypeId =  $Setup.MACYS_CaseSLAMappingWithPriority__c.MACYS_CaseReportRequestID__c,   MACYS_OnBehalfOf__r.MACYS_HR_Flag__c = &apos;Y&apos;, NOT(OR( 
					ISPICKVAL( Origin ,  &apos;Internal&apos;),
					ISPICKVAL( Origin ,  &apos;Case Import&apos;)
				))     )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>MACYS_CaseCreationNotificationOnBehalf</fullName>
        <actions>
            <name>MACYS_CaseCreationNotificationToOnBehalfContact</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>The workflow rule is used to trigger the notification to the contact associated to the Case about its creation. Separated the Contact and On behalf as per HR/Non-HR changes</description>
        <formula>AND( 	ISPICKVAL(Status, &apos;Open&apos;), 	NOT(ISPICKVAL( MACYS_CaseCategory__c , &apos;Misdirects&apos;)), 	NOT(MACYS_IsFirstCallResolution__c), 	OR( 		ISPICKVAL( MACYS_PreferredModeOfContact__c ,&apos;Work Email&apos;), 		ISPICKVAL( MACYS_PreferredModeOfContact__c ,&apos;Other Email&apos;), 		ISPICKVAL( MACYS_PreferredModeOfContact__c ,&apos;Other&apos;)           ), 	RecordTypeId &lt;&gt; $Setup.MACYS_CaseSLAMappingWithPriority__c.MACYS_Step1RecordType__c,  	RecordTypeId &lt;&gt; $Setup.MACYS_CaseSLAMappingWithPriority__c.MACYS_Step2RecordType__c , 	RecordTypeId &lt;&gt; $Setup.MACYS_CaseSLAMappingWithPriority__c.MACYS_Step3RecordType__c, 	RecordTypeId &lt;&gt;$Setup.MACYS_CaseSLAMappingWithPriority__c.MACYS_Step4RecordType__c , 	RecordTypeId &lt;&gt;$Setup.MACYS_CaseSLAMappingWithPriority__c.MACYS_CaseReportRequestID__c,  	MACYS_OnBehalfOf__r.MACYS_HR_Flag__c = &apos;Y&apos;, NOT(OR( 
					ISPICKVAL( Origin ,  &apos;Internal&apos;),
					ISPICKVAL( Origin ,  &apos;Case Import&apos;)
				))     )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>MACYS_CaseCustomerFollowupNotification</fullName>
        <actions>
            <name>MACYS_CaseCustomerActionRequiredNotification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6 AND 7</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Pending Customer Action,Pending paperwork from employee</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MACYS_CaseCategory__c</field>
            <operation>notEqual</operation>
            <value>Misdirects,Death Notification,Solutions InSTORE</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MACYS_IsFirstCallResolution__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MACYS_PreferredModeOfContact__c</field>
            <operation>equals</operation>
            <value>Work Email,Other Email,Other</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MACYS_CaseSubCategory1__c</field>
            <operation>notEqual</operation>
            <value>First appeal,Second appeal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MACYS_HR_Flag__c</field>
            <operation>equals</operation>
            <value>Y</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>notEqual</operation>
            <value>Case Import,Internal</value>
        </criteriaItems>
        <description>The workflow rule is used to trigger the notification to the contact associated to the Case for followup on the pending action</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MACYS_CaseOwnerManagersEmailUpdate</fullName>
        <actions>
            <name>MACYS_CaseOwnersManagerEmailUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR( ISNEW() ,ISCHANGED(OwnerId)&amp;&amp; NOT(ISPICKVAL((Status),&apos;Draft&apos;)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>MACYS_CasePriorityUpdate</fullName>
        <actions>
            <name>MACYS_CasePriorityUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.MACYS_CaseCategory__c</field>
            <operation>equals</operation>
            <value>Termination</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MACYS_CaseSubCategory1__c</field>
            <operation>equals</operation>
            <value>Additional Final Pay</value>
        </criteriaItems>
        <description>This workflow will update Case priority to Critical for Cases with category as Termination and Sub category as  Final Pay.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>MACYS_CaseUpdatePreferredEmail</fullName>
        <actions>
            <name>MACYS_UpdateEmailId</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>MACYS_UpdatePreferredModeOfContactInform</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update the Work Email or Other Email on Case</description>
        <formula>OR(ISNEW(), ISCHANGED( MACYS_PreferredModeOfContact__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>MACYS_ChangeStatusToInProgress</fullName>
        <actions>
            <name>MACYS_ChangeStatusto_InProgressFU</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When the case cat is changes, the status should be changed to &quot;In- Progress&quot;.  Requirement came from Sriram over call during Sprint 5.</description>
        <formula>ISPICKVAL(Status,&apos;Open&apos;) &amp;&amp; ISCHANGED(MACYS_CaseCategory__c) &amp;&amp;  NOT(ISPICKVAL(Status, &apos;Closed&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>MACYS_ChangingRecordTypeToDeathClaims</fullName>
        <actions>
            <name>MACYS_CaseRecordTypeChangeToDeathClaim</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>MACYS_CaseStatusUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.MACYS_CaseCategory__c</field>
            <operation>equals</operation>
            <value>Death Notification</value>
        </criteriaItems>
        <description>This is to change Case record type from general Inquiry to Death Claims when case category is changed from General Inquiry to Death Notification.
Mentioned in Sriram&apos;s mail on 11th may 16.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MACYS_ChangingRecordTypeToSISStep2</fullName>
        <actions>
            <name>MACYS_CaseRecordTypeChangeToStep2</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>MACYS_CaseStatusUpdatetoCurrentStep</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This is to change Case record type from SIS other step  to SIS Step 2.
Created as part of SIT defect fixes.</description>
        <formula>AND(      ISPICKVAL(MACYS_SISStep__c,&apos;Step-2&apos;),      NOT(CONTAINS(RecordTypeId,$Setup.MACYS_CaseSLAMappingWithPriority__c.MACYS_Step2RecordType__c)),     NOT(ISPICKVAL(Status,&apos;Closed&apos;))  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MACYS_ChangingRecordTypeToSISStep3</fullName>
        <actions>
            <name>MACYS_CaseRecordTypeChangeToStep3</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>MACYS_CaseStatusUpdatetoCurrentStep</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This is to change Case record type from SIS other step  to SIS Step 3.
Created as part of SIT defect fixes.</description>
        <formula>AND(      ISPICKVAL(MACYS_SISStep__c,&apos;Step-3&apos;),      NOT(CONTAINS(RecordTypeId,$Setup.MACYS_CaseSLAMappingWithPriority__c.MACYS_Step3RecordType__c)),     NOT(ISPICKVAL(Status,&apos;Closed&apos;))  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MACYS_ChangingRecordTypeToSISStep4</fullName>
        <actions>
            <name>MACYS_CaseRecordTypeChangeToStep4</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>MACYS_CaseStatusUpdatetoCurrentStep</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This is to change Case record type from any SIS   to SIS Step 4.
Created as part of SIT defect fixes.</description>
        <formula>AND(      ISPICKVAL(MACYS_SISStep__c,&apos;Step-4&apos;),      NOT(CONTAINS(RecordTypeId,$Setup.MACYS_CaseSLAMappingWithPriority__c.MACYS_Step4RecordType__c)),     NOT(ISPICKVAL(Status,&apos;Closed&apos;))  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MACYS_DeathClaimRO</fullName>
        <actions>
            <name>MACYS_DeathClaimRecordTypeUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Death Claims</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <description>When the Death Claim case status is changed to closed, then change the record type to &quot; RO Close&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MACYS_HRCommStatusCloseRO</fullName>
        <actions>
            <name>MACYS_RecordTypeChangeForHRCommCloseCase</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Content Creation/Update Request</value>
        </criteriaItems>
        <description>When the &quot;HR Communication/Update Content&quot; case status is changed to closed, then change the record type to &quot; RO Close&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MACYS_PaperWorkSentStatusUpdate</fullName>
        <actions>
            <name>MACYS_ChangeStatusForPaperWorkSent</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When paper work is sent, status will be updated to Pending Customer action.</description>
        <formula>AND(NOT(ISBLANK(MACYS_PaperworkSent__c)),ISBLANK(MACYS_DatePaperworkReceived__c))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MACYS_PopulatingCaseOriginForPortal</fullName>
        <actions>
            <name>MACYS_CaseOriginPopulation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>MACYS_ CommunityUser</value>
        </criteriaItems>
        <description>to populate case origin created from Portal</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MACYS_ReportRequestRO</fullName>
        <actions>
            <name>MACYS_ReoprtRequestRO</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Ad Hoc Report Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <description>When the Report Request case status is changed to closed, then change the record type to &quot; RO Close&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MACYS_SIS1StatusClosedRO</fullName>
        <actions>
            <name>MACYS_RecordTypeChangeForSIS1ClosedCase</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MACYS_SISStep__c</field>
            <operation>equals</operation>
            <value>Step-1</value>
        </criteriaItems>
        <description>When the SIS 1 case status is changed to closed, then change the record type to &quot; RO Close&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MACYS_SIS2StatusClosedRO</fullName>
        <actions>
            <name>MACYS_RecordTypeChangeForSIS2ClosedCase</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MACYS_SISStep__c</field>
            <operation>equals</operation>
            <value>Step-2</value>
        </criteriaItems>
        <description>When the SIS 2 case status is changed to closed, then change the record type to &quot; RO Close&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MACYS_SIS3StatusClosedRO</fullName>
        <actions>
            <name>MACYS_RecordTypeChangeForSIS3ClosedCase</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MACYS_SISStep__c</field>
            <operation>equals</operation>
            <value>Step-3</value>
        </criteriaItems>
        <description>When the SIS 3 case status is changed to closed, then change the record type to &quot; RO Close&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MACYS_SIS4StatusClosedRO</fullName>
        <actions>
            <name>MACYS_RecordTypeChangeForSIS4ClosedCase</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MACYS_SISStep__c</field>
            <operation>equals</operation>
            <value>Step-4</value>
        </criteriaItems>
        <description>When the SIS 4 case status is changed to closed, then change the record type to &quot; RO Close&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MACYS_ScheduleCaseForAutoClose</fullName>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Pending Customer Action,Pending paperwork from employee</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>equals</operation>
            <value>Critical</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MACYS_CaseCategory__c</field>
            <operation>notEqual</operation>
            <value>Solutions InSTORE,Death Notification</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MACYS_HR_Flag__c</field>
            <operation>equals</operation>
            <value>Y</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MACYS_CaseSubCategory1__c</field>
            <operation>notEqual</operation>
            <value>First appeal,Second appeal,401(K) and Pension</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>notEqual</operation>
            <value>Case Import,Internal</value>
        </criteriaItems>
        <description>This rule will schedule the Case for customer followup if the status is pending customer action and of priority critical</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>MACYS_PendingCustomer</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>MACYS_PendingCustomerToManager</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.MACYS_SecondFollowUp__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>MACYS_CaseCustomerActionRequiredNotification</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.MACYS_FirstFollowUp__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>MACYS_ScheduleCaseForAutoCloseFieldUpdate</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Pending Customer Action,Pending paperwork from employee</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>equals</operation>
            <value>Critical</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MACYS_CaseCategory__c</field>
            <operation>notEqual</operation>
            <value>Solutions InSTORE</value>
        </criteriaItems>
        <description>This rule will schedule the Case for customer followup if the status is pending customer action and of priority critical. Separated field update as part of HR/Non HR changes</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>MACYS_SettingCaseSubStatusToFired</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.MACYS_SecondFollowUp__c</offsetFromField>
            <timeLength>8</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>MACYS_ScheduleCaseForAutoCloseMedLow</fullName>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Pending Customer Action,Pending paperwork from employee</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>notEqual</operation>
            <value>Critical</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MACYS_CaseCategory__c</field>
            <operation>notEqual</operation>
            <value>Solutions InSTORE,Death Notification</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MACYS_HR_Flag__c</field>
            <operation>equals</operation>
            <value>Y</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MACYS_CaseSubCategory1__c</field>
            <operation>notEqual</operation>
            <value>First appeal,Second appeal,401(K) and Pension</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>notEqual</operation>
            <value>Case Import,Internal</value>
        </criteriaItems>
        <description>This rule will schedule the Case for auto closure if the status is pending customer action and the information required from customer is of low or medium priority</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>MACYS_CaseCustomerActionRequiredNotification</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.MACYS_FirstFollowUp__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>MACYS_CaseCustomerActionRequiredNotification</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.MACYS_SecondFollowUp__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>MACYS_ScheduleCaseForAutoCloseMedLowFieldUpdate</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Pending Customer Action,Pending paperwork from employee</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>notEqual</operation>
            <value>Critical</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MACYS_CaseCategory__c</field>
            <operation>notEqual</operation>
            <value>Solutions InSTORE,Death Notification</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MACYS_CaseSubCategory1__c</field>
            <operation>notEqual</operation>
            <value>First appeal,Second appeal,401(K) and Pension</value>
        </criteriaItems>
        <description>This rule will schedule the Case for auto closure if the status is pending customer action and the information required from customer is of low or medium priority (split based on the HR/Non HR criteria</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>MACYS_UpdateCaseStatusClosed</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.MACYS_SecondFollowUp__c</offsetFromField>
            <timeLength>8</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>MACYS_SendEmailForHR</fullName>
        <actions>
            <name>MACYS_SendEmailToHR</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.MACYS_HRManagerConfirmation__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>notEqual</operation>
            <value>Case Import,Internal</value>
        </criteriaItems>
        <description>When agent  select &quot;May we inform HR Rep at your location?&quot;, then send the email to HR Representative.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MACYS_SetAdHocReportRequestPriority</fullName>
        <actions>
            <name>MACYS_AdhocReportUpdatePriorityCritical</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>The priority of the Ad Hoc Report Request field will be set to critical, if it is a legal requirement or raised by VP and above</description>
        <formula>AND( 	ISPICKVAL( MACYS_CaseCategory__c , &quot;Ad Hoc HR Report&quot;) , 	OR(   		ISNEW(),  		AND(ISCHANGED( MACYS_ReportRequestedToMeetLegalReqs__c ), ISPICKVAL(MACYS_ReportRequestedToMeetLegalReqs__c,&quot;yes&quot;)),   		AND(ISCHANGED( MACYS_ReportRequestedForGVPorAbove__c ),  ISPICKVAL(MACYS_ReportRequestedForGVPorAbove__c,&quot;yes&quot;)) 		) 	)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>MACYS_StatusClosedRO</fullName>
        <actions>
            <name>MACYS_RecordTypeChangeForGIClosedCase</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>General Inquiry</value>
        </criteriaItems>
        <description>When the General Inquiry case status is changed to closed, then change the record type to &quot;General Inquiry RO Close&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MACYS_SystemSupportRO</fullName>
        <actions>
            <name>MACYS_SystemSupportRO</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>HR System Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <description>When the System Support case status is changed to closed, then change the record type to &quot; RO Close&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MACYS_UpdateCaseRecordType</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <description>When the case is closed, it will update the closed record type</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MACYS_UpdateEmailForHR</fullName>
        <actions>
            <name>MACYS_HRManagerEmailUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When the Case is created, update the HR manager Id field. Which can be used to send notification to the contacts HR manager</description>
        <formula>ISNEW()</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>MACYS_UpdateIsFirstCallResolution</fullName>
        <actions>
            <name>MACYS_UpdateFirstCallResolution</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When the Case status changes from Open to Closed, update the first call resolution flag to true</description>
        <formula>AND(OR(CONTAINS(Owner:User.UserRole.Name, &apos;Contact Center&apos;),Owner:Queue.QueueName==&apos;Contact Center&apos;), 
OR(  AND(  ISNEW() ,  ISPICKVAL(Status, &quot;Closed&quot;)  ),  AND(  ISPICKVAL(PRIORVALUE(Status),&quot;Open&quot;),  ISPICKVAL(Status, &quot;Closed&quot;)  ) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>MACYS_UpdatePriorityEmpCompliance</fullName>
        <actions>
            <name>MACYS_UpdatePriorityEmpCompliance</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Employment Compliance</value>
        </criteriaItems>
        <description>This workflow rule is used to update the priority of the employment compliance record to  high</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>MACYS_UpdateSISCaseNumberForStep1</fullName>
        <actions>
            <name>MACYS_UpdateSISCaseNumberForStep1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Solutions InSTORE Step 1</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MACYS_UpdateSISCaseNumberForStep2</fullName>
        <actions>
            <name>MACYS_UpdateSISCaseNumberForStep2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Solutions InSTORE Step 2</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MACYS_UpdateSISCaseNumberForStep3</fullName>
        <actions>
            <name>MACYS_UpdateSISCaseNumberForStep3</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Solutions InSTORE Step 3</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MACYS_UpdateSISCaseNumberForStep4</fullName>
        <actions>
            <name>MACYS_UpdateSISCaseNumberForStep4</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Solutions InSTORE Step 4</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MACYS_ValuesOnBasesOfSourceField</fullName>
        <actions>
            <name>MACYS_HavyouspokentoManagementValueUpdat</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>MACYS_HowdidyouhearaboutSISValueUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>MACYS_MayweinformHRRepValueUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.MACYS_Source__c</field>
            <operation>equals</operation>
            <value>Compliance Connection,Tell Us What You Think,CEO/Executive Committee Letter</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update SIS Case Number</fullName>
        <actions>
            <name>MACYS_UpdateSISCaseNumber</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>SIS Step 1,SIS Step 2,SIS Step 3,SIS Step 4</value>
        </criteriaItems>
        <description>This workflow is triggered when the SIS case is created and appends the suffix to the case number</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
