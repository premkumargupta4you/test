<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>MACYS_InactivateUser</fullName>
        <description>When the termination date is set, user will be inactivated from salesforce post 13 months from termination</description>
        <field>IsActive</field>
        <literalValue>0</literalValue>
        <name>Inactivate the User</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>MACYS_InactivateUserRecord</fullName>
        <active>true</active>
        <description>Rule will inactivate the user record when the inactivation date is reached</description>
        <formula>NOT(ISBLANK( MACYS_UserInactivateDate__c ))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>MACYS_InactivateUser</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>User.MACYS_UserInactivateDate__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
