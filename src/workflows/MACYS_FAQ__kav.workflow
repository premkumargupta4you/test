<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>MACYS_FAQEmailtoOwner</fullName>
        <description>Send Email to Article owner when the field is checked</description>
        <protected>false</protected>
        <recipients>
            <field>MACYS_ArticleOwner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>MACYs_EmailTemplates/MACYS_ReceiveEmailNotificationAboutContentChangeFAQ</template>
    </alerts>
    <fieldUpdates>
        <fullName>MACYS_FAQExpiryDateUpdate</fullName>
        <field>MACYS_ExpiredDate__c</field>
        <formula>IF(MONTH(TODAY())&gt;6, DATE(YEAR(TODAY())+1,1,31),if(MONTH(TODAY())==1,DATE(YEAR(TODAY()),1,31),DATE(YEAR(TODAY()),6,30)))</formula>
        <name>FAQ Expiry Date Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MACYS_FAQExpiryDateUpdateOtherOwner</fullName>
        <field>MACYS_ExpiredDate__c</field>
        <formula>IF(MONTH(TODAY())&gt;4, DATE(YEAR(TODAY())+1,4,30),DATE(YEAR(TODAY()),4,30))</formula>
        <name>FAQ Expiry Date Update Other Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MACYS_UncheckforSendEmailtoOwnerFAQ</fullName>
        <field>MACYS_SendEmailtoOwner__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck for &apos;Send Email to Owner&apos; FAQ</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <knowledgePublishes>
        <fullName>MACYS_PublishArticle</fullName>
        <action>Publish</action>
        <description>Publish article when loaded via import wizard</description>
        <label>Publish Article</label>
        <language>en_US</language>
        <protected>false</protected>
    </knowledgePublishes>
    <rules>
        <fullName>MACYS_FAQEmailtoOwner</fullName>
        <actions>
            <name>MACYS_FAQEmailtoOwner</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MACYS_UncheckforSendEmailtoOwnerFAQ</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>MACYS_FAQ__kav.MACYS_SendEmailtoOwner__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>It will send the email to article owner if the field &quot;Send Email to Owner&quot; is checked</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MACYS_FAQExpiryDate</fullName>
        <actions>
            <name>MACYS_FAQExpiryDateUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>MACYS_FAQ__kav.MACYS_ArticleOwner__c</field>
            <operation>equals</operation>
            <value>Alison Payne</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>MACYS_FAQExpiryDateOtherOwner</fullName>
        <actions>
            <name>MACYS_FAQExpiryDateUpdateOtherOwner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>MACYS_FAQ__kav.MACYS_ArticleOwner__c</field>
            <operation>notEqual</operation>
            <value>Alison Payne</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Publish FAQ Article</fullName>
        <actions>
            <name>MACYS_PublishArticle</name>
            <type>KnowledgePublish</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.FirstName</field>
            <operation>equals</operation>
            <value>HR</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.LastName</field>
            <operation>equals</operation>
            <value>Admin</value>
        </criteriaItems>
        <description>Publish the FAQ article when loaded via import wizard</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
