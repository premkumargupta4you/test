<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>MACYS_OtherRefMatEmailtoOwner</fullName>
        <description>Send Email to Article owner when the field is checked</description>
        <protected>false</protected>
        <recipients>
            <field>MACYS_ArticleOwner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>MACYs_EmailTemplates/MACYS_ReceiveEmailNotificationAboutContentChangeOther</template>
    </alerts>
    <fieldUpdates>
        <fullName>MACYS_ExpiryDateUpdate</fullName>
        <field>MACYS_ExpiredDate__c</field>
        <formula>IF(MONTH(TODAY())&gt;6, DATE(YEAR(TODAY())+1,1,31),if(MONTH(TODAY())==1,DATE(YEAR(TODAY()),1,31),DATE(YEAR(TODAY()),6,30)))</formula>
        <name>Expiry Date Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MACYS_ExpiryDateUpdateWithoutOwner</fullName>
        <field>MACYS_ExpiredDate__c</field>
        <formula>IF(MONTH(TODAY())&gt;4, DATE(YEAR(TODAY())+1,4,30),DATE(YEAR(TODAY()),4,30))</formula>
        <name>Expiry Date Update Without Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MACYS_UncheckforSendEmailtoOwnerMat</fullName>
        <field>MACYS_SendEmailtoOwner__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck for &apos;Send Email to Owner&apos; Mat</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <knowledgePublishes>
        <fullName>MACYS_PublishArticle</fullName>
        <action>Publish</action>
        <description>Publish the other ref material article using import wizard</description>
        <label>Publish Article</label>
        <language>en_US</language>
        <protected>false</protected>
    </knowledgePublishes>
    <rules>
        <fullName>MACYS_OtherRefMatEmailtoOwner</fullName>
        <actions>
            <name>MACYS_OtherRefMatEmailtoOwner</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MACYS_UncheckforSendEmailtoOwnerMat</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>MACYS_OtherReferenceMaterial__kav.MACYS_SendEmailtoOwner__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>It will send the email to article owner if the field &quot;Send Email to Owner&quot; is checked</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MACYS_OtherreferenceExpiryDate</fullName>
        <actions>
            <name>MACYS_ExpiryDateUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>MACYS_OtherReferenceMaterial__kav.MACYS_ArticleOwner__c</field>
            <operation>equals</operation>
            <value>Alison Payne</value>
        </criteriaItems>
        <description>updating expiry date of article.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>MACYS_OtherreferenceExpiryDateOtherOwner</fullName>
        <actions>
            <name>MACYS_ExpiryDateUpdateWithoutOwner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>MACYS_OtherReferenceMaterial__kav.MACYS_ArticleOwner__c</field>
            <operation>notEqual</operation>
            <value>Alison Payne</value>
        </criteriaItems>
        <description>updating expiry date of article.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Publish Other Reference Material Article</fullName>
        <actions>
            <name>MACYS_PublishArticle</name>
            <type>KnowledgePublish</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.FirstName</field>
            <operation>equals</operation>
            <value>HR</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.LastName</field>
            <operation>equals</operation>
            <value>Admin</value>
        </criteriaItems>
        <description>Publish the other ref material article using import wizard</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
