<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>MACYS_DTPEmailtoOwner</fullName>
        <description>It will send the email to article owner if the field &quot;Send Email to Owner&quot; is checked</description>
        <protected>false</protected>
        <recipients>
            <field>MACYS_ArticleOwner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>MACYs_EmailTemplates/MACYS_ReceiveEmailNotificationAboutContentChangeDTP</template>
    </alerts>
    <fieldUpdates>
        <fullName>MACYS_DTPExpiryDateUpdate</fullName>
        <field>MACYS_ExpiredDate__c</field>
        <formula>IF(MONTH(TODAY())&gt;6, DATE(YEAR(TODAY())+1,1,31),if(MONTH(TODAY())==1,DATE(YEAR(TODAY()),1,31),DATE(YEAR(TODAY()),6,30)))</formula>
        <name>DTP Expiry Date Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MACYS_DTPExpiryDateUpdateOtherOwner</fullName>
        <field>MACYS_ExpiredDate__c</field>
        <formula>IF(MONTH(TODAY())&gt;4, DATE(YEAR(TODAY())+1,4,30),DATE(YEAR(TODAY()),4,30))</formula>
        <name>DTP Expiry Date Update Other Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MACYS_UncheckforSendEmailtoOwnerDTP</fullName>
        <field>MACYS_SendEmailtoOwner__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck for &apos;Send Email to Owner&apos; DTP</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <knowledgePublishes>
        <fullName>MACYS_PublishArticle</fullName>
        <action>Publish</action>
        <description>Publish the DTP article loaded via import wizard</description>
        <label>Publish Article</label>
        <language>en_US</language>
        <protected>false</protected>
    </knowledgePublishes>
    <rules>
        <fullName>MACYS_DTPEmailtoOwner</fullName>
        <actions>
            <name>MACYS_DTPEmailtoOwner</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MACYS_UncheckforSendEmailtoOwnerDTP</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>MACYS_DTP__kav.MACYS_SendEmailtoOwner__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>It will send the email to article owner if the field &quot;Send Email to Owner&quot; is checked</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MACYS_DTPExpiryDate</fullName>
        <actions>
            <name>MACYS_DTPExpiryDateUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>MACYS_DTP__kav.MACYS_ArticleOwner__c</field>
            <operation>equals</operation>
            <value>Alison Payne</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>MACYS_DTPExpiryDateOtherOwner</fullName>
        <actions>
            <name>MACYS_DTPExpiryDateUpdateOtherOwner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>MACYS_DTP__kav.MACYS_ArticleOwner__c</field>
            <operation>notEqual</operation>
            <value>Alison Payne</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Publish DTP Article</fullName>
        <actions>
            <name>MACYS_PublishArticle</name>
            <type>KnowledgePublish</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.FirstName</field>
            <operation>equals</operation>
            <value>HR</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.LastName</field>
            <operation>equals</operation>
            <value>Admin</value>
        </criteriaItems>
        <description>Publish the DTP article loaded via import wizard</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
