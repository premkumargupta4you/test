<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>MACYS_UpdateCreateModeToManual</fullName>
        <field>MACYS_CreateMode__c</field>
        <literalValue>Manual</literalValue>
        <name>Update Create Mode to Manual</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MACYS_UpdateEmployeeNumber</fullName>
        <description>It will be created on contact creation. It will be overridden when PSFT will update the employee number. It is visible to HR admin.</description>
        <field>MACYS_EmployeeNumber__c</field>
        <formula>MACYS_EmployeeAutoNumber__c</formula>
        <name>MACYS Update Employee Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>MACYS_AutoEmployeeNumber</fullName>
        <actions>
            <name>MACYS_UpdateCreateModeToManual</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>MACYS_UpdateEmployeeNumber</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.CreatedById</field>
            <operation>notEqual</operation>
            <value>HR Admin</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MACYS_CreateMode__c</field>
            <operation>notEqual</operation>
            <value>Case Import</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MACYS_AutoUpdateEmployeeNo__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>It will be created on contact creation. It will be overridden when PSFT will update the employee number. It is visible to HR admin.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
