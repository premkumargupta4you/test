/******************************************************************************************
    * Create By     :     Deloitte Consulting.
    * Create Date   :     5/10/2016         
    * Description   :     Test Class for Case Trigger 'MACYS_Case' and MACYS_CaseTriggerHelper class

    *     Modification Log:
    *    -----------------------------------------------------------------------------
    *    * Developer                   Date               Description
    *    * ----------------------------------------------------------------------------                 
    *    * Puneet Jalota               5/10/2016           
******************************************************************************************/
@isTest
private class MACYS_CaseTriggerTest{

    public static MACYS_EntitlementData__c entitleSetting=MACYS_TestUtility.creatingMACYS_EntitlementData('1', '', '', '', false, true,'Medium',480,120);
    public static MACYS_EntitlementData__c entitleCriticalSetting=MACYS_TestUtility.creatingMACYS_EntitlementData('2', '', '', '', false, true,'Critical',480,120);
    public static MACYS_EntitlementData__c entitleHighSetting=MACYS_TestUtility.creatingMACYS_EntitlementData('3', '', '', '', false, true,'High',480,120);
    public static MACYS_EntitlementData__c entitleLowSetting=MACYS_TestUtility.creatingMACYS_EntitlementData('4', '', '', '', false, true,'Low',480,120);
    public static MACYS_EntitlementData__c entitleSISSetting=MACYS_TestUtility.creatingMACYS_EntitlementData('5', 'Solutions InStore','','',false, false,'',0,0);
    public static MACYS_EntitlementData__c entitleBIFASetting=MACYS_TestUtility.creatingMACYS_EntitlementData('6','Benefit Inquiry and Transaction', 'First appeal', '', false, true, '',28800,960);
    public static MACYS_EntitlementData__c entitleBISASetting=MACYS_TestUtility.creatingMACYS_EntitlementData('7','Benefit Inquiry and Transaction', 'Second appeal', '', false, true, '',28800,960);
    public static MACYS_EntitlementData__c entitleDN1Setting=MACYS_TestUtility.creatingMACYS_EntitlementData('8','Death Notification', 'Active Employee Death Notification', '', false, true, '',9600,120);
    public static MACYS_EntitlementData__c entitleDN2Setting=MACYS_TestUtility.creatingMACYS_EntitlementData('9','Death Notification', 'Former Employee Death Notification', '', false, true,'',9600,120);
    public static MACYS_EntitlementData__c entitleDN3Setting=MACYS_TestUtility.creatingMACYS_EntitlementData('10','Death Notification', 'Dependent Death Notification', '', false, true,'',9600,120);
    public static MACYS_EntitlementData__c entitleDN4Setting=MACYS_TestUtility.creatingMACYS_EntitlementData('11','Death Notification', 'Additional notification', '', true, true,'',9600,120);
    public static BusinessHours businessHour=[SELECT Id,IsActive,TimeZoneSidKey FROM BusinessHours Where IsActive = true and TimeZoneSidKey='America/Chicago' limit 1];
   
    public static MACYS_SubCat1WithoutSubCat2__c subcatsetting=MACYS_TestUtility.creatingMACYS_SubCat1WithoutSubCat2();
    
    /*******************************************************************************************
    * Method         :  testCaseInsert
    * Description    :  This test methood is used to check Case trigger and helper class.
    * Parameter      :  None
    * Return Type    :  None
    *******************************************************************************************/
    
    private static testmethod void testassociateEntitlementProcessandaddCaseCreator(){
    
        User runningUser=MACYS_TestUtility.createUser('rKumar','System Administrator');
        
        
        
        Contact con=MACYS_TestUtility.creatingContact(runningUser,'2345');
        
        List<MACYS_CaseSLAMappingWithPriority__c> customset=new List<MACYS_CaseSLAMappingWithPriority__c>();
        customset=[select id from MACYS_CaseSLAMappingWithPriority__c];
        if(!customset.isEmpty()){
            delete customset;
        }
        
        Test.startTest();
        MACYS_CaseSLAMappingWithPriority__c  custSetObj1= MACYS_TestUtility.createCustomSettingForArticle();
        Test.stopTest();
        
        
        List<Case> casesToAssociateEntitlement=new List<Case>();
        Id caseGeneralInquiryRecordTypeId=Schema.SObjectType.Case.getRecordTypeInfosByName().get('General Inquiry').getRecordTypeId();
        Id caseSISStepOneRecordTypeId=Schema.SObjectType.Case.getRecordTypeInfosByName().get('Solutions InSTORE Step 1').getRecordTypeId();
        
        
        System.runAs(runningUser) {
            
            Account acc = new Account(Name='testacc');
            insert acc;
            
      
            
            MACYS_Locations__c dummyLocation=new MACYS_Locations__c();
            dummyLocation.Location__c='01525 - MST - Roosevelt Mall (PA)';
            insert dummyLocation;
            
            Case case1=new Case();
            case1.MACYS_IsAnonymous__c=true;
            //case1.MACYS_OnBehalfOf__c=contact.id;
            case1.status='open';
            case1.Priority='Medium';
            case1.MACYS_SISCaseLocation__c=dummyLocation.id;
            case1.recordtypeid=caseGeneralInquiryRecordTypeId;
            case1.Origin ='Phone';
            casesToAssociateEntitlement.add(case1);
            
            Case case2=new Case();
            case2.MACYS_IsAnonymous__c=true;
            case2.MACYS_SISCaseLocation__c=dummyLocation.id;
            case2.status='open';
            case2.Priority='Medium';
            case2.MACYS_CaseCategory__c='Solutions InSTORE';
            case2.recordtypeid=caseSISStepOneRecordTypeId;
            case2.MACYS_SISStep__c='Step-4';
            case2.Origin ='Phone';
            casesToAssociateEntitlement.add(case2);
            
            insert casesToAssociateEntitlement;
            
            List<Case> casesToverify=[select id,EntitlementId,MACYS_CaseCategory__c from case where id in :casesToAssociateEntitlement];
            
            
           
           List<CaseTeamMember> caseTeamList=[select MemberId from CaseTeamMember where ParentId in :casesToAssociateEntitlement];
           
           for(CaseTeamMember teammember: caseTeamList){
               System.assertEquals(teammember.MemberId , runningUser.Id);
           }
            
        }
        
    }
    
    private static testmethod void testupdateCaseOwner(){
        User runningUserManager=MACYS_TestUtility.createUser('PKumar','System Administrator');
        User runningUser=MACYS_TestUtility.createUser('rKumar','System Administrator');
        runningUser.ManagerId=runningUserManager.id;
        update runningUser;
        
        Contact con=MACYS_TestUtility.creatingContact(runningUser,'12345');
        List<Case> casesToUpdateOwner=new List<Case>();
        
        System.runAs(runningUser) {
        
            MACYS_Locations__c dummyLocation=new MACYS_Locations__c();
            dummyLocation.Location__c='01525 - MST - Roosevelt Mall (PA)';
            insert dummyLocation;
        
        
            Case case1=new Case();
            case1.MACYS_CaseSubStatus__c='Pending Action Violated';
            case1.MACYS_IsAnonymous__c=true;
            case1.Priority='Medium';
            case1.MACYS_SISCaseLocation__c=dummyLocation.id;
            case1.Origin ='Phone';
            //case1.BusinessHoursId=businessHour.id;
            casesToUpdateOwner.add(case1);
            
            insert casesToUpdateOwner;
            
            casesToUpdateOwner.clear();
            
            case1.MACYS_CaseSubStatus__c='Resolution SLA Violated';
            casesToUpdateOwner.add(case1);
            
            update casesToUpdateOwner;
            
            system.debug('owner time zone'+runningUser.TimeZoneSidKey);
            Case casetoverify=[select id,OwnerId,BusinessHoursId from case where id=: case1.id];
            BusinessHours bhr=[SELECT Id,IsActive,TimeZoneSidKey FROM BusinessHours Where IsActive = true and TimeZoneSidKey=:runningUser.TimeZoneSidKey limit 1];
            
            System.assertEquals(casetoverify.OwnerId , runningUserManager.Id);
            //System.assertEquals(casetoverify.BusinessHoursId, bhr.Id);
            system.debug('time zone'+casetoverify.BusinessHoursId);
        }
    }
    
    private static testmethod void testupdateFollowUpDates(){
    
    
        MACYS_Locations__c dummyLocation=new MACYS_Locations__c();
        dummyLocation.Location__c='01525 - MST - Roosevelt Mall (PA)';
        insert dummyLocation;
            
        MACYS_CaseFollowUpDays__c followupCustomSetting=MACYS_TestUtility.createCustomSettingForFollowUpDays();
        List<Case> casesToUpdateFollowUpDates=new List<Case>();
        
        Case case1=new Case();
        case1.Status='Open';
        case1.Priority='Medium';
        case1.MACYS_IsAnonymous__c=true;
        case1.BusinessHoursId=businessHour.id;
        case1.Origin ='Phone';
        case1.MACYS_SISCaseLocation__c=dummyLocation.id;
        casesToUpdateFollowUpDates.add(case1);
        
        insert casesToUpdateFollowUpDates;
        
        
        casesToUpdateFollowUpDates.clear();
        
        case1.status='Pending Customer Action';
        casesToUpdateFollowUpDates.add(case1);
        
        update casesToUpdateFollowUpDates;
        
        Case casetoverify=[select id,MACYS_FirstFollowUp__c, MACYS_SecondFollowUp__c from case where id=: case1.id];
        System.assert(casetoverify.MACYS_FirstFollowUp__c!=null);
        System.assert(casetoverify.MACYS_SecondFollowUp__c!=null);
    }
    
    private static testmethod void testupdateMilestoneStatus(){
    
        MACYS_CaseFollowUpDays__c followupCustomSetting=MACYS_TestUtility.createCustomSettingForFollowUpDays();
        
        Account acc = new Account(Name='testacc');
        insert acc;
        
       
        
        MACYS_Locations__c dummyLocation=new MACYS_Locations__c();
        dummyLocation.Location__c='01525 - MST - Roosevelt Mall (PA)';
        insert dummyLocation;
        
        List<Case> casesToUpdateFollowUpDates=new List<Case>();
        
        Case case1=new Case();
        case1.Status='Open';
        case1.Priority='Medium';
        case1.MACYS_IsAnonymous__c=true;
        case1.BusinessHoursId=businessHour.id;
        case1.MACYS_SISCaseLocation__c=dummyLocation.id;
        case1.Origin = 'Phone';
        casesToUpdateFollowUpDates.add(case1);
        
        insert casesToUpdateFollowUpDates;
        
        List<ID> caseIDs=new List<ID>();
        
        for(Case cse:casesToUpdateFollowUpDates){
            caseIDs.add(cse.id);
        }
        
        
        casesToUpdateFollowUpDates.clear();
        
        case1.status='Closed';
        case1.MACYS_CaseResolutionNotes__c='Closed';
        casesToUpdateFollowUpDates.add(case1);
        
        update casesToUpdateFollowUpDates;
        
        CaseComment commentsOnClosedcase=new CaseComment();
        commentsOnClosedcase.parentid=case1.id;
        
        try{
            insert commentsOnClosedcase;
            commentsOnClosedcase.CommentBody = 'Test Update';
            
            update commentsOnClosedcase;
        }
        catch(exception e){
        }
        List<CaseComment> caseCommentList= [select id,CommentBody from CaseComment limit 1];
        
        for(CaseComment cc: caseCommentList){
            cc.CommentBody = 'abc1';
        }
        
        try{
            update caseCommentList;
        }
        catch(exception e){
        }
        
        
    }
    
    private static testmethod void testsetSISFormData(){
        MACYS_Locations__c dummyLocation=new MACYS_Locations__c();
        dummyLocation.Location__c='01525 - MST - Roosevelt Mall (PA)';
        insert dummyLocation;
        
        Case case1=new Case();
        case1.Status='Open';
        case1.Priority='Medium';
        case1.MACYS_IsAnonymous__c=true;
        case1.MACYS_PreferredModeOfContact__c='Other';
        case1.BusinessHoursId=businessHour.id;
        case1.Origin ='Phone';
        case1.MACYS_SISCaseLocation__c=dummyLocation.id;
        insert case1;
        
        Case case2=new Case();
        case2.Status='Open';
        case2.MACYS_IsAnonymous__c=true;
        case2.MACYS_RelatedCase__c=case1.id;
        case2.BusinessHoursId=businessHour.id;
        case2.Origin ='Phone';
        case2.MACYS_SISCaseLocation__c=dummyLocation.id;
        insert case2;
        
        Case casetoverify=[select id,MACYS_PreferredModeOfContact__c from case where id=: case2.id];
        System.assertEquals(casetoverify.MACYS_PreferredModeOfContact__c, case1.MACYS_PreferredModeOfContact__c);  
    }
    
    private static testmethod void testStep2FieldPopulationIfCreatedDirectly(){
        
        
        
        List<MACYS_CaseSLAMappingWithPriority__c> customset=new List<MACYS_CaseSLAMappingWithPriority__c>();
        customset=[select id from MACYS_CaseSLAMappingWithPriority__c];
        if(!customset.isEmpty()){
            delete customset;
        }
        
        Test.startTest();
        MACYS_CaseSLAMappingWithPriority__c  custSetObj1= MACYS_TestUtility.createCustomSettingForArticle();
        Test.stopTest();
        
        User runningUser=MACYS_TestUtility.createUser('rKumar','System Administrator');
        Id caseSISStepTwoRecordTypeId=Schema.SObjectType.Case.getRecordTypeInfosByName().get('Solutions InSTORE Step 2').getRecordTypeId();
        Contact con=MACYS_TestUtility.creatingContact(runningUser,'2345');
        system.debug('sdwwrw'+con.id);
        System.runAs(runningUser) {
            Case case1=new Case();
            case1.Status='Open';
            case1.recordtypeid=caseSISStepTwoRecordTypeId;
            case1.Priority='Medium';
            case1.MACYS_SISStep__c='Step-4';
            case1.ContactId=con.id;
            case1.BusinessHoursId=businessHour.id;
            insert case1;
            
            system.debug('wcwdcwdcw'+case1.contactid+case1.MACYS_RelatedCase__c+case1.recordtypeid);
            
            Case casetoverify=[select id,MACYS_City__c from case where id=: case1.id];
            System.assertEquals(casetoverify.MACYS_City__c , con.MailingCity);
        }
         
    }
    
    private static testmethod void testSLATimeFields(){
        User runningUserManager=MACYS_TestUtility.createUser('PKumar','System Administrator');
        User runningUser=MACYS_TestUtility.createUser('rKumar','System Administrator');
        runningUser.ManagerId=runningUserManager.id;
        update runningUser;
        
        Contact con=MACYS_TestUtility.creatingContact(runningUser,'12345');
        list<Case> lstCase = new list<Case>();
        
        System.runAs(runningUser) {
            Case cse=new Case();
            cse.MACYS_CaseCategory__c = 'General Inquiry';
            cse.MACYS_CaseSubCategory1__c = 'Absence';
            cse.MACYS_CaseSubCategory2__c = 'Other'; 
            cse.Priority = 'Medium';
            cse.ContactId = con.Id;
            cse.Subject = 'Test SLA';
            cse.Status='Open';
            cse.Origin = 'Phone';
            cse.BusinessHoursId=businessHour.id;
            lstCase.add(cse);
            
            Case cse1=new Case();
            cse1.MACYS_CaseCategory__c = 'Death Notification';
            cse1.MACYS_CaseSubCategory1__c = 'Additional notification';
            cse1.Priority = 'Medium';
            cse1.ContactId = con.Id;
            cse1.Subject = 'Test SLA';
            cse1.Status='Open';
            cse1.Origin = 'Phone';
            cse1.BusinessHoursId=businessHour.id;
            lstCase.add(cse1);
            
            insert lstCase;
            
            system.debug('owner time zone'+runningUser.TimeZoneSidKey+'kdkdkala'+cse.Status);
            Case objCase = [select id,OwnerId,BusinessHoursId,MACYS_ResolutionTime__c from case where id=: cse.id];
            
            //System.assertEquals(objCase.MACYS_ResolutionTime__c , BusinessHours.Add(businessHour.id, System.now(), 960*60*1000L));
            lstCase=new List<Case>();
            cse.Status='Pending Customer Action';
            cse1.Status='Pending Customer Action';
            lstCase.add(cse);
            lstCase.add(cse1);
            update lstCase;
                        
            lstCase=new List<Case>();
            cse.Status='Open';
            cse1.Status='Open';
            lstCase.add(cse);
            lstCase.add(cse1);
            update lstCase;
        }
    }
     private static testmethod void testInvestigatorFacililitator(){
       
        User investigator = MACYS_TestUtility.createUser('PInv','Macys HR Community Employee');
        Contact investContact=MACYS_TestUtility.creatingContact(investigator,'1234'); 
        User facilitator = MACYS_TestUtility.createUser('PFac','Macys HR Community Employee');
        Contact faciContact=MACYS_TestUtility.creatingContact(facilitator,'2345');
        User secfacilitator = MACYS_TestUtility.createUser('PFac1','Macys HR Community Employee');
        Contact secfaciContact=MACYS_TestUtility.creatingContact(facilitator,'2346');
         
        Id caseSISStepThreeRecordTypeId=Schema.SObjectType.Case.getRecordTypeInfosByName().get('Solutions InSTORE Step 3').getRecordTypeId();
        
         Case case1;
         case1=new Case();
         case1.origin='email';
         case1.Status='Open';
         case1.recordtypeid=caseSISStepThreeRecordTypeId;
         case1.Priority='Medium';
         case1.MACYS_SISStep__c='Step-4';
         case1.MACYS_ClaimTypeAndReason__c ='CIC - Commission Pay';
         case1.ContactId=investContact.id;
         case1.MACYS_FacilitatorName__c = faciContact.Id;
         case1.BusinessHoursId=businessHour.id;
        
        
        insert case1;
         system.debug('case1===='+case1.Id);
         
        CaseTeamRole ctr = new CaseTeamRole();
        
        
         ctr=[select Id, AccessLevel from CaseTeamRole where Name like 'Macys Facilitators' limit 1];

         
         
         List<CaseTeamMember> ctmList = [select TeamRoleId from CaseTeamMember where TeamRoleId=:ctr.Id];
         
         System.assertEquals(ctmList[0].TeamRoleId , ctr.Id);
         
         delete ctmList;
        
         Case objCase = [select id,OwnerId,MACYS_Decision__c,MACYS_DecisionSettlement__c,MACYS_DecisionDate__c,MACYS_DecisionStatus__c,MACYS_FacilitatorName__c from case where id=: case1.id];
         objCase.OwnerId = investigator.Id;
         objCase.MACYS_Decision__c = 'Grant';
         objCase.MACYS_DecisionSettlement__c = 'Test';
         objCase.MACYS_DecisionDate__c = Date.today();
         objCase.MACYS_DecisionStatus__c = 'Accept';
         objCase.MACYS_PeerPanelDate__c = Date.today();
         objCase.MACYS_FacilitatorName__c = secfaciContact.Id;
         
         update objCase;
         
        
         
         objCase = [select id,MACYS_Decision__c,MACYS_DecisionSettlement__c,MACYS_DecisionDate__c,MACYS_DecisionStatus__c from case where id=: case1.id];
         objCase.MACYS_Decision__c = 'Modify';
         objCase.MACYS_DecisionSettlement__c = 'Test';
         objCase.MACYS_DecisionDate__c = Date.today();
         objCase.MACYS_DecisionStatus__c = 'Decline';
         
         update objCase;
         
         
     }
     
     private static testmethod void testAddingCommentsFromPreviousStep(){
        
        User runningUser=MACYS_TestUtility.createUser('rKumar','System Administrator');

        Contact con=MACYS_TestUtility.creatingContact(runningUser,'2345');
        
         Case cse=new Case();
         cse.Contactid=con.id;
         cse.MACYS_SISStep__c='Step-1';
         cse.status='Open';
         cse.Origin = 'Phone';
         insert cse;
         
         CaseComment newComm=new CaseComment();
         newComm.CommentBody='abc';
         newComm.ParentId=cse.Id;
         newComm.IsPublished=true;
         
         insert newComm;
         
         Case cse1=new Case();
         cse1.Contactid=con.id;
         cse1.MACYS_SISStep__c='Step-2';
         cse1.MACYS_RelatedCase__c=cse.id;
         cse1.status='Open';
         cse1.Origin = 'Phone';
         insert cse1;
     }
      
}