/******************************************************************************************
    * Create By     :     Deloitte Consulting.
    * Create Date   :     6/21/2016         
    * Description   :     A Class for sending Email for Article Type

    *     Modification Log:
    *    -----------------------------------------------------------------------------
    *    * Developer                   Date               Description
    *    * ----------------------------------------------------------------------------                 
    *    * Prem Kumar Gupta            3/31/2016          Initial version.
******************************************************************************************/
public class MACYS_EmailTemplateForArticle {
    
    /*******************************************************************************************
    * Method         :  sendEmail
    * Description    :  This is method which is called from MACYS_ArticleBatchClass used to Send Email
    * Parameter      :  lsArticle - list<MACYS_ArticleType__kav> 
                        typeOfAction - String
    * Return Type    :  None
    ******************************************************************************************/
    public static void sendEmail(list<sobject> lsArticle,String typeOfAction){
        
        System.debug('lsArticle in sendEmail() ->'+lsArticle);
        System.debug('typeOfAction in sendEmail() ->'+typeOfAction);
        
        MACYS_CaseSLAMappingWithPriority__c custSetObj = MACYS_CaseSLAMappingWithPriority__c.getOrgDefaults(); 
        String imageUrl = custSetObj.MACYS_ArticleHeaderURL__c ;
        
        list<Messaging.SingleEmailMessage> listmsg=new list<Messaging.SingleEmailMessage>();

        if(typeOfAction.equalsIgnoreCase(custSetObj.MACYS_ArchivedArticle__c)){  // checking if the article is Archived
            for(sobject article : lsArticle){
                String ownerId =(String) article.get('MACYS_ArticleOwner__c'); // MACYS_ArticleOwner__c changed from Ownerid
                String ArticleNumber =(String) article.get('ArticleNumber');
                String Title =(String) article.get('Title');
                String createdByname =(String) article.get('MACYS_ArticleOwner__c');
                String ownerName = getAccOwnerName(article);
                String UrlName =(String) article.get('UrlName');
                String ArticleType =(String) article.get('ArticleType');
                String url = getArticleUrl(ArticleType,UrlName,custSetObj);
                
                
                if(String.isNotBlank(createdByname)){
                    Messaging.SingleEmailMessage msg=new Messaging.SingleEmailMessage();
                    msg.setTargetObjectId(ownerId); // mail to the owner of article
                    msg.setSubject(ArticleNumber +': "'+Title +'" is Archived');
                    
                    String body = ' <apex:page >'+
                                        '<div align="right"><img border="0" src="'+imageUrl+'" /></div>'+
                                        '</br>'+
                                        'Dear '+ownerName +','+
                                        '</br></br>'+
                                        'This notification serves to inform you that <b>'+ArticleNumber+'</b> - "'+Title+'" is archived. You are receiving this notification because you are listed as the Content Owner for this article. '+
                                        '</br></br>'+
                                        'Action required from you: Please review the article <b>"'+Title+'"</b> to confirm if any content / meta data updates are required. You are responsible for gathering any necessary approvals required to publish an updated Article.'+
                                        '</br></br>'+
                                        'If you have any additional questions, please contact <b>Employee Service</b>'+
                                        '</br></br>'+
                                        'Regards,</br>'+
                                        'Macy\'s Employee Services'+
                                        '</br></br>'+
                                        'This is an automated email – please do not reply to this mailbox as we will not receive your message.'+
                                    '</apex:page>';
                    msg.setHtmlBody(body);
                    msg.setSaveAsActivity(false); 
                    listmsg.add(msg);
                }
            }
        }
        
        if(typeOfAction.equalsIgnoreCase(custSetObj.MACYS_1YearAfterPublishedArticle__c)){  // checking if the article is Published 1 Year Ago 
            for(sobject article : lsArticle){
                String ownerId =(String) article.get('MACYS_ArticleOwner__c'); // MACYS_ArticleOwner__c changed from Ownerid
                String ArticleNumber =(String) article.get('ArticleNumber');
                String Title =(String) article.get('Title');
                String createdByname =(String) article.get('MACYS_ArticleOwner__c');
                String ownerName = getAccOwnerName(article);
                String UrlName =(String) article.get('UrlName');
                String ArticleType =(String) article.get('ArticleType');
                String url = getArticleUrl(ArticleType,UrlName,custSetObj);
                
                if(String.isNotBlank(createdByname)){
                    Messaging.SingleEmailMessage msg=new Messaging.SingleEmailMessage();
                    msg.setTargetObjectId(ownerId);  // mail to owner 
                    msg.setSubject(ArticleNumber +': "'+Title +'" is not updated since 1 Year');
                    String body = ' <apex:page >'+
                                        '<div align="right"><img border="0" src="'+imageUrl+'"/></div>'+
                                        '</br>'+
                                        'Dear '+ownerName +','+
                                        '</br></br>'+
                                        'This notification serves to inform you that <b>'+ArticleNumber+'</b> - "'+Title+'" is 1 Year older. You are receiving this notification because you are listed as the Content Owner for this article. '+
                                        '</br></br>'+
                                        'Action required from you: Please review the article <b>"'+Title+'"</b> to confirm if any content / meta data updates are required. You are responsible for gathering any necessary approvals required to publish an updated Article.'+
                                        '</br></br>'+
                                        'If you have any additional questions, please <a href="'+url+'">Click Here</a> to review'+
                                        '</br></br>'+
                                        'Regards,</br>'+
                                        'Macy\'s Employee Services'+
                                        '</br></br>'+
                                        'This is an automated email – please do not reply to this mailbox as we will not receive your message.'+
                                    '</apex:page>';
                    msg.setHtmlBody(body);
                    msg.setSaveAsActivity(false); 
                    listmsg.add(msg);
                }
                
            }
        }
        
        if(typeOfAction.equalsIgnoreCase(custSetObj.MACYS_goingToExpin30daysArticle__c)){  // checking if the article is expiring within 30 days 
            for(sobject article : lsArticle){
                String ownerId =(String) article.get('MACYS_ArticleOwner__c'); // MACYS_ArticleOwner__c changed from Ownerid
                String ArticleNumber =(String) article.get('ArticleNumber');
                String Title =(String) article.get('Title');
                String createdByname =(String) article.get('MACYS_ArticleOwner__c');
                String ownerName = getAccOwnerName(article);
                String UrlName =(String) article.get('UrlName');
                String ArticleType =(String) article.get('ArticleType');
                String url = getArticleUrl(ArticleType,UrlName,custSetObj);
                
                if(String.isNotBlank(createdByname)){
                    Messaging.SingleEmailMessage msg=new Messaging.SingleEmailMessage();
                    msg.setTargetObjectId(ownerId); // mail to the owner of article
                    msg.setSubject(ArticleNumber +': "'+Title +'" is going to Expire in 30 Days');
                    String body = ' <apex:page >'+
                                        '<div align="right"><img border="0" src="'+imageUrl+'" /></div>'+
                                        '</br>'+
                                        'Dear '+ownerName +','+
                                        '</br></br>'+
                                        'This notification serves to inform you that <b>'+ArticleNumber+'</b> - "'+Title+'" will expire in 30 days. You are receiving this notification because you are listed as the Content Owner for this article. '+
                                        '</br></br>'+
                                        'Action required from you: Please review the article <b>"'+Title+'"</b> to confirm if any content / meta data updates are required. You are responsible for gathering any necessary approvals required to publish an updated Article.'+
                                        '</br></br>'+
                                        'If you have any additional questions, please <a href="'+url+'">Click Here</a> to review'+
                                        '</br></br>'+
                                        'Regards,</br>'+
                                        'Macy\'s Employee Services'+
                                        '</br></br>'+
                                        'This is an automated email – please do not reply to this mailbox as we will not receive your message.'+
                                    '</apex:page>';
                    msg.setHtmlBody(body);
                    msg.setSaveAsActivity(false); 
                    listmsg.add(msg);
                }
            }
        }
        
        if(typeOfAction.equalsIgnoreCase(custSetObj.MACYS_goingToExpin60daysArticle__c)){  // checking if the article is expiring within 60 days  
            for(sobject article : lsArticle){
                String ownerId =(String) article.get('MACYS_ArticleOwner__c'); // MACYS_ArticleOwner__c changed from Ownerid
                String ArticleNumber =(String) article.get('ArticleNumber');
                String Title =(String) article.get('Title');
                String createdByname =(String) article.get('MACYS_ArticleOwner__c');
                String ownerName = getAccOwnerName(article);
                String UrlName =(String) article.get('UrlName');
                String ArticleType =(String) article.get('ArticleType');
                String url = getArticleUrl(ArticleType,UrlName,custSetObj);
                
                if(String.isNotBlank(createdByname)){
                    Messaging.SingleEmailMessage msg=new Messaging.SingleEmailMessage();
                    msg.setTargetObjectId(ownerId); // mail to the owner of article
                    msg.setSubject(ArticleNumber +': "'+Title +'" is going to Expire in 60 Days');
                    String body = ' <apex:page >'+
                                        '<div align="right"><img border="0" src="'+imageUrl+'" /></div>'+
                                        '</br>'+
                                        'Dear '+ownerName +','+
                                        '</br></br>'+
                                        'This notification serves to inform you that <b>'+ArticleNumber+'</b> - "'+Title+'" will expire in 60 days. You are receiving this notification because you are listed as the Content Owner for this article. '+
                                        '</br></br>'+
                                        'Action required from you: Please review the article <b>"'+Title+'"</b> to confirm if any content / meta data updates are required. You are responsible for gathering any necessary approvals required to publish an updated Article.'+
                                        '</br></br>'+
                                        'If you have any additional questions, please <a href="'+url+'">Click Here</a> to review'+
                                        '</br></br>'+
                                        'Regards,</br>'+
                                        'Macy\'s Employee Services'+
                                        '</br></br>'+
                                        'This is an automated email – please do not reply to this mailbox as we will not receive your message.'+
                                    '</apex:page>';
                    msg.setHtmlBody(body);
                    msg.setSaveAsActivity(false); 
                    listmsg.add(msg);
                }
            }
        }
        
        if(typeOfAction.equalsIgnoreCase(custSetObj.MACYS_goingToExpin90daysArticle__c)){  // checking if the article is expiring within 90 days 
            for(sobject article : lsArticle){
                String ownerId =(String) article.get('MACYS_ArticleOwner__c'); // MACYS_ArticleOwner__c changed from Ownerid
                String ArticleNumber =(String) article.get('ArticleNumber');
                String Title =(String) article.get('Title');
                String createdByname =(String) article.get('MACYS_ArticleOwner__c');
                String ownerName = getAccOwnerName(article);
                String UrlName =(String) article.get('UrlName');
                String ArticleType =(String) article.get('ArticleType');
                String url = getArticleUrl(ArticleType,UrlName,custSetObj);
                
                if(String.isNotBlank(createdByname)){
                    Messaging.SingleEmailMessage msg=new Messaging.SingleEmailMessage();
                    msg.setTargetObjectId(ownerId); // mail to the owner of article
                    msg.setSubject(ArticleNumber +': "'+Title +'" is going to Expire in 90 Days');
                    String body = ' <apex:page >'+
                                        '<div align="right"><img border="0" src="'+imageUrl+'" /></div>'+
                                        '</br>'+
                                        'Dear '+ownerName +','+
                                        '</br></br>'+
                                        'This notification serves to inform you that <b>'+ArticleNumber+'</b> - "'+Title+'" will expire in 90 days. You are receiving this notification because you are listed as the Content Owner for this article. '+
                                        '</br></br>'+
                                        'Action required from you: Please review the article <b>"'+Title+'"</b> to confirm if any content / meta data updates are required. You are responsible for gathering any necessary approvals required to publish an updated Article.'+
                                        '</br></br>'+
                                        'If you have any additional questions, please <a href="'+url+'">Click Here</a> to review'+
                                        '</br></br>'+
                                        'Regards,</br>'+
                                        'Macy\'s Employee Services'+
                                        '</br></br>'+
                                        'This is an automated email – please do not reply to this mailbox as we will not receive your message.'+
                                    '</apex:page>';
                    msg.setHtmlBody(body);
                    msg.setSaveAsActivity(false); 
                    listmsg.add(msg);
                }
            }
        }
        
        if(typeOfAction.equalsIgnoreCase(custSetObj.MACYS_expiredArticle__c)){  // checking if the article is expired 
            for(sobject article : lsArticle){
                String ownerId =(String) article.get('MACYS_ArticleOwner__c'); // MACYS_ArticleOwner__c changed from Ownerid
                String ArticleNumber =(String) article.get('ArticleNumber');
                String Title =(String) article.get('Title');
                String createdByname =(String) article.get('MACYS_ArticleOwner__c');
                String ownerName = getAccOwnerName(article);
                String UrlName =(String) article.get('UrlName');
                String ArticleType =(String) article.get('ArticleType');
                String url = getArticleUrl(ArticleType,UrlName,custSetObj);
                
                if(String.isNotBlank(createdByname)){
                    Messaging.SingleEmailMessage msg=new Messaging.SingleEmailMessage();
                    //msg.setTargetObjectId(article.OwnerId); // mail to the owner of article
                    system.debug('article.OwnerId--->'+ownerId);
                    list<String> lsEmail = new list<String>();
                    lsEmail.add(ownerId);
                    msg.setToAddresses(lsEmail);  // mail to owner
                    msg.setSubject(ArticleNumber +': "'+Title +'" going to expire today ');
                    String body = ' <apex:page >'+
                                        '<div align="right"><img border="0" src="'+imageUrl+'"/></div>'+
                                        '</br>'+
                                        'Dear '+ownerName+','+
                                        '</br></br>'+
                                        'The  article - <b>'+ArticleNumber+'</b> - "'+Title+'" is going to expire today and will be Auto-Archived .'+
                                        '</br></br>'+
                                        'Please take the necessary actions.'+
                                        '</br></br>'+
                                        'Thank you for contacting Macy\'s Employee Services. If we can be of any further assistance or if the issue still persists, please contact us to raise a new case by visiting the Employee Services Portal for our contact information and to chat live with us, or call us during normal business hours. We will be happy to assist you. '+
                                        '</br></br>'+
                                        'Regards,</br>'+
                                        'Macy\'s Employee Services'+
                                        '</br></br>'+
                                        'This is an automated email – please do not reply to this mailbox as we will not receive your message.'+
                                    '</apex:page>';
                    msg.setHtmlBody(body);
                    msg.setSaveAsActivity(false); 
                    listmsg.add(msg);
                    system.debug('Expired Article Mail------> '+msg);
                }
            }
        }
        System.debug('listmsg in sendEmail() ->'+listmsg);
        Messaging.sendEmail(listmsg); 
        
        
    }
    
    /*******************************************************************************************
    * Method         :  getArticleUrl
    * Description    :  get the url
    * Parameter      :  ArticleType - String
                        UrlName - String
                        custSetObj - object
    * Return Type    :  None
    ******************************************************************************************/
    public static String getArticleUrl(String ArticleType,String UrlName,MACYS_CaseSLAMappingWithPriority__c custSetObj){
        String articlelink='';
          if(ArticleType.contains('MACYS_Brochure__kav')){
              articlelink = custSetObj.MACYS_CommunityBaseURL__c+'/articles/en_US/MACYS_Brochure/'+UrlName;
          }else if(ArticleType.contains('Macys_CallCenterGuides__kav')){
              articlelink = custSetObj.MACYS_CommunityBaseURL__c+'/articles/en_US/Macys_CallCenterGuides/'+UrlName;
          }else if(ArticleType.contains('MACYS_DTP__kav')){
              articlelink = custSetObj.MACYS_CommunityBaseURL__c+'/articles/en_US/MACYS_DTP/'+UrlName;
          }else if(ArticleType.contains('MACYS_FAQ__kav')){
              articlelink = custSetObj.MACYS_CommunityBaseURL__c+'/articles/en_US/MACYS_FAQ/'+UrlName;
          }else if(ArticleType.contains('MACYS_Form__kav')){
              articlelink = custSetObj.MACYS_CommunityBaseURL__c+'/articles/en_US/MACYS_Form/'+UrlName;
          }else if(ArticleType.contains('MACYS_IOP__kav')){
              articlelink = custSetObj.MACYS_CommunityBaseURL__c+'/articles/en_US/MACYS_IOP/'+UrlName;
          }else if(ArticleType.contains('MACYS_JobAidCase__kav')){
              articlelink = custSetObj.MACYS_CommunityBaseURL__c+'/articles/en_US/MACYS_JobAidCase/'+UrlName;
          }else if(ArticleType.contains('MACYS_OtherReferenceMaterial__kav')){
              articlelink = custSetObj.MACYS_CommunityBaseURL__c+'/articles/en_US/MACYS_OtherReferenceMaterial/'+UrlName;
          }else if(ArticleType.contains('MACYS_Policy__kav')){
              articlelink = custSetObj.MACYS_CommunityBaseURL__c+'/articles/en_US/MACYS_Policy/'+UrlName;
          }
        return articlelink;
    }
    
/*******************************************************************************************
    * Method         :  sendEmail
    * Description    :  get the url
    * Parameter      :  article - sobject
                        articleOwner - String
    * Return Type    :  None
    ******************************************************************************************/
    public static String getAccOwnerName(sobject article){
        Boolean result =false;
        String nameart='';
        result = article instanceof MACYS_Brochure__kav;
        if(result){
            MACYS_Brochure__kav art = (MACYS_Brochure__kav)article;
            nameart = art.MACYS_ArticleOwner__r.name;
            result =false;
            
        }
        result = article instanceof Macys_CallCenterGuides__kav ;
        if(result){
            Macys_CallCenterGuides__kav  art = (Macys_CallCenterGuides__kav)article;
            nameart = art.MACYS_ArticleOwner__r.name;
            result =false;
            
        } 
        result = article instanceof MACYS_DTP__kav  ;
        if(result){
            MACYS_DTP__kav   art = (MACYS_DTP__kav)article;
            nameart = art.MACYS_ArticleOwner__r.name;
            result =false;
            
        } 
        result = article instanceof MACYS_FAQ__kav  ;
        if(result){
            MACYS_FAQ__kav art = (MACYS_FAQ__kav)article;
            nameart = art.MACYS_ArticleOwner__r.name;
            result =false;
            
        } 
        result = article instanceof MACYS_Form__kav  ;
        if(result){
            MACYS_Form__kav art = (MACYS_Form__kav)article;
            nameart = art.MACYS_ArticleOwner__r.name;
            result =false;
            
        } 
        result = article instanceof MACYS_IOP__kav  ;
        if(result){
            MACYS_IOP__kav   art = (MACYS_IOP__kav)article;
            nameart = art.MACYS_ArticleOwner__r.name;
            result =false;
            
        } 
        result = article instanceof MACYS_JobAidCase__kav  ;
        if(result){
            MACYS_JobAidCase__kav   art = (MACYS_JobAidCase__kav)article;
            nameart = art.MACYS_ArticleOwner__r.name;
            result =false;
            
        } 
        result = article instanceof MACYS_OtherReferenceMaterial__kav  ;
        if(result){
            MACYS_OtherReferenceMaterial__kav   art = (MACYS_OtherReferenceMaterial__kav)article;
            nameart = art.MACYS_ArticleOwner__r.name;
            result =false;
            
        } 
        result = article instanceof MACYS_Policy__kav  ;
        if(result){
            MACYS_Policy__kav   art = (MACYS_Policy__kav)article;
            nameart = art.MACYS_ArticleOwner__r.name;
            result =false;
            
        } 
        return nameart;
    }

}