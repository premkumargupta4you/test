/******************************************************************************************
    * Create By     :     Deloitte Consulting.
    * Create Date   :     3/31/2016         
    * Description   :     Batch Class for creating Email for FAQ with published status = Archived

    *     Modification Log:
    *    -----------------------------------------------------------------------------
    *    * Developer                   Date               Description
    *    * ----------------------------------------------------------------------------                 
    *    * Prem Kumar Gupta            6/21/2016          Initial version.
******************************************************************************************/

global class MACYS_ArticleArchivedFormBatchClass implements Database.Batchable<sObject>{
    
     String query;
     Date todayDate; 
     MACYS_CaseSLAMappingWithPriority__c  custSetObj;  
     String articleType;
    
    
  /*******************************************************************************************
    * Method         :  Constructor
    * Description    :  This is constructor used to initialize values
    * Parameter      :  None
    * Return Type    :  None
  ******************************************************************************************/
    public MACYS_ArticleArchivedFormBatchClass(){
        todayDate = System.today(); 
        custSetObj = MACYS_CaseSLAMappingWithPriority__c.getOrgDefaults(); 
        if(custSetObj.MACYS_ArticleTypes__c!=null)
       articleType = custSetObj.MACYS_ArticleTypes__c.split(',')[4];
    }
    
    /*******************************************************************************************
    * Method         :  start
    * Description    :  This methood is used to fetch the records for processing
    * Parameter      :  Database.BatchableContext
    * Return Type    :  list of sobject records
    ******************************************************************************************/
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        Database.QueryLocator queryLocator;
        try{
            query = 'Select id,Title , ArticleNumber,ArticleType,MACYS_ArticleOwner__r.name, MACYS_PublishedDate__c,CreatedBy.Name,'+
                'Summary,CreatedBy.Manager.Id, UrlName ,LastModifiedBy.email,MACYS_ArticleOwner__c,LastModifiedById,OwnerId,MACYS_ExpiredDate__c from  '+articleType+
                ' where  PublishStatus=\''+custSetObj.MACYS_ArticleStatusArchived__c+'\' and language =\''+custSetObj.MACYS_ArticleLang__c+'\'';
            query = query + ' and ArchivedDate = '+custSetObj.MACYS_ArchivedDate__c; // TODAY
            // +' and MACYS_ExpiredDate__c !='+custSetObj.MACYS_ExpiredToday__c+' 
            system.debug('query>>>>' + query);
            queryLocator = Database.getQueryLocator(query);}catch(Exception ex){System.debug('Exception Caught-------'+ex);MACYS_Exception.createException('MACYS_ArticleArchivedBatchClass - start',ex.getMessage(), ex.getStackTraceString(),ex.getTypeName(),null,null);}
        return queryLocator;
        
    }
    
    /*******************************************************************************************
    * Method         :  execute
    * Description    :  This methood is used to process the records fetched in Start methood
    * Parameter      :  BC - Database.BatchableContext
                        scope - list sobjects sent from start methood
    * Return Type    :  None
    *******************************************************************************************/
    global void execute(Database.BatchableContext BC, list<sobject> scope){
        list<sobject> lstArtType;
        try{
           
            system.debug('inside   batch execute');
           // lstArtType = new list<MACYS_FAQ__kav>();
           // lstArtType = (list<MACYS_FAQ__kav>)scope;
            system.debug('scope----->'+scope);
            MACYS_EmailTemplateForArticle.sendEmail(scope,custSetObj.MACYS_ArchivedArticle__c);
            
            system.debug('<----- Mail sent successfully for ArchivedArticle Form----->');}catch(System.Exception ex){ system.debug('Exception caused on MACYS_ArticleArchivedBatchClass by : '+ ex);MACYS_Exception.createException('MACYS_ArticleArchivedBatchClass - execute',ex.getMessage(), ex.getStackTraceString(),ex.getTypeName(),null,null);}
    }
    
    /*******************************************************************************************
    * Method         :  finish
    * Description    :  This methood is used to do final activities
    * Parameter      :  BC - Database.BatchableContext                      
    * Return Type    :  None
    *******************************************************************************************/
    global void finish(Database.BatchableContext BC){
        system.debug('----- Inside finish of MACYS_ArticleArchivedBatchClass');
        system.debug('----- Calling Batch for Expired , going to Expired Article and Auto Archived Articles-----');
        try{
            MACYS_ArticleArchivedIOPBatchClass expiryArticleBatchObj = new MACYS_ArticleArchivedIOPBatchClass(); //calling  batch class
            database.executebatch(expiryArticleBatchObj);}catch(System.Exception ex){ system.debug('Exception caused in MACYS_ArticleArchivedIOPBatchClass by : '+ ex);MACYS_Exception.createException('MACYS_ArticleArchivedIOPBatchClass - calling Batch',ex.getMessage(), ex.getStackTraceString(),ex.getTypeName(),null,null);}
        
    }

}