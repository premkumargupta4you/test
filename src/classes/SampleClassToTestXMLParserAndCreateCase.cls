public class SampleClassToTestXMLParserAndCreateCase{

    public void creatingCase(String xmlString){
    
        EmailXMLParser obj=new EmailXMLParser();
        String resultString=obj.parse(xmlString);
        List<String> caseValuesWithApiName=resultString.split('\n');
        
        String key='';
        String value='';
        
        //map of case field API name and value
        Map<String,String> mapOfCaseWithApiNameAndValue= new Map<String,String>();
        
        for(String itr :caseValuesWithApiName){
            if(itr.contains('Element: name,') || itr.contains('Element: value,')){
                system.debug('itr values'+itr);
                if(itr.contains('Element: name,')){
                    key=itr.Substring(itr.indexOf('text=')+5);
                }
                if(itr.contains('Element: value,')){
                    value=itr.Substring(itr.indexOf('text=')+5);
                    mapOfCaseWithApiNameAndValue.put(key,value);
                    key='';
                    value='';
                }
            }
        }
        system.debug('values in map'+mapOfCaseWithApiNameAndValue);
          
        Case cseRec=new Case();
        
        if(!mapOfCaseWithApiNameAndValue.isempty()){
            for(String values : mapOfCaseWithApiNameAndValue.keyset()){
                cseRec.put(values,mapOfCaseWithApiNameAndValue.get(values));
            }
        
        }
        
        insert cseRec;
        system.debug('case created'+cseRec.id);
    }
}