/****************************************************************************************
* Create By       :     Deloitte Consulting
* Create Date     :     08/05/2016
* Description     :     Helper Class for Contact object
* Modification Log:
*   -----------------------------------------------------------------------------
*   * Developer             Date                Modification ID     Description
*   * ----------------------------------------------------------------------------                 
*   * PKARADI               08/05/2016                              Initial version.        
*****************************************************************************************/
global class MACYS_ContactUserCreateAndUpdateBatch implements Database.Batchable<sObject> 
{
    String query;
    MACYS_CaseSLAMappingWithPriority__c  custSetObj= MACYS_CaseSLAMappingWithPriority__c.getOrgDefaults();
    /*******************************************************************************************
    * Method         :  MACYS_ContactUserCreateAndUpdateBatch
    * Description    :  This is constructor used to initialize values
    * Parameter      :  None
    * Return Type    :  None
    ******************************************************************************************/
    public MACYS_ContactUserCreateAndUpdateBatch()
    {
        
    }
    
    /*******************************************************************************************
    * Method         :  start
    * Description    :  This methood is used to fetch the records for processing
    * Parameter      :  Database.BatchableContext
    * Return Type    :  list of sobject records
    ******************************************************************************************/
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        query = 'Select Id, LastModifiedById, MACYS_HR_Flag__c, MACYS_TermDate__c, MACYS_User__c, MACYS_User__r.isActive, FirstName, LastName,';
        query = query + ' MACYS_ISN__c, Email, MACYS_AssignmentStatus__c, MACYS_Executive__c, MACYS_EmployeeNumber__c from Contact where';
        query = query + ' LastModifiedById = \''+custSetObj.MACYS_HRAdminUserId__c+'\'';
        query = query + ' and lastmodifieddate >= '+Label.MACYS_LastNDays;
        System.debug(query);
        return Database.getQueryLocator(query); 
    }
    
    /*******************************************************************************************
    * Method         :  execute
    * Description    :  This methood is used to process the records fetched in Start methood
    * Parameter      :  BC - Database.BatchableContext
                        scope - List sobjects sent from start methood
    * Return Type    :  None
    ******************************************************************************************/
    global void execute(Database.BatchableContext BC, List<sobject> scope)
    {
        try
        {
            list<Contact> lstConScope = new list<Contact>();
            lstConScope = (list<Contact>)scope;
            set<Id> setUsrToInactivate = new set<Id>();
            set<Id> setUsrToActivate = new set<Id>();
            set<Id> setUsrs = new set<Id>();
            list<User> lstUsrForTermUpd = new list<User>();
            list<User> lstUsersCreated = new list<User>();
            list<User> lstUsersAll = new list<User>();
            
            map<String, Id> mapUsrDtls = new map<String, Id>();
            
            map<Id, Date> mapUsrIdTermDate = new map<Id, Date>();
            set<Id> setUsrIdIns = new set<Id>();
            
            //Get the contacts for which the users need to be created or terminated
            for(Contact objCon: lstConScope)
            {
                if(objCon.MACYS_User__c == null && objCon.MACYS_HR_Flag__c == 'Y' && 
                    (objCon.MACYS_AssignmentStatus__c == Label.MACYS_AssignmentStatusActive || 
                    objCon.MACYS_AssignmentStatus__c == Label.MACYS_AssignmentStatusLOA || 
                    objCon.MACYS_AssignmentStatus__c == Label.MACYS_AssignmentStatusLWP) && objCon.MACYS_Executive__c != Label.MACYS_EmployeeClass)
                {
                    lstUsersCreated.add(CreateUserRecord(objCon));
                    System.debug(objCon.LastName);
                }
                else if(objCon.MACYS_User__c != null && objCon.MACYS_HR_Flag__c == 'N' && objCon.MACYS_User__r.isActive == true && objCon.MACYS_TermDate__c == null)
                {System.debug(objCon.LastName);
                    setUsrToInactivate.add(objCon.MACYS_User__c);
                    System.debug(setUsrToInactivate);
                }
                else if(objCon.MACYS_User__c != null && objCon.MACYS_HR_Flag__c == 'Y' && 
                    (objCon.MACYS_AssignmentStatus__c == Label.MACYS_AssignmentStatusActive || 
                    objCon.MACYS_AssignmentStatus__c == Label.MACYS_AssignmentStatusLOA || 
                    objCon.MACYS_AssignmentStatus__c == Label.MACYS_AssignmentStatusLWP) && objCon.MACYS_Executive__c != Label.MACYS_EmployeeClass && objCon.MACYS_TermDate__c == null)
                {System.debug(objCon.LastName);
                    setUsrToActivate.add(objCon.MACYS_User__c);
                    System.debug(setUsrToActivate);
                }
                else
                {
                    if(objCon.MACYS_TermDate__c != null && objCon.MACYS_User__c != null)
                    {System.debug(objCon.LastName);
                        mapUsrIdTermDate.put(objCon.MACYS_User__c, objCon.MACYS_TermDate__c);
                    }
                }
            }
            System.debug(lstUsersCreated);
            //insert the user records
            if(!lstUsersCreated.isEmpty())
            {
                Database.DMLOptions dml = new Database.DMLOptions(); 
                Database.SaveResult[] lstsr = Database.insert(lstUsersCreated, dml);
                for(Database.SaveResult sr: lstsr)
                {
                    if (sr.isSuccess())
                    {
                        if(sr.getId() != null)
                        {
                            setUsrIdIns.add(sr.getId()); //Get the inserted userIds
                        }
                    }
                    else
                    {
                        for(Database.Error err : sr.getErrors()) 
                        {
                            MACYS_Exception.createException('MACYS_ContactUserCreateAndUpdateBatch',err.getMessage(), String.valueOf(err.getMessage()).Right(250),'Error','User',null);
                        }
                    }
                }
            }
            System.debug(setUsrIdIns);
            setUsrs.addAll(setUsrIdIns);
            System.debug(setUsrs);
            setUsrs.addAll(setUsrToInactivate);
            System.debug(setUsrs);
            setUsrs.addAll(setUsrToActivate);
            System.debug(setUsrs);
            if(mapUsrIdTermDate != null)
            {
                setUsrs.addAll(mapUsrIdTermDate.keySet());
            }
            System.debug(setUsrs);
            //Logic to associate UserId and Contact
            //Get the Users impacted for this transaction
            for(User objUsrIns: [SELECT Id, FederationIdentifier, MACYS_ISN__c From User where Id =: setUsrs])
            {
                mapUsrDtls.put(objUsrIns.MACYS_ISN__c, objUsrIns.Id);
                lstUsersAll.add(objUsrIns);
            }
            
            
            System.debug(mapUsrDtls);
            for(Contact objCon: lstConScope)
            {
                if(objCon.MACYS_User__c == null)
                { System.debug(objCon.LastName);System.debug(objCon.MACYS_ISN__c);
                    if(mapUsrDtls != null && mapUsrDtls.containskey(objCon.MACYS_ISN__c))
                    { System.debug(mapUsrDtls);
                        objCon.MACYS_User__c = mapUsrDtls.get(objCon.MACYS_ISN__c);
                    }
                }
            }
            
            //Logic to update the the User record
            for(User usr: lstUsersAll)
            {System.debug(lstUsersAll);
                if(mapUsrIdTermDate != null && mapUsrIdTermDate.containsKey(usr.Id))
                {System.debug(usr.MACYS_ISN__c);
                    usr.MACYS_UserInactivateDate__c = mapUsrIdTermDate.get(usr.Id).addMonths(Integer.valueOf(Label.MACYS_AddMonths));
                    lstUsrForTermUpd.add(usr);
                }
                else if(!setUsrToInactivate.isEmpty() && setUsrToInactivate.contains(usr.id))
                {System.debug(usr.MACYS_ISN__c);
                    usr.IsActive = false;
                    lstUsrForTermUpd.add(usr);
                }
                else
                {System.debug(usr.MACYS_ISN__c);
                    if(!setUsrToActivate.isEmpty() && setUsrToActivate.contains(usr.Id))
                    {System.debug(usr.MACYS_ISN__c);
                        usr.IsActive = true;
                        lstUsrForTermUpd.add(usr);
                    }
                }
            }
            
            //Update Users for termination
            if(!lstUsrForTermUpd.isEmpty())
            {
                try
                {
                    Database.DMLOptions dml = new Database.DMLOptions(); 
                    Database.SaveResult[] lstsr = Database.update(lstUsrForTermUpd, dml);
                }
                catch(System.Exception ex)
                {
                    MACYS_Exception.createException('MACYS_ContactUserCreateAndUpdateBatch',ex.getMessage(), ex.getStackTraceString(),ex.getTypeName(),null,null); 
                }
            }
            
            //Update contacts with user id
            update lstConScope;
        }
        catch(System.Exception ex)
        {
            MACYS_Exception.createException('MACYS_ContactUserCreateAndUpdateBatch',ex.getMessage(), ex.getStackTraceString(),ex.getTypeName(),null,null); 
        }
    }
    
    /*******************************************************************************************
    * Method         :  finish
    * Description    :  This methood is used to do final activities
    * Parameter      :  BC - Database.BatchableContext                      
    * Return Type    :  None
    ******************************************************************************************/
    global void finish(Database.BatchableContext BC)
    {
        
    }
    
    /*******************************************************************************************
    * Method         :  CreateUserRecord
    * Description    :  This methood is used create the user record context
    * Parameter      :  Contact record 
    * Return Type    :  User Record
    ******************************************************************************************/
    User CreateUserRecord(Contact objConForUser)
    {
        User objNewUsr = new User();
        objNewUsr.FirstName =  objConForUser.FirstName;
        objNewUsr.LastName  =  objConForUser.LastName;
        objNewUsr.Alias     =  String.valueOf(String.valueOf(objConForUser.FirstName).left(1) + objConForUser.LastName).left(8);
        if(objConForUser.Email == null || objConForUser.Email == '')
        {
            objNewUsr.Email = String.valueOf(Label.MACYS_SampleUserName+'.'+objConForUser.MACYS_EmployeeNumber__c).replaceAll( '\\s+', '')+Label.MACYS_EmailSuffix;
            objNewUsr.Username  =  String.valueOf(Label.MACYS_SampleUserName+'.'+objConForUser.MACYS_EmployeeNumber__c).replaceAll( '\\s+', '')+Label.MACYS_EmailSuffix;
        }
        else
        {
            objNewUsr.Email     =  objConForUser.Email;
            objNewUsr.Username  =  objConForUser.Email;
        } 
        
        objNewUsr.CommunityNickname =   String.valueOf(objConForUser.FirstName + objConForUser.LastName).left(120);
        objNewUsr.TimeZoneSidKey    =   Label.MACYS_TimeZoneKeyForUser;
        objNewUsr.IsActive = true;
        objNewUsr.ProfileId = custSetObj.MACYS_HRManagerProfileId__c;
        objNewUsr.FederationIdentifier = objConForUser.MACYS_EmployeeNumber__c;
        objNewUsr.MACYS_ISN__c = objConForUser.MACYS_ISN__c;
        objNewUsr.MACYS_EmpNumExtId__c = objConForUser.MACYS_EmployeeNumber__c;
        objNewUsr.LocaleSidKey = Label.MACYS_LocaleSidKey;
        objNewUsr.EmailEncodingKey = Label.MACYS_EmailEncodingKey;
        objNewUsr.LanguageLocaleKey = Label.MACYS_LanguageLocaleKey; 
        return objNewUsr;
    }
}