/************************************************************************************
    * Class        :   MACYS_Exception
    * Description  :   Class to define the method for exception record creation
    * Parameter    :   Exception object fields   
    * Return Type  :   
*************************************************************************************/ 
public with sharing class MACYS_Exception extends Exception
{
    /************************************************************************************
    * Method        :   createException
    * Description   :   Accept the exception parameters and create the record in exception object
    * Parameter     :   Exception values   
    * Return Type  :    
    *************************************************************************************/ 
    public static void createException(String strClassMethod, String strMessage,  String strStackString, String strType,String strObject1,String initcause)
    {   
        MACYS_Exception__c objExceptionInstance = new MACYS_Exception__c();
        
        objExceptionInstance.MACYS_ClassMethod__c = strClassMethod;
        objExceptionInstance.MACYS_Message__c = strMessage;
        objExceptionInstance.MACYS_Timestamp__c= system.now();
        objExceptionInstance.MACYS_Type__c = strType;
        objExceptionInstance.MACYS_StackTrace__c= strStackString;
        if(strObject1!=null){
           objExceptionInstance.MACYS_Object__c = strObject1;
        }
        if(initcause!=null){
            objExceptionInstance.MACYS_RecordId__c= initcause;
        }
                
        try 
        {
            insert objExceptionInstance;
        } 
        catch(Exception e)
        {    
            return;
        }        
        return;    
    }
}