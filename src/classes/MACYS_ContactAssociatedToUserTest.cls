/******************************************************************************************
    * Create By     :     Deloitte Consulting.
    * Create Date   :     8/23/2016         
    * Description   :     Test Class for MACYS_ContactAssociatedToUser and MACYS_CaseCommentsComponentController class

    *     Modification Log:
    *    -----------------------------------------------------------------------------
    *    * Developer                   Date               Description
    *    * ----------------------------------------------------------------------------                 
    *    * Puneet Jalota               8/23/2016           
******************************************************************************************/
@isTest
private class MACYS_ContactAssociatedToUserTest{

    static Id caseSISStepOneRecordTypeId=Schema.SObjectType.Case.getRecordTypeInfosByName().get('Solutions InSTORE Step 1').getRecordTypeId();
    MACYS_CaseSLAMappingWithPriority__c  custSetObj1= MACYS_TestUtility.createCustomSettingForArticle();
    
    private static testmethod void testcontactID(){
        
        User runningUser=MACYS_TestUtility.createUser('rKumar','System Administrator');
        Contact con=MACYS_TestUtility.creatingContact(runningUser,'2345');
        
        
        System.runAs(runningUser) {
            MACYS_ContactAssociatedToUser.contactID();
        }
    }
    
    private static testmethod void testCaseComments(){
        
        User runningUser=MACYS_TestUtility.createUser('rKumar','System Administrator');
        Contact con=MACYS_TestUtility.creatingContact(runningUser,'2345');
        
        MACYS_Locations__c locInsta=new MACYS_Locations__c();
        locInsta.Location__c='test123';
        insert locInsta;
        
        System.runAs(runningUser) {
            Case case2=new Case();
            case2.MACYS_IsAnonymous__c=true;
            case2.status='open';
            case2.Priority='Medium';
            case2.Origin='Phone';
            case2.MACYS_CaseCategory__c='Solutions InSTORE';
            case2.MACYS_CaseSubCategory1__c='Discrimination';
            case2.MACYS_CaseSubCategory2__c='Age';
            case2.recordtypeid=caseSISStepOneRecordTypeId;
            case2.MACYS_SISStep__c='Step-4';
            case2.MACYS_SISCaseLocation__c=locInsta.id;
            case2.MACYS_SISCaseRegion__c='71995 - Region - Southwest';
            case2.MACYS_SISDistrict__c='71903 - District - Delmarva South';
            case2.MACYS_SISBusinessUnit__c='00091 - FACS Business Unit';
            case2.Origin='Phone';
            
            insert case2;
            
            CaseComment newValue=new CaseComment();
            newValue.CommentBody='Test123';
            newValue.ParentId=case2.id;
            newValue.IsPublished=true;
            
            insert newValue;
            
            MACYS_CaseCommentsComponentController newInsta= new MACYS_CaseCommentsComponentController();
            
            newInsta.caseId=case2.id;
            newInsta.newComments='test1234';
            newInsta.NewComment();
            
            MACYS_CaseCommentsComponentController.cComments innerInsta=new MACYS_CaseCommentsComponentController.cComments();
            
            
            innerInsta.cComment=newValue;
            innerInsta.commentText ='test123';
            innerInsta.PublicPrivateAction='Make Public';
            
            List<MACYS_CaseCommentsComponentController.cComments> innerList=newInsta.comments;
        }
    }
    
}