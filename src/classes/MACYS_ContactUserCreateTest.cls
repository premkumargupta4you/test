/******************************************************************************************
    * Create By     :     Deloitte Consulting.
    * Create Date   :     8/8/2016         
    * Description   :     Test Class for MACYS_ContactUserCreateAndUpdateBatch

    *     Modification Log:
    *    -----------------------------------------------------------------------------
    *    * Developer                   Date               Description
    *    * ----------------------------------------------------------------------------                 
    *    * Prem Kumar Gupta            3/31/2016          Initial version.
    *    * Prem Kumar Gupta            4/6/2016           testArticleArchived and testArticlePublished created.
    *    * Puneet Jalota               4/6/2016           testArticleExpired created
******************************************************************************************/
@isTest
private class MACYS_ContactUserCreateTest{
 
    
/*******************************************************************************************
* Method         :  testContactUserRelation
* Description    :  This test methood is used to check if MACYS_ContactUserCreateAndUpdateBatch class is working fine;
* Parameter      :  None
* Return Type    :  None
*******************************************************************************************/
    private static testmethod void testContactUserRelation() {
        Test.StartTest();
        User u = MACYS_TestUtility1.createUser('TsMACYS','System Administrator');
        
        String strProfile ='Macys HR Community Manager';
        Id communityManagerId= [Select Id From Profile Where Name =: strProfile Limit 1].id;
        
        MACYS_CaseSLAMappingWithPriority__c cusSetObj = new MACYS_CaseSLAMappingWithPriority__c();
        cusSetObj.MACYS_HRAdminUserId__c=u.id;
        cusSetObj.MACYS_HRManagerProfileId__c=communityManagerId;
        insert cusSetObj;
        
        System.runas(u){
            List<Contact> testCons=new List<Contact>();
            
            Contact con=new contact();
            con.MACYS_Account_Code__c='4440000';
            con.FirstName='ABC';
            con.lastname='XYZ';
            con.Email=system.now().millisecond()+'abc@macys.com';
            con.MACYS_EmployeeNumber__c='23123231';
            con.MACYS_AssignmentStatus__c='Active';
            testCons.add(con);
            
            
            Contact con1=new contact();
            con1.MACYS_Account_Code__c='4440000';
            con1.FirstName='CBA';
            con1.lastname='ZYX';
            con1.Email=system.now().millisecond()+'xyz@macys.com';
            con1.MACYS_EmployeeNumber__c='2123231';
            con1.MACYS_TermDate__c=System.Today();
            con1.MACYS_User__c=u.id;
            con1.MACYS_AssignmentStatus__c='Active';
            testCons.add(con1);
            
            insert testCons;
            
 
            
            MACYS_ContactUserCreateAndUpdateBatch testObj = new MACYS_ContactUserCreateAndUpdateBatch();
            database.executebatch(testObj);
            Test.StopTest();
            
            Contact firstContact=[select id,MACYS_User__c from contact where id=:con.id];
            Contact secondContact=[select id,MACYS_TermDate__c from contact where id=:con1.id];
            User userUsed=[select id, MACYS_UserInactivateDate__c from user where id=:u.id];
            
            system.debug('asca'+firstContact.MACYS_User__c);
            System.assert(firstContact.MACYS_User__c!=null);
            System.assert(userUsed.MACYS_UserInactivateDate__c==(secondContact.MACYS_TermDate__c.addMonths(13)) );
            
        }
    }
}