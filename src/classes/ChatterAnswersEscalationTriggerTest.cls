@isTest
private class ChatterAnswersEscalationTriggerTest {
    static testMethod void validateQuestionEscalation() {
        String questionTitle = 'questionTitle';
        String questionBody = 'questionBody';
        Community[] c = [SELECT Id from Community where Isactive = true];
        // We cannot create a question without a community
        if (c.size() == 0) { return; }
        String communityId = c[0].Id;
        Question q = new Question();
        q.Title = questionTitle;
        q.Body = questionBody;
        q.CommunityId = communityId;
        insert(q);
        q.Priority = 'high';
        update(q);
        system.debug('-----q------'+q);
        list<Case> ca = new list<Case>();
        ca = [SELECT Origin, CommunityId, Subject, Description from Case where QuestionId =: q.Id];
        // Test that escaltion trigger correctly escalate the question to a case
        for(Case cx:ca)
        {
            System.assertEquals(questionTitle, cx.Subject);
            System.assertEquals(questionBody, cx.Description);
            System.assertEquals('Chatter Answers', cx.Origin);
            System.assertEquals(communityId, cx.CommunityId);
        }
    }
}