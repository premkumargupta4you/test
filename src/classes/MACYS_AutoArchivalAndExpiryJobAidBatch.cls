/******************************************************************************************
* Create By     :     Deloitte Consulting.
* Create Date   :     04/04/2016         
* Description   :     Batch Class for AutoArchival of articles and sending notification to Article owners about expiration happening
today, after 30 days, 60 days, 90 days.

*     Modification Log:
*    -----------------------------------------------------------------------------
*    * Developer                   Date               Description
*    * ----------------------------------------------------------------------------                 
*    * Puneet Jalota             04/04/2016          Initial version.
******************************************************************************************/
global class MACYS_AutoArchivalAndExpiryJobAidBatch implements Database.Batchable<sObject>{

    String query;
    Date todayDate; 
    MACYS_CaseSLAMappingWithPriority__c  custSetObj;  
    String articleType;
    
    
/*******************************************************************************************
* Method         :  Constructor
* Description    :  This is constructor used to initialize values
* Parameter      :  None
* Return Type    :  None
******************************************************************************************/
    public MACYS_AutoArchivalAndExpiryJobAidBatch (){
    
        todayDate  = System.today();
        custSetObj = MACYS_CaseSLAMappingWithPriority__c.getOrgDefaults(); 
        if(custSetObj.MACYS_ArticleTypes__c!=null)
            articleType = custSetObj.MACYS_ArticleTypes__c.split(',')[6]; //MACYS_JobAidCase__kav 
    }

/*******************************************************************************************
* Method         :  start
* Description    :  This methood is used to fetch the records for processing
* Parameter      :  Database.BatchableContext
* Return Type    :  list of sobject records
******************************************************************************************/
    global Database.QueryLocator start(Database.BatchableContext BC){
        Database.QueryLocator queryLocator;
        try{

            query = 'Select id,Title ,URLName, KnowledgeArticleId,  ArticleNumber,ArticleType ,MACYS_ArticleOwner__c,MACYS_ArticleOwner__r.name, MACYS_PublishedDate__c,CreatedBy.Name,'+
                    'Summary,CreatedBy.Manager.email,LastModifiedBy.email,LastModifiedById,OwnerId, MACYS_ExpiredDate__c, '+
                    'LastPublishedDate from '+articleType+' where  PublishStatus=\''+custSetObj.MACYS_ArticleStatusOnline__c+'\' and '+
                    'language =\''+custSetObj.MACYS_ArticleLang__c+'\'';
            
            query = query + ' and MACYS_ExpiredDate__c <= NEXT_90_DAYS'; // Adding articles going to expire in 90 days.
            //  check for LastPublishedDate or firstPublishedDate
            system.debug('query>>>>' + query);
            queryLocator = Database.getQueryLocator(query);
        }catch(Exception ex){
            System.debug('Exception Caught');
            MACYS_Exception.createException('MACYS_ArticlePublishedBatchClass - start',ex.getMessage(), ex.getStackTraceString(),ex.getTypeName(),null,null);
        }
        return queryLocator;
    }
/*******************************************************************************************
* Method         :  execute
* Description    :  This methood is used to process the records fetched in Start methood
* Parameter      :  BC - Database.BatchableContext
                    scope - list sobjects sent from start methood
* Return Type    :  None
*******************************************************************************************/
    global void execute(Database.BatchableContext BC, list<sobject> scope){
        list<MACYS_JobAidCase__kav> lstArtType;
        try{

            /* for articles which are published today and published a year back */
            //starts
            system.debug('inside   batch execute');
            lstArtType = new list<MACYS_JobAidCase__kav>();
            lstArtType = (list<MACYS_JobAidCase__kav>)scope; 
            system.debug('lstArtType----->'+lstArtType);

            list<MACYS_JobAidCase__kav> listArticleToArchive = new list<MACYS_JobAidCase__kav>();
            list<MACYS_JobAidCase__kav> listArticleToExpireAfterThirtyDays = new list<MACYS_JobAidCase__kav>();
            list<MACYS_JobAidCase__kav> listArticleToExpireAfterSixtyDays = new list<MACYS_JobAidCase__kav>();
            list<MACYS_JobAidCase__kav> listArticleToExpireAfterNintyDays = new list<MACYS_JobAidCase__kav>();

            for(MACYS_JobAidCase__kav  allArticle: lstArtType){
                if(allArticle.MACYS_ExpiredDate__c==todayDate){
                    listArticleToArchive.add(allArticle);
                }
                else if(todayDate.daysBetween(allArticle.MACYS_ExpiredDate__c)==Integer.valueOf(custSetObj.MACYS_goingToExpin30daysArticle__c)){
                    listArticleToExpireAfterThirtyDays.add(allArticle);
                }
                else if(todayDate.daysBetween(allArticle.MACYS_ExpiredDate__c)==Integer.valueOf(custSetObj.MACYS_goingToExpin60daysArticle__c)){
                    listArticleToExpireAfterSixtyDays.add(allArticle);
                }
                else if(todayDate.daysBetween(allArticle.MACYS_ExpiredDate__c)==Integer.valueOf(custSetObj.MACYS_goingToExpin90daysArticle__c)){
                    listArticleToExpireAfterNintyDays.add(allArticle);
                }
            }
            system.debug('all lists'+listArticleToArchive +listArticleToExpireAfterThirtyDays );
            if(listArticleToExpireAfterThirtyDays.size()>0){
                system.debug('30 days');
                MACYS_EmailTemplateForArticle.sendEmail(listArticleToExpireAfterThirtyDays,custSetObj.MACYS_goingToExpin30daysArticle__c);
            }
            if(listArticleToExpireAfterSixtyDays.size()>0)
                MACYS_EmailTemplateForArticle.sendEmail(listArticleToExpireAfterSixtyDays,custSetObj.MACYS_goingToExpin60daysArticle__c);
            if(listArticleToExpireAfterNintyDays.size()>0)
                MACYS_EmailTemplateForArticle.sendEmail(listArticleToExpireAfterNintyDays,custSetObj.MACYS_goingToExpin90daysArticle__c);
            if(listArticleToArchive.size()>0){
                system.debug('article to archive');
                expirationOfArticles(listArticleToArchive);
                MACYS_EmailTemplateForArticle.sendEmail(listArticleToArchive,custSetObj.MACYS_expiredArticle__c);
            }

        }
        catch(System.Exception ex){ 
            system.debug('Exception caused in MACYS_ArticlePublishedBatchClass by : '+ ex);
            MACYS_Exception.createException('MACYS_ArticlePublishedBatchClass - execute',ex.getMessage(), ex.getStackTraceString(),ex.getTypeName(),null,null);
        }
    }

/*******************************************************************************************
* Method         :  expirationOfArticles
* Description    :  This methood is used to Archive the records which have Expiry date as today
* Parameter      :  list of articles
* Return Type    :  None
*******************************************************************************************/
    public void expirationOfArticles(list<MACYS_JobAidCase__kav> articlesexpiringToday){
        for(MACYS_JobAidCase__kav  articleToexpire: articlesexpiringToday){
            System.debug('came here'+articleToexpire.id);
            KbManagement.PublishingService.archiveOnlineArticle(articleToexpire.KnowledgeArticleId ,null);
        } 
    }

/*******************************************************************************************
* Method         :  finish
* Description    :  This methood is used to do final activities
* Parameter      :  BC - Database.BatchableContext                      
* Return Type    :  None
*******************************************************************************************/
    global void finish(Database.BatchableContext BC){
        system.debug('----- Inside finish of MACYS_ArticlePublishedBatchClass');
        try{
            MACYS_AutoArchivalAndExpiryORMBatch autoArticleBatchObj = new MACYS_AutoArchivalAndExpiryORMBatch(); //calling  batch class
            database.executebatch(autoArticleBatchObj);
        }catch(System.Exception ex){ 
            system.debug('Exception caused in MACYS_AutoArchivalAndExpiryORMBatch by : '+ ex);
            MACYS_Exception.createException('MACYS_AutoArchivalAndExpiryORMBatch - calling Batch',ex.getMessage(), ex.getStackTraceString(),ex.getTypeName(),null,null);
        }
    }
}