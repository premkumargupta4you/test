/******************************************************************************************
    * Create By     :     Deloitte Consulting.
    * Create Date   :     31/3/2016         
    * Description   :     Schedulable Class for calling batch class for Contact Creation

    *     Modification Log:
    *    -----------------------------------------------------------------------------
    *    * Developer                   Date               Description
    *    * ----------------------------------------------------------------------------                 
    *    * Prem Kumar Gupta            9/8/2016          Initial version.
******************************************************************************************/

global class MACYS_ContactBatchSchedularClass implements schedulable{
    
    /*******************************************************************************************
    * Method         :  execute
    * Description    :  This methood is used to process request for making Schedular call for Batch Apex
    * Parameter      :  sc - SchedulableContext
    * Return Type    :  None
    *******************************************************************************************/
    global void execute(SchedulableContext sc){
        MACYS_ContactUserCreateAndUpdateBatch contactCreateObj = new MACYS_ContactUserCreateAndUpdateBatch(); //calling  batch class
        database.executebatch(contactCreateObj);
        
        
    }
}