/******************************************************************************************
* Create By     :     Deloitte Consulting.
* Create Date   :     4/20/2016         
* Description   :     Use to find the related SIS cases and display them on inlive VF page of SIS record types.

*     Modification Log:
*    -----------------------------------------------------------------------------
*    * Developer                   Date               Description
*    * ----------------------------------------------------------------------------                 
*    * Prem Kumar Gupta            4/20/2016          Initial version.
******************************************************************************************/
public with sharing class MACYS_SearchRelatedSISCase 
{
    private ApexPages.StandardController controller{get;set;}
    public List<Case> sisCaseList{get;set;}
    public Boolean hasRelatedCase{get;set;}
    public String profileName{get;set;}
    public Boolean isCommunityUser{get;set;}
    public Boolean isConsoleUser{get;set;}
    public String communityUrl{get;set;}
    private Case cse{get;set;}
    private List<Case> caseList ;
    private String sisCaseNumber = '';
    private String commMan;
    private String commemp;

    public MACYS_SearchRelatedSISCase(ApexPages.StandardController controller)
    {
        this.controller = controller;
        cse = (Case)controller.getRecord();
        hasRelatedCase = false;
        isCommunityUser = false;
        isConsoleUser = true;
        commMan = System.Label.MACYS_CommunityProfileManagerName;
        commemp = System.Label.MACYS_CommunityProfileName;
    }


    /*******************************************************************************************
    * Method         :  getRelatedCase
    * Description    :  This method is used to find related case .
    * Parameter      :  none
    * Return Type    :  None
    *******************************************************************************************/ 

    public void getRelatedCase()
    {
        Id Id1 = userinfo.getProfileId();
        List<Profile> profileList = [Select Name from Profile where Id =:Id1];
        if(profileList[0].Name!=null){
            profileName = profileList[0].Name;
            
            if(profileName.equalsIgnoreCase(commMan) || profileName.equalsIgnoreCase(commemp)){
                isCommunityUser = true;
                isConsoleUser = false;
            }
            MACYS_CaseSLAMappingWithPriority__c  custSetObj= MACYS_CaseSLAMappingWithPriority__c.getOrgDefaults(); 
            communityUrl = custSetObj.MACYS_CommunityBaseURL__c;

        }
            
        system.debug('Profile ======'+profileName);
        
        caseList = [select id,CaseNumber,MACYS_SISCaseNumber__c from Case where id= :cse.id limit 1];
        for(Case c : caseList){
            if(c.MACYS_SISCaseNumber__c != null){
                sisCaseNumber =c.MACYS_SISCaseNumber__c.substring(0,c.MACYS_SISCaseNumber__c.length()-2) + '%'; 
            }
        }
        Integer count = 0;
        Integer exitingCaseNumberIndex =-1;
        sisCaseList = [select id,casenumber,MACYS_SISStep__c,MACYS_SISCaseNumber__c, Subject from Case where MACYS_SISCaseNumber__c like :sisCaseNumber order by casenumber];
        for(Case c : sisCaseList){
            if(c.id == cse.id){
                exitingCaseNumberIndex = count;
            }
            system.debug(c.casenumber);
            count++;
        }
        sisCaseList.remove(exitingCaseNumberIndex);
        if(sisCaseList.size()>0){
            hasRelatedCase =true;
        }
    }
}