/******************************************************************************************
    * Create By     :     Deloitte Consulting.
    * Create Date   :     7/12/2016         
    * Description   :     Test Class for BML Trigger 'MACYS_BMLTrigger' and MACYS_creatingCaseRecordForBMlInsert class

    *     Modification Log:
    *    -----------------------------------------------------------------------------
    *    * Developer                   Date               Description
    *    * ----------------------------------------------------------------------------                 
    *    * Puneet Jalota               7/12/2016           
******************************************************************************************/
@isTest
private class MACYS_BAMLTriggerTest{

/*******************************************************************************************
    * Method         :  testBAMLFunctionality
    * Description    :  This test methood is used to check BAML trigger and helper class.
    * Parameter      :  None
    * Return Type    :  None
    *******************************************************************************************/
    
    private static testmethod void testBAMLFunctionality(){
        User testUser=MACYS_TestUtility.createUser('rKumar','System Administrator');
        Contact con=MACYS_TestUtility.creatingContact(testUser,'2345');
        con.MACYS_ISN__c ='1234';
        list<MACYS_BMLRecord__c> listObj = new list<MACYS_BMLRecord__c>();
        
        MACYS_BMLRecord__c bamlInstance=new MACYS_BMLRecord__c();
        bamlInstance.MACYS_ISN__c='2345';
        //bamlInstance.Name='22131023';
        //bamlInstance.MACYS_TransactionID__c =22131023;
        //bamlInstance.MACYS_Comments__c='Test BAML';
        bamlInstance.MACYS_CaseCategory__c='General Inquiry';
        bamlInstance.MACYS_CaseSubCategory1__c='Absence';
        bamlInstance.MACYS_CaseSubCategory2__c='Bereavement';
        bamlInstance.MACYS_Subject__c='Test BML';
        bamlInstance.MACYS_CaseClosedStatus__c=false;
        listObj.add(bamlInstance);

        MACYS_BMLRecord__c bamlInstance1=new MACYS_BMLRecord__c();
        bamlInstance1.MACYS_ISN__c='1234';
        //bamlInstance1.Name='22131024';
        //bamlInstance1.MACYS_TransactionID__c =22131024;
        //bamlInstance1.MACYS_Comments__c='Test BAML1';
        bamlInstance1.MACYS_CaseCategory__c='General Inquiry';
        bamlInstance1.MACYS_CaseSubCategory1__c='Absence';
        bamlInstance1.MACYS_CaseSubCategory2__c='Bereavement';
        bamlInstance1.MACYS_Subject__c='Test BML1';
        bamlInstance1.MACYS_CaseClosedStatus__c=true;
        listObj.add(bamlInstance1);
        
        Test.startTest();
        update con;
        insert listObj;
        
        Test.stopTest();
    }
}