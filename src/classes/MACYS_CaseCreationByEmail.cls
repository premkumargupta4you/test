/******************************************************************************************
    * Create By     :     Deloitte Consulting.
    * Create Date   :     07/11/2016         
    * Description   :     Class to create Case record when an email hits the Salesforce org.

    *     Modification Log:
    *    -----------------------------------------------------------------------------
    *    * Developer                   Date               Description
    *    * ----------------------------------------------------------------------------                 
    *    * Puneet Jalota             07/11/2016          Initial version.
******************************************************************************************/
global class MACYS_CaseCreationByEmail implements Messaging.InboundEmailHandler {



/*******************************************************************************************
* Method         :  handleInboundEmail
* Description    :  This methood is used to create Case records getting detail from the email recieved.
* Parameter      :  InboundEmail and Inboundenvelope 
* Return Type    :  InboundEmailResult
******************************************************************************************/
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email,Messaging.Inboundenvelope envelope) {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        
        BusinessHours defaultBUsinessHour=[SELECT Id FROM BusinessHours Where isActive = true and isDefault = true limit 1];
        system.debug('what is hour'+defaultBUsinessHour);
        String truncatedToAddress=envelope.toaddress.substring(0, envelope.toaddress.indexOf('@'));
        System.debug('email hit that class'+truncatedToAddress);
        MACYS_CaseFieldMappingForEmailService__c customSetting= MACYS_CaseFieldMappingForEmailService__c.getValues(truncatedToAddress);
        //customSetting.getValues('General Inquiry');
        System.debug('email hit that class'+customSetting);
        
        Case caseToInsert=new Case();
        
        if(customSetting!=null){
            caseToInsert.RecordTypeID=customSetting.MACYS_CaseRecordTypeID__c;
            caseToInsert.Subject=email.subject;
            
            if (email.binaryAttachments !=null && email.binaryAttachments.size() > 0){
                caseToInsert.Subject=caseToInsert.Subject+Label.MACYS_AttachmentDeleted;
            }
            
            caseToInsert.Description=email.plainTextBody;
            caseToInsert.Origin=customSetting.MACYS_CaseOrigin__c;
            caseToInsert.MACYS_CaseCategory__c=customSetting.MACYS_CaseCategory__c;
            caseToInsert.MACYS_CaseSubCategory1__c=customSetting.MACYS_CaseSubCategory1__c;
            caseToInsert.MACYS_CaseSubCategory2__c=customSetting.MACYS_CaseSubCategory2__c;
            caseToInsert.BusinessHoursId =defaultBUsinessHour.id;
            
            Database.DMLOptions dmo = new Database.DMLOptions();
            dmo.assignmentRuleHeader.useDefaultRule = true;
            caseToInsert.setOptions(dmo);
            
            try{
                insert caseToInsert;
            }
            catch(exception e){
            }
        }
        System.debug('bcbc'+caseToInsert.id);
        
        
        
          
        //Searches the email for binary attachments and
        // associates them with the job application  
      /*  List<Attachment> allAttachments=new List<Attachment>();
        
        if (email.binaryAttachments !=null && email.binaryAttachments.size() > 0 && caseToInsert.id!=null){
            
            for (integer i = 0 ; i < email.binaryAttachments.size() ; i++){
            Attachment a = new Attachment(ParentId = caseToInsert.Id,
            Name = email.binaryAttachments[i].filename,
            Body = email.binaryAttachments[i].body);
            allAttachments.add(a);
            
            }
        }
        if(!allAttachments.isEmpty()){
            insert allAttachments;
        }*/
        
        return result;
    }
}