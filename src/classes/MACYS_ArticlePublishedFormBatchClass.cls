/******************************************************************************************
    * Create By     :     Deloitte Consulting.
    * Create Date   :     3/31/2016         
    * Description   :     Batch Class for creating Email for Article Type for publish articles
                          and those which are going to expire within 90,60 and 30 days

    *     Modification Log:
    *    -----------------------------------------------------------------------------
    *    * Developer                   Date               Description
    *    * ----------------------------------------------------------------------------                 
    *    * Prem Kumar Gupta            6/21/2016          Initial version.
******************************************************************************************/

global class MACYS_ArticlePublishedFormBatchClass implements Database.Batchable<sObject>{
    
     String query;
     Date todayDate; 
     MACYS_CaseSLAMappingWithPriority__c  custSetObj;  
     String articleType;
    
    
    /*******************************************************************************************
    * Method         :  Constructor
    * Description    :  This is constructor used to initialize values
    * Parameter      :  None
    * Return Type    :  None
    ******************************************************************************************/
    public MACYS_ArticlePublishedFormBatchClass(){
        todayDate = System.today(); 
        custSetObj = MACYS_CaseSLAMappingWithPriority__c.getOrgDefaults(); 
        if(custSetObj.MACYS_ArticleTypes__c!=null)
            articleType = custSetObj.MACYS_ArticleTypes__c.split(',')[4];
        
    }
    
    /*******************************************************************************************
    * Method         :  start
    * Description    :  This methood is used to fetch the records for processing
    * Parameter      :  Database.BatchableContext
    * Return Type    :  list of sobject records
    ******************************************************************************************/
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        Database.QueryLocator queryLocator;
        try{
            query = 'Select id,Title , ArticleNumber,ArticleType,MACYS_ArticleOwner__r.name, MACYS_PublishedDate__c,CreatedBy.Name,'+
                'Summary,CreatedBy.Manager.Id, UrlName ,LastModifiedBy.email,LastModifiedById,MACYS_ArticleOwner__c,OwnerId, MACYS_ExpiredDate__c, '+
                'LastPublishedDate from '+articleType+' where  PublishStatus=\''+custSetObj.MACYS_ArticleStatusOnline__c+'\' and '+
                'language =\''+custSetObj.MACYS_ArticleLang__c+'\'';
            query = query + ' and LastPublishedDate = '+custSetObj.MACYS_LastPublishedDate__c+''; // TODAY,YESTERDAY
            //  check for LastPublishedDate or firstPublishedDate
            system.debug('query>>>>' + query);
            queryLocator = Database.getQueryLocator(query);}catch(Exception ex){System.debug('Exception Caught');MACYS_Exception.createException('MACYS_ArticlePublishedBatchClass - start',ex.getMessage(), ex.getStackTraceString(),ex.getTypeName(),null,null);}
        return queryLocator;
    }
    
    /*******************************************************************************************
    * Method         :  execute
    * Description    :  This methood is used to process the records fetched in Start methood
    * Parameter      :  BC - Database.BatchableContext
                        scope - list sobjects sent from start methood
    * Return Type    :  None
    *******************************************************************************************/
    global void execute(Database.BatchableContext BC, list<sobject> scope){
        list<sobject> lstArtType = scope;
        try{
            
            /* for articles which are published today and published a year back */
            //starts
            system.debug('inside   batch execute');
            //lstArtType = new list<MACYS_ArticleType__kav>();
            //lstArtType = (list<MACYS_ArticleType__kav>)scope; 
            system.debug('lstArtType----->'+lstArtType);
           
            list<sobject> listpublished1YearAgoArticle = new list<sobject>();
            for(sobject listPublished : lstArtType){
                // check last published date of the each article
                DateTime lastPublishedDate = (DateTime)listPublished.get('LastPublishedDate');
                Date dtPublished = date.newinstance(lastPublishedDate.year(),
                                        lastPublishedDate.month(), 
                                        lastPublishedDate.day());
               /* Date dtPublished = date.newinstance(listPublished.LastPublishedDate.year(),
                                                    listPublished.LastPublishedDate.month(), 
                                                    listPublished.LastPublishedDate.day()); */
                system.debug('dtPublished----->'+dtPublished);
                if (dtPublished == (todayDate -365)){ //  for articles which are published a year back   
                    listpublished1YearAgoArticle.add(listPublished);
                    system.debug('list added in listpublished1YearAgoArticle');
                }
            }
            if(listpublished1YearAgoArticle.size()>0)
                MACYS_EmailTemplateForArticle.sendEmail(listpublished1YearAgoArticle,custSetObj.MACYS_1YearAfterPublishedArticle__c);
            
            system.debug('<----- Mail sent successfully for 1YearAfterPublishedArticle Form----->');}catch(System.Exception ex){ system.debug('Exception caused in MACYS_ArticlePublishedBatchClass by : '+ ex);MACYS_Exception.createException('MACYS_ArticlePublishedBatchClass - execute',ex.getMessage(), ex.getStackTraceString(),ex.getTypeName(),null,null);}
    }
    
    /*******************************************************************************************
    * Method         :  finish
    * Description    :  This methood is used to do final activities
    * Parameter      :  BC - Database.BatchableContext                      
    * Return Type    :  None
    *******************************************************************************************/
    global void finish(Database.BatchableContext BC){
        system.debug('----- Inside finish of MACYS_ArticlePublishedBatchClass');
        system.debug('----- Calling Batch for Archived Article -----');
        try{
            MACYS_ArticlePublishedIOPBatchClass archivedArticleBatchObj = new MACYS_ArticlePublishedIOPBatchClass(); //calling  batch class
            database.executebatch(archivedArticleBatchObj);}catch(System.Exception ex){ system.debug('Exception caused in MACYS_ArticlePublishedIOPBatchClass by : '+ ex);MACYS_Exception.createException('MACYS_ArticlePublishedIOPBatchClass - calling Batch',ex.getMessage(), ex.getStackTraceString(),ex.getTypeName(),null,null);}
        
        
    }

}