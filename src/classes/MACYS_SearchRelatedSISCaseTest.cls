/******************************************************************************************
    * Create By     :     Deloitte Consulting.
    * Create Date   :     07/14/2016         
    * Description   :     Test Class for SIS Related Case Search

    *     Modification Log:
    *    -----------------------------------------------------------------------------
    *    * Developer                   Date               Description
    *    * ----------------------------------------------------------------------------                 
    *    * Puneet Jalota               5/10/2016           
******************************************************************************************/
@isTest
private class MACYS_SearchRelatedSISCaseTest{
    
    /*public static MACYS_EntitlementData__c entitleSetting=MACYS_TestUtility.creatingMACYS_EntitlementData('Medium',15,'Medium',480,120);
    public static MACYS_EntitlementData__c entitleCriticalSetting=MACYS_TestUtility.creatingMACYS_EntitlementData('Critical',15,'Critical',480,120);
    public static MACYS_EntitlementData__c entitleHighSetting=MACYS_TestUtility.creatingMACYS_EntitlementData('High',15,'High',480,120);
    public static MACYS_EntitlementData__c entitleReportSetting=MACYS_TestUtility.creatingMACYS_EntitlementData('HR Ad Hoc Report Request',15,'High',480,120);
    public static MACYS_EntitlementData__c entitleLowSetting=MACYS_TestUtility.creatingMACYS_EntitlementData('Low',15,'Low',480,120);
    public static MACYS_EntitlementData__c entitleSIS1Setting=MACYS_TestUtility.creatingMACYS_EntitlementData('Step-1',15,'Medium',480,120);
    public static MACYS_EntitlementData__c entitleSIS2Setting=MACYS_TestUtility.creatingMACYS_EntitlementData('Step-2',15,'Medium',480,120);
    public static MACYS_EntitlementData__c entitleSIS3Setting=MACYS_TestUtility.creatingMACYS_EntitlementData('Step-3',15,'Medium',480,120);*/
    
    public static MACYS_EntitlementData__c entitleSetting=MACYS_TestUtility.creatingMACYS_EntitlementData('1', '', '', '', false, true,'Medium',480,120);
    public static MACYS_EntitlementData__c entitleCriticalSetting=MACYS_TestUtility.creatingMACYS_EntitlementData('2', '', '', '', false, true,'Critical',480,120);
    public static MACYS_EntitlementData__c entitleHighSetting=MACYS_TestUtility.creatingMACYS_EntitlementData('3', '', '', '', false, true,'High',480,120);
    public static MACYS_EntitlementData__c entitleLowSetting=MACYS_TestUtility.creatingMACYS_EntitlementData('4', '', '', '', false, true,'Low',480,120);
    public static MACYS_EntitlementData__c entitleSISSetting=MACYS_TestUtility.creatingMACYS_EntitlementData('5', 'Solutions InStore','','',false, false,'',0,0);
    public static MACYS_EntitlementData__c entitleBIFASetting=MACYS_TestUtility.creatingMACYS_EntitlementData('6','Benefit Inquiry and Transaction', 'First appeal', '', false, true, '',28800,960);
    public static MACYS_EntitlementData__c entitleBISASetting=MACYS_TestUtility.creatingMACYS_EntitlementData('7','Benefit Inquiry and Transaction', 'Second appeal', '', false, true, '',28800,960);
    public static MACYS_EntitlementData__c entitleDN1Setting=MACYS_TestUtility.creatingMACYS_EntitlementData('8','Death Notification', 'Active Employee Death Notification', '', false, true, '',9600,120);
    public static MACYS_EntitlementData__c entitleDN2Setting=MACYS_TestUtility.creatingMACYS_EntitlementData('9','Death Notification', 'Former Employee Death Notification', '', false, true,'',9600,120);
    public static MACYS_EntitlementData__c entitleDN3Setting=MACYS_TestUtility.creatingMACYS_EntitlementData('10','Death Notification', 'Dependent Death Notification', '', false, true,'',9600,120);
    public static MACYS_EntitlementData__c entitleDN4Setting=MACYS_TestUtility.creatingMACYS_EntitlementData('11','Death Notification', 'Additional notification', '', true, true,'',9600,120);
    
    public static BusinessHours businessHour=[SELECT Id,IsActive,TimeZoneSidKey FROM BusinessHours Where IsActive = true and TimeZoneSidKey='America/Chicago' limit 1];
    private static MACYS_SubCat1WithoutSubCat2__c subcatsetting=MACYS_TestUtility.creatingMACYS_SubCat1WithoutSubCat2();
    
    private static testmethod void testRelatedCase(){
        
        User runningUser=MACYS_TestUtility.createUser('rKumar','System Administrator');
        Test.startTest(); 
        Contact con=MACYS_TestUtility.creatingContact(runningUser,'2345');
        Test.stopTest(); 
        
        //MACYS_CaseSLAMappingWithPriority__c  custSetObj=MACYS_TestUtility.createCustomSettingForArticle();
        List<Case> casesToAssociateEntitlement=new List<Case>();
        Id caseSISStepOneRecordTypeId=Schema.SObjectType.Case.getRecordTypeInfosByName().get('Solutions InSTORE Step 1').getRecordTypeId();
        Id caseSISStepTwoRecordTypeId=Schema.SObjectType.Case.getRecordTypeInfosByName().get('Solutions InSTORE Step 2').getRecordTypeId();
        //List<Entitlement> entitle=new List<Entitlement>();
        
        System.runAs(runningUser) {
        
        Case case1=new Case();
        case1.Status='Open';
        case1.Priority='Medium';
        case1.MACYS_PreferredModeOfContact__c='Phone';
        case1.ContactId = con.Id;
        case1.BusinessHoursId=businessHour.id;
        case1.RecordTypeId = caseSISStepOneRecordTypeId;
        case1.Origin='Phone';
        insert case1;
        
        Case case2=new Case();
        case2.Status='Open';
        case2.BusinessHoursId=businessHour.id;
        case2.ContactId = con.Id;
        case2.RecordTypeId = caseSISStepTwoRecordTypeId;
        case2.MACYS_RelatedCase__c = case1.Id;
        case2.Origin='Phone';
        insert case2;
        
        Apexpages.StandardController stdController = new Apexpages.StandardController(case2);
 
        MACYS_SearchRelatedSISCase objTestClass = new MACYS_SearchRelatedSISCase(StdController);
        objTestClass.getRelatedCase();
        
        Case casetoverify=[select id,MACYS_SISCaseNumber__c from case where id=: case2.id];
        Case case1Data=[select id,MACYS_SISCaseNumber__c, CaseNumber from case where id=: case1.id];
        System.assertEquals(casetoverify.MACYS_SISCaseNumber__c, case1Data.CaseNumber+'-2'); 
        } 
    }

}