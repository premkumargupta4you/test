@isTest
public class MACYS_CaseCreationByEmailTest{

    public static testMethod void EmailAttachmentTester(){
        
        MACYS_EntitlementData__c entitleSetting=new MACYS_EntitlementData__c();
        entitleSetting.name='Medium';
        entitleSetting.MACYS_ResolutionMilestone__c=1440;
        entitleSetting.MACYS_SLAWarningTime__c=120;
        insert entitleSetting;
        
        String toAddress=Label.MACYS_ToAddress;
        
        MACYS_CaseFieldMappingForEmailService__c customSetting=new MACYS_CaseFieldMappingForEmailService__c();
        customSetting.name=toAddress.substring(0, toAddress.indexOf('@'));
        customSetting.MACYS_CaseOrigin__c='Email';
        customSetting.MACYS_CaseRecordTypeID__c=Schema.SObjectType.Case.getRecordTypeInfosByName().get('General Inquiry').getRecordTypeId();
        
        insert customSetting;
        
        MACYS_CaseCreationByEmail emailClass=new MACYS_CaseCreationByEmail();
    
        // Create a new email, envelope object and Attachment
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        
        // add an Binary attachment
        Messaging.InboundEmail.BinaryAttachment attachment = new Messaging.InboundEmail.BinaryAttachment();
        attachment.body = blob.valueOf('my attachment text');
        attachment.fileName = 'textfileone.txt';
        attachment.mimeTypeSubType = 'text/plain';
        email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachment };
        
        envelope.toaddress=toAddress;
        emailClass.handleInboundEmail(email, envelope);
    
    }
    
}