/******************************************************************************************
    * Create By     :     Deloitte Consulting.
    * Create Date   :     9/28/2016         
    * Description   :     Extension Controller for Case

    *     Modification Log:
    *    -----------------------------------------------------------------------------
    *    * Developer                   Date               Description
    *    * ----------------------------------------------------------------------------                 
    *    * Prem Kumar Gupta               9/28/2016           
******************************************************************************************/
public class MACYS_CaseControllerExtension {

    private final Account acct;
    
    public MACYS_CaseControllerExtension(ApexPages.StandardController stdController) {
        this.acct = (Account)stdController.getRecord();
    }

    public String getGreeting() {
        return 'Hello ' + acct.name + ' (' + acct.id + ')';
    }
}