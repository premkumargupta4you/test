/******************************************************************************************
    * Create By     :     Deloitte Consulting.
    * Create Date   :     8/23/2016         
    * Description   :     Class to get details from Pre-chat Live chat.

    *     Modification Log:
    *    -----------------------------------------------------------------------------
    *    * Developer                   Date               Description
    *    * ----------------------------------------------------------------------------                 
    *    * Prem Kumar Gupta               9/26/2016           
******************************************************************************************/
public with sharing class MACYS_LiveAgentController{

     public String description{get;set;}
     public String caseRecordType{get;set;}
     public String userId{get;set;}  
     public String contactId{get;set;}
     public String category{get;set;}
     public String subCategory1{get;set;}
   
     
     MACYS_CaseSLAMappingWithPriority__c  custSetObj= MACYS_CaseSLAMappingWithPriority__c.getOrgDefaults(); 
    
     
     public MACYS_LiveAgentController(){
            userId = UserInfo.getUserId();
            getContactId();
            if(custSetObj.MACYS_CaseGeneralInquiryID__c != null)
                caseRecordType = custSetObj.MACYS_CaseGeneralInquiryID__c;
            category = 'General Inquiry';
            subCategory1 = 'Other';
     }
    
     
     public void getContactId(){
     
         try{
             List<Contact> lstCOntact = [select id,name from contact where MACYS_User__c =:userId limit 1];
             if(lstCOntact.size()>0)
                 contactId = lstCOntact[0].id;
         }catch(Exception e){
             system.debug('----Exception in getContactId---'+e);
         }
         system.debug('----Contact Id :---'+contactId);
         
         
     }
     
    
     
}