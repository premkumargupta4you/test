/******************************************************************************************
    * Create By     :     Deloitte Consulting.
    * Create Date   :     3/31/2016         
    * Description   :     Test Class for Article Type

    *     Modification Log:
    *    -----------------------------------------------------------------------------
    *    * Developer                   Date               Description
    *    * ----------------------------------------------------------------------------                 
    *    * Prem Kumar Gupta            3/31/2016          Initial version.
    *    * Prem Kumar Gupta            4/6/2016           testArticleArchived and testArticlePublished created.
    *    * Puneet Jalota               4/6/2016           testArticleExpired created
******************************************************************************************/

public class MACYS_TestUtility{

/*******************************************************************************************
* Method         :  createUser
* Description    :  This test methood is used to create User for Article
* Parameter      :  strUniqueKey,strProfile
* Return Type    :  User
*******************************************************************************************/   
   public static User createUser(String strUniqueKey, String strProfile){
                
                User oUSer                    = new User();                           
                oUser.Alias                   = strUniqueKey;
                oUser.Email                   = strUniqueKey + '@testmacys.org';
                oUser.EmailEncodingKey        = 'UTF-8';
                oUser.LastName                = 'testuser';
                oUser.FirstName               = strUniqueKey;
                oUser.LanguageLocaleKey       = 'en_US';
                oUser.LocaleSidKey            = 'en_US';
                oUser.ProfileId               = [Select Id From Profile Where Name =: strProfile Limit 1].Id;
                oUser.TimeZoneSidKey          = 'America/Chicago';
                oUser.Username                = strUniqueKey + '@testmacys.org';
                oUser.Country                 = 'United States';        
          insert oUser;
          return oUser; 
    } 
    
    

/*******************************************************************************************
* Method         :  createCustomSettingForArticle
* Description    :  This test methood is used to create Custom Setting for Article
* Parameter      :  none
* Return Type    :  MACYS_CaseSLAMappingWithPriority__c
*******************************************************************************************/  
     public static MACYS_CaseSLAMappingWithPriority__c createCustomSettingForArticle(){
            MACYS_CaseSLAMappingWithPriority__c cusSetObj = MACYS_CaseSLAMappingWithPriority__c.getOrgDefaults();
            cusSetObj.MACYS_ArticleStatusOnline__c = 'Online';
            cusSetObj.MACYS_ArticleStatusArchived__c = 'Archived';
            cusSetObj.MACYS_ArticleLang__c ='en_US';
            cusSetObj.MACYS_ArchivedDate__c = 'TODAY';
            cusSetObj.MACYS_LastPublishedDate__c = 'TODAY';
            cusSetObj.MACYS_1YearAfterPublishedArticle__c = 'AYear';
            cusSetObj.MACYS_LoggedinUserProfileName__c='System Administrator';
            cusSetObj.MACYS_CaseGeneralInquiryID__c=Schema.SObjectType.Case.getRecordTypeInfosByName().get('General Inquiry').getRecordTypeId();
            cusSetObj.MACYS_Step1RecordType__c=Schema.SObjectType.Case.getRecordTypeInfosByName().get('Solutions InSTORE Step 1').getRecordTypeId();
            cusSetObj.MACYS_Step2RecordType__c=Schema.SObjectType.Case.getRecordTypeInfosByName().get('Solutions InSTORE Step 2').getRecordTypeId();
            cusSetObj.MACYS_ArticleTypes__c = 'MACYS_Brochure__kav,Macys_CallCenterGuides__kav,MACYS_DTP__kav,MACYS_FAQ__kav,MACYS_Form__kav,MACYS_IOP__kav,MACYS_JobAidCase__kav,MACYS_OtherReferenceMaterial__kav,MACYS_Policy__kav';
            cusSetObj.MACYS_ArticleHeaderURL__c ='https://c.cs20.content.force.com/servlet/servlet.ImageServer?id=015m00000005o2D&oid=00Dm0000000Cy7h';
            
            upsert cusSetObj MACYS_CaseSLAMappingWithPriority__c.Id;
         return cusSetObj;
    }
    
/*******************************************************************************************
* Method         :  createCustomSettingForFollowUpDays
* Description    :  This test methood is used to create Custom Setting for FollowUp Days
* Parameter      :  none
* Return Type    :  MACYS_CaseFollowUpDays__c
*******************************************************************************************/   
    public static MACYS_CaseFollowUpDays__c createCustomSettingForFollowUpDays(){
        MACYS_CaseFollowUpDays__c followupCustomSetting=new MACYS_CaseFollowUpDays__c();
        followupCustomSetting.name='Medium';
        followupCustomSetting.MACYS_FirstNotification__c=1;
        followupCustomSetting.MACYS_SecondNotification__c=2;
        insert followupCustomSetting;
        return followupCustomSetting;
    }
    

/*******************************************************************************************
* Method         :  insertArticles
* Description    :  This test methood is used to insert Article
* Parameter      :  none
* Return Type    :  MACYS_ArticleType__kav
*******************************************************************************************/   
    
    public static MACYS_Brochure__kav insertArticles(){
          MACYS_Brochure__kav article = new MACYS_Brochure__kav(
                                                Title='Unit Test',
                                                UrlName  ='UnitTest',
                                                MACYS_ExpiredDate__c =System.today(),
                                                MACYS_EffectiveDate__c=System.today(),
                                                MACYS_PublishedDate__c=System.today());
            insert article;
            system.debug('article======'+article);
            return article;
    }

    public static Contact creatingContact(User usertoassociate, String employeeNumber){
        Contact con=new Contact();
        con.lastname='Roy';
        con.Macys_User__c=usertoassociate.id;
        con.MACYS_EmployeeNumber__c=employeeNumber;
        con.MailingCity='washington';
        insert con;
        return con;
    }
    
    public static MACYS_EntitlementData__c creatingMACYS_EntitlementData(String name, String strCategory, String strSubCat1, String strSubCat2, Boolean isActualHrs, Boolean isSLAApp, String Priority, Integer resolution, Integer warning){
    
        MACYS_EntitlementData__c newInsta=new MACYS_EntitlementData__c ();
        newInsta.name=name;
        newInsta.MACYS_CategoryValue__c = strCategory;
        newInsta.MACYS_SubCategory1__c = strSubCat1;
        newInsta.MACYS_SubCategory2__c = strSubCat2;
        newInsta.MACYS_IsActualHours__c = isActualHrs;
        newInsta.MACYS_IsSLAApplicable__c = isSLAApp;
        newInsta.MACYS_Priority__c=Priority;
        newInsta.MACYS_ResolutionMilestone__c=resolution;
        newInsta.MACYS_SLAWarningTime__c=warning;
        insert newInsta;
        return newInsta;
    }
    
    public static MACYS_SubCat1WithoutSubCat2__c creatingMACYS_SubCat1WithoutSubCat2(){
        MACYS_SubCat1WithoutSubCat2__c newInsta=new MACYS_SubCat1WithoutSubCat2__c();
        newInsta.MACYS_SubCategory1Values1__c=';Attendance;Clocking;Policies;Employment Verification;Benefits Verification;Sick / Safe Time;Benefits;Retirement;HR Technology;HR Reporting;HR Content;Other;Knowledge management content Creation;Labor Content Creation;Disaster Content Creation;';
        newInsta.MACYS_SubCategory1Values2__c=';Content Update;Metadata Change;Records Submission;I-9 Reverification;I-9 Receipt;E-Verify;SSN Applied For;Conversion Inquiry;Update Carrier;Deduction;Benefits Other;Retiree Medical / Life Eligibility Research;PTO Research;';
        newInsta.MACYS_SubCategory1Values3__c=';Active Employee Death Notification;Former Employee Death Notification;Dependent Death Notification;Additional notification;Social Security Number Change;Voluntary Rate Change;Holiday Pay;EAP;Systems Support;Vendor;Move non-sell hours;Dishonest Employee;';
        newInsta.MACYS_SubCategory1Values4__c=';Hourly off cycle;Exec off cycle;Missing pay / clockings;Salary advance - hourly;Salary advance - salaried;Tax update;Non-benefit deductions;Replacement check;Holiday;HR;Benefits;Payroll;All;Drop Call;Unemployment Claim;Customer Service Discount;';
        newInsta.MACYS_SubCategory1Values5__c=';HR;Benefits;Unemployment Claim;Other;;';
        insert newInsta;
        return newInsta;
    }
}