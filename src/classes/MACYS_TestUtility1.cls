/******************************************************************************************
    * Create By     :     Deloitte Consulting.
    * Create Date   :     3/31/2016         
    * Description   :     Test Class for Article Type

    *     Modification Log:
    *    -----------------------------------------------------------------------------
    *    * Developer                   Date               Description
    *    * ----------------------------------------------------------------------------                 
    *    * Prem Kumar Gupta            3/31/2016          Initial version.
    *    * Prem Kumar Gupta            4/6/2016           testArticleArchived and testArticlePublished created.
    *    * Puneet Jalota               4/6/2016           testArticleExpired created
******************************************************************************************/

public class MACYS_TestUtility1{

/*******************************************************************************************
* Method         :  createUser
* Description    :  This test methood is used to create User for Article
* Parameter      :  strUniqueKey,strProfile
* Return Type    :  User
*******************************************************************************************/   
   public static User createUser(String strUniqueKey, String strProfile){
                
                User oUSer                    = new User();                           
                oUser.Alias                   = strUniqueKey;
                oUser.Email                   = strUniqueKey + '@testmacys.org';
                oUser.EmailEncodingKey        = 'UTF-8';
                oUser.LastName                = 'testuser';
                oUser.FirstName               = strUniqueKey;
                oUser.LanguageLocaleKey       = 'en_US';
                oUser.LocaleSidKey            = 'en_US';
                oUser.ProfileId               = [Select Id From Profile Where Name =: strProfile Limit 1].Id;
                oUser.TimeZoneSidKey          = 'America/Panama';
                oUser.Username                = strUniqueKey + '@testmacys.org';
                oUser.Country                 = 'United States';
                oUser.UserPermissionsKnowledgeUser = true;        
          insert oUser;
          return oUser; 
    } 
    
    

/*******************************************************************************************
* Method         :  createCustomSettingForArticle
* Description    :  This test methood is used to create Custom Setting for Article
* Parameter      :  none
* Return Type    :  MACYS_CaseSLAMappingWithPriority__c
*******************************************************************************************/   
    public static MACYS_CaseSLAMappingWithPriority__c createCustomSettingForArticle(){
            MACYS_CaseSLAMappingWithPriority__c cusSetObj = new MACYS_CaseSLAMappingWithPriority__c();
            cusSetObj.MACYS_ArticleStatusOnline__c = 'Online';
            cusSetObj.MACYS_ArticleStatusArchived__c = 'Archived';
            cusSetObj.MACYS_ArticleLang__c ='en_US';
            cusSetObj.MACYS_ArchivedArticle__c='AYear';
            cusSetObj.MACYS_ArchivedDate__c = 'TODAY';
            cusSetObj.MACYS_expiredArticle__c='Expired';
            cusSetObj.MACYS_ExpiredToday__c='TODAY';
            cusSetObj.MACYS_goingToExpin30daysArticle__c ='30';
            cusSetObj.MACYS_goingToExpin60daysArticle__c ='60';
            cusSetObj.MACYS_goingToExpin90daysArticle__c ='90';
            cusSetObj.MACYS_LastPublishedDate__c = 'TODAY';
            cusSetObj.MACYS_1YearAfterPublishedArticle__c = 'AYear';
            cusSetObj.MACYS_LoggedinUserProfileName__c='System Administrator';
            cusSetObj.MACYS_CaseGeneralInquiryID__c=Schema.SObjectType.Case.getRecordTypeInfosByName().get('General Inquiry').getRecordTypeId();
            cusSetObj.MACYS_Step1RecordType__c=Schema.SObjectType.Case.getRecordTypeInfosByName().get('Solutions InSTORE Step 1').getRecordTypeId();
            cusSetObj.MACYS_Step2RecordType__c=Schema.SObjectType.Case.getRecordTypeInfosByName().get('Solutions InSTORE Step 2').getRecordTypeId();
            cusSetObj.MACYS_ArticleTypes__c = 'MACYS_Brochure__kav,Macys_CallCenterGuides__kav,MACYS_DTP__kav,MACYS_FAQ__kav,MACYS_Form__kav,MACYS_IOP__kav,MACYS_JobAidCase__kav,MACYS_OtherReferenceMaterial__kav,MACYS_Policy__kav';
            cusSetObj.MACYS_ArticleHeaderURL__c ='https://c.cs20.content.force.com/servlet/servlet.ImageServer?id=015m00000005o2D&oid=00Dm0000000Cy7h';
            
            insert cusSetObj;
         return cusSetObj;
    }
    
/*******************************************************************************************
* Method         :  createCustomSettingForFollowUpDays
* Description    :  This test methood is used to create Custom Setting for FollowUp Days
* Parameter      :  none
* Return Type    :  MACYS_CaseFollowUpDays__c
*******************************************************************************************/   
    public static MACYS_CaseFollowUpDays__c createCustomSettingForFollowUpDays(){
        MACYS_CaseFollowUpDays__c followupCustomSetting=new MACYS_CaseFollowUpDays__c();
        followupCustomSetting.name='Critical';
        followupCustomSetting.MACYS_FirstNotification__c=1;
        followupCustomSetting.MACYS_SecondNotification__c=2;
        insert followupCustomSetting;
        return followupCustomSetting;
    }
    

   
/*******************************************************************************************
* Method         :  insertArticles
* Description    :  This test methood is used to insert Article
* Parameter      :  none
* Return Type    :  MACYS_ArticleType__kav
*******************************************************************************************/   
    
    
    public static MACYS_Brochure__kav insertBrochureArticles(){
          MACYS_Brochure__kav article = new MACYS_Brochure__kav(
                                                Title=String.valueOf(system.now().millisecond()),
                                                UrlName  =String.valueOf(system.now().millisecond()),
                                                MACYS_ExpiredDate__c =System.today(),
                                                MACYS_EffectiveDate__c=System.today(),
                                                MACYS_PublishedDate__c=System.today()-365);
            insert article;
            system.debug('article======'+article);
            return article;
    }
    public static MACYS_Brochure__kav updateBrochureArticles(MACYS_Brochure__kav article,Integer noOfDays){
          MACYS_Brochure__kav article1 = [SELECT MACYS_ExpiredDate__c FROM MACYS_Brochure__kav where id= :article.Id];
          if(noOfDays == 0){
              article1.MACYS_ExpiredDate__c = System.today();
          }else if (noOfDays == 30){
              article1.MACYS_ExpiredDate__c = System.today()+30;
          }else if(noOfDays == 60){
              article1.MACYS_ExpiredDate__c = System.today()+60;
          }else if(noOfDays == 90){
              article1.MACYS_ExpiredDate__c = System.today()+90;
          }
            update article1;
            system.debug('article======'+article1);
            system.debug('article======'+article1.Id);
            return article1;
    }
    
    public static Macys_CallCenterGuides__kav insertCCSArticles(){
          Macys_CallCenterGuides__kav article = new Macys_CallCenterGuides__kav(
                                                Title='Unit CCS',
                                                UrlName  ='UnitCCS',
                                                MACYS_ExpiredDate__c =System.today(),
                                                MACYS_EffectiveDate__c=System.today(),
                                                MACYS_PublishedDate__c=System.today()-365);
            insert article;
            system.debug('article======'+article);
            system.debug('article======'+article.Id);
            return article;
    }
   
    public static Macys_CallCenterGuides__kav updateCCSArticles(Macys_CallCenterGuides__kav article,Integer noOfDays){
          Macys_CallCenterGuides__kav article1 = [SELECT MACYS_ExpiredDate__c FROM Macys_CallCenterGuides__kav where id= :article.Id];
          if(noOfDays == 0){
              article1.MACYS_ExpiredDate__c = System.today();
          }else if (noOfDays == 30){
              article1.MACYS_ExpiredDate__c = System.today()+30;
          }else if(noOfDays == 60){
              article1.MACYS_ExpiredDate__c = System.today()+60;
          }else if(noOfDays == 90){
              article1.MACYS_ExpiredDate__c = System.today()+90;
          }
            update article1;
            system.debug('article======'+article1);
            system.debug('article======'+article1.Id);
            return article1;
    }
    
    public static MACYS_DTP__kav insertDTPArticles(){
          MACYS_DTP__kav article = new MACYS_DTP__kav(
                                                Title='Unit DTP',
                                                UrlName  ='UnitDTP',
                                                MACYS_ExpiredDate__c =System.today(),
                                                MACYS_EffectiveDate__c=System.today(),
                                                MACYS_PublishedDate__c=System.today()-365);
            insert article;
            system.debug('article======'+article);
            system.debug('article======'+article.Id);
            return article;
    }
    
    public static MACYS_DTP__kav updateDTPArticles(MACYS_DTP__kav article,Integer noOfDays){
          MACYS_DTP__kav article1 = [SELECT MACYS_ExpiredDate__c FROM MACYS_DTP__kav where id= :article.Id];
          if(noOfDays == 0){
              article1.MACYS_ExpiredDate__c = System.today();
          }else if (noOfDays == 30){
              article1.MACYS_ExpiredDate__c = System.today()+30;
          }else if(noOfDays == 60){
              article1.MACYS_ExpiredDate__c = System.today()+60;
          }else if(noOfDays == 90){
              article1.MACYS_ExpiredDate__c = System.today()+90;
          }
            update article1;
            system.debug('article======'+article1);
            system.debug('article======'+article1.Id);
            return article1;
    }
    
    
    
    public static MACYS_FAQ__kav insertFAQArticles(){
          MACYS_FAQ__kav article = new MACYS_FAQ__kav(
                                                Title='Unit FAQ',
                                                UrlName  ='UnitTest',
                                                MACYS_ExpiredDate__c =System.today(),
                                                MACYS_EffectiveDate__c=System.today(),
                                                MACYS_PublishedDate__c=System.today()-365);
            insert article;
            system.debug('article======'+article);
            return article;
    }
    public static MACYS_FAQ__kav updateFAQArticles(MACYS_FAQ__kav article,Integer noOfDays){
          MACYS_FAQ__kav article1 = [SELECT MACYS_ExpiredDate__c FROM MACYS_FAQ__kav where id= :article.Id];
          if(noOfDays == 0){
              article1.MACYS_ExpiredDate__c = System.today();
          }else if (noOfDays == 30){
              article1.MACYS_ExpiredDate__c = System.today()+30;
          }else if(noOfDays == 60){
              article1.MACYS_ExpiredDate__c = System.today()+60;
          }else if(noOfDays == 90){
              article1.MACYS_ExpiredDate__c = System.today()+90;
          }
            update article1;
            system.debug('article======'+article1);
            system.debug('article======'+article1.Id);
            return article1;
    }
    
    public static MACYS_Form__kav insertFormArticles(){
          MACYS_Form__kav article = new MACYS_Form__kav(
                                                Title='Unit Form',
                                                UrlName  ='UnitForm',
                                                MACYS_ExpiredDate__c =System.today(),
                                                MACYS_EffectiveDate__c=System.today(),
                                                MACYS_PublishedDate__c=System.today()-365);
            insert article;
            system.debug('article======'+article);
            return article;
    }
    
    public static MACYS_Form__kav updateFormArticles(MACYS_Form__kav article,Integer noOfDays){
          MACYS_Form__kav article1 = [SELECT MACYS_ExpiredDate__c FROM MACYS_Form__kav where id= :article.Id];
          if(noOfDays == 0){
              article1.MACYS_ExpiredDate__c = System.today();
          }else if (noOfDays == 30){
              article1.MACYS_ExpiredDate__c = System.today()+30;
          }else if(noOfDays == 60){
              article1.MACYS_ExpiredDate__c = System.today()+60;
          }else if(noOfDays == 90){
              article1.MACYS_ExpiredDate__c = System.today()+90;
          }
            update article1;
            system.debug('article======'+article1);
            system.debug('article======'+article1.Id);
            return article1;
    }
    
    public static MACYS_IOP__kav insertIOPArticles(){
          User use=createUser('4321', 'System Administrator');
          MACYS_IOP__kav article = new MACYS_IOP__kav(
                                                Title='Unit IOP',
                                                UrlName  ='UnitIOP',
                                                MACYS_ExpiredDate__c =System.today(),
                                                MACYS_EffectiveDate__c=System.today(),
                                                MACYS_PublishedDate__c=System.today()-365,
                                                MACYS_ArticleOwner__c=use.id);
            insert article;
            system.debug('article======'+article);
            return article;
    }
    public static MACYS_IOP__kav updateIOPArticles(MACYS_IOP__kav article,Integer noOfDays){
          MACYS_IOP__kav article1 = [SELECT MACYS_ExpiredDate__c FROM MACYS_IOP__kav where id= :article.Id];
          if(noOfDays == 0){
              article1.MACYS_ExpiredDate__c = System.today();
          }else if (noOfDays == 30){
              article1.MACYS_ExpiredDate__c = System.today()+30;
          }else if(noOfDays == 60){
              article1.MACYS_ExpiredDate__c = System.today()+60;
          }else if(noOfDays == 90){
              article1.MACYS_ExpiredDate__c = System.today()+90;
          }
            update article1;
            system.debug('article======'+article1);
            system.debug('article======'+article1.Id);
            return article1;
    }
    
    public static MACYS_JobAidCase__kav insertJobAidArticles(){
          MACYS_JobAidCase__kav article = new MACYS_JobAidCase__kav(
                                                Title='Unit JobAid',
                                                UrlName  ='UnitJobAid',
                                                MACYS_ExpiredDate__c =System.today(),
                                                MACYS_EffectiveDate__c=System.today(),
                                                MACYS_PublishedDate__c=System.today()-365);
            insert article;
            system.debug('article======'+article);
            return article;
    }
    
    public static MACYS_JobAidCase__kav updateJobAidArticles(MACYS_JobAidCase__kav article,Integer noOfDays){
          MACYS_JobAidCase__kav article1 = [SELECT MACYS_ExpiredDate__c FROM MACYS_JobAidCase__kav where id= :article.Id];
          if(noOfDays == 0){
              article1.MACYS_ExpiredDate__c = System.today();
          }else if (noOfDays == 30){
              article1.MACYS_ExpiredDate__c = System.today()+30;
          }else if(noOfDays == 60){
              article1.MACYS_ExpiredDate__c = System.today()+60;
          }else if(noOfDays == 90){
              article1.MACYS_ExpiredDate__c = System.today()+90;
          }
            update article1;
            system.debug('article======'+article1);
            system.debug('article======'+article1.Id);
            return article1;
    }
    
    public static MACYS_OtherReferenceMaterial__kav insertORMArticles(){
          MACYS_OtherReferenceMaterial__kav article = new MACYS_OtherReferenceMaterial__kav(
                                                Title='Unit ORM',
                                                UrlName  ='UnitORM',
                                                MACYS_ExpiredDate__c =System.today(),
                                                MACYS_EffectiveDate__c=System.today(),
                                                MACYS_PublishedDate__c=System.today()-365);
            insert article;
            system.debug('article======'+article);
            return article;
    }
    
    public static MACYS_OtherReferenceMaterial__kav updateORMArticles(MACYS_OtherReferenceMaterial__kav article,Integer noOfDays){
          MACYS_OtherReferenceMaterial__kav article1 = [SELECT MACYS_ExpiredDate__c FROM MACYS_OtherReferenceMaterial__kav where id= :article.Id];
          if(noOfDays == 0){
              article1.MACYS_ExpiredDate__c = System.today();
          }else if (noOfDays == 30){
              article1.MACYS_ExpiredDate__c = System.today()+30;
          }else if(noOfDays == 60){
              article1.MACYS_ExpiredDate__c = System.today()+60;
          }else if(noOfDays == 90){
              article1.MACYS_ExpiredDate__c = System.today()+90;
          }
            update article1;
            system.debug('article======'+article1);
            system.debug('article======'+article1.Id);
            return article1;
    }
    
    public static MACYS_Policy__kav insertPolicyArticles(){
          MACYS_Policy__kav article = new MACYS_Policy__kav(
                                                Title='Unit Policy',
                                                UrlName  ='UnitPolicy',
                                                MACYS_ExpiredDate__c =System.today(),
                                                MACYS_EffectiveDate__c=System.today(),
                                                MACYS_PublishedDate__c=System.today()-365);
            insert article;
            system.debug('article======'+article);
            return article;
    }
    
    public static MACYS_Policy__kav updatePolicyArticles(MACYS_Policy__kav article,Integer noOfDays){
          MACYS_Policy__kav article1 = [SELECT MACYS_ExpiredDate__c FROM MACYS_Policy__kav where id= :article.Id];
          if(noOfDays == 0){
              article1.MACYS_ExpiredDate__c = System.today();
          }else if (noOfDays == 30){
              article1.MACYS_ExpiredDate__c = System.today()+30;
          }else if(noOfDays == 60){
              article1.MACYS_ExpiredDate__c = System.today()+60;
          }else if(noOfDays == 90){
              article1.MACYS_ExpiredDate__c = System.today()+90;
          }
              
            update article1;
            system.debug('article======'+article1);
            system.debug('article======'+article1.Id);
            return article1;
    }

    public static Contact creatingContact(User usertoassociate, String employeeNumber){
        Contact con=new Contact();
        con.lastname='Roy';
        con.Macys_User__c=usertoassociate.id;
        con.MACYS_EmployeeNumber__c=employeeNumber;
        con.MailingCity='washington';
        insert con;
        return con;
    }
}