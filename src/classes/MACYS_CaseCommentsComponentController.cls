/******************************************************************************************
    * Create By     :     Deloitte Consulting.
    * Create Date   :     8/23/2016         
    * Description   :     Class to fetch Case Comments and Displaye them on MACYS_CaseCommentsRelatedList page.

    *     Modification Log:
    *    -----------------------------------------------------------------------------
    *    * Developer                   Date               Description
    *    * ----------------------------------------------------------------------------                 
    *    * Puneet Jalota               8/23/2016           
******************************************************************************************/
public with sharing class MACYS_CaseCommentsComponentController{

    public MACYS_CaseSLAMappingWithPriority__c  custSetObj{get;set;}
    public String newComments{get;set;}
    public ID loggedinUserId{get;set;}

    public MACYS_CaseCommentsComponentController() {
        custSetObj= MACYS_CaseSLAMappingWithPriority__c.getOrgDefaults();
        loggedinUserId=UserInfo.getUserId();
    }

    public Id caseId {get; set;}
    
    public cComments[] comments{
        get{
            List<cComments> comments = new List<cComments>();
            for(CaseComment comment : [Select LastModifiedDate, LastModifiedBy.Id, LastModifiedBy.Name, IsPublished, CreatedDate, CreatedBy.Id, CreatedBy.Name, CommentBody From CaseComment c where ParentId = :caseId and (IsPublished=true or CreatedBy.Id=:loggedinUserId) order by c.LastModifiedDate desc])
            {
                cComments tempcComment = new cComments();
                tempcComment.cComment = comment;

                // Build String to display.
                tempcComment.commentText = '<b>Created By: <a href=\'/' + comment.CreatedBy.Id + '\'>' + comment.CreatedBy.Name + '</a> (' + comment.CreatedDate.format() + ') </b><br> ';
                //tempcComment.commentText += 'Last Modified By: <a href=\'/' + comment.LastModifiedBy.Id + '\'>' + comment.LastModifiedBy.Name + '</a> (' + comment.LastModifiedDate.format() + ')</b><br>';
                tempcComment.commentText += comment.CommentBody;

                if(comment.IsPublished)
                    tempcComment.PublicPrivateAction = 'Make Private';
                else
                    tempcComment.PublicPrivateAction = 'Make Public';
                
                //Add to list
                comments.add(tempcComment); 
            }
            return comments;
        }

        set;
    }

    public PageReference NewComment(){
        system.debug('value entered'+newComments);

        CaseComment newValue=new CaseComment();
        newValue.CommentBody=newComments;
        newValue.ParentId=caseId;
        newValue.IsPublished=true;
    
        try{
            insert newValue;
            
            newComments='';
        }
        Catch(Exception e){
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,''));
        }
        return null;
    }

    public class cComments {

        public CaseComment cComment {get; set;}
        public String commentText {get; set;}
        public String PublicPrivateAction {get; set;}
    }
    
    
}