/******************************************************************************************
    * Create By     :     Deloitte Consulting.
    * Create Date   :     06/09/2016         
    * Description   :     Helper class for BAML Record object trigger.

    *     Modification Log:
    *    -----------------------------------------------------------------------------
    *    * Developer                   Date               Description
    *    * ----------------------------------------------------------------------------                 
    *    * Prem Kumar Gupta           22/09/2016          Initial version.
******************************************************************************************/
public class MACYS_CaseImport{

    
    MACYS_CaseSLAMappingWithPriority__c  custSetObj;
    Map<String,MACYS_BMLRecord__c> mapofISN = new Map<String,MACYS_BMLRecord__c>();
    Map<String,MACYS_BMLRecord__c> mapofISN1 = new Map<String,MACYS_BMLRecord__c>();
    Map<String,MACYS_BMLRecord__c> mapofFoundISN = new Map<String,MACYS_BMLRecord__c>();
    Map<String,MACYS_BMLRecord__c> mapofnotFoundISN = new Map<String,MACYS_BMLRecord__c>();
    List<Contact> listOfFoundISNcontacts = new List<Contact>();
    List<String> isnList = new List<String>();
    Map<String,Id> contactISNMap =  new Map<String,Id>();
    Integer bamlcounter=0;
    Map<MACYS_BMLRecord__c,Integer> mapforCaseandBaml;
    List<Case> listcasesToInsert =  new List<Case>();
    List<Account> listAcc =  new List<Account>();
    List<Contact> lsContactToInsert = new List<Contact>();
    Map<String,Id> dummyContactISNMap;
    MACYS_BMLRecord__c bamlRecordObj;
    
    /*******************************************************************************************
    * Method         :  creatingCase
    * Description    :  This methood is used to create Case records per BAML record inserted with Contact populated as ISN on BAML record.
    * Parameter      :  List of BAML Records 
    * Return Type    :  list of BAML records records
    ******************************************************************************************/

    public void creatingCase(List<MACYS_BMLRecord__c> bamlInserted){
    
        custSetObj= MACYS_CaseSLAMappingWithPriority__c.getOrgDefaults(); 
        mapofFoundISN = new Map<String,MACYS_BMLRecord__c>();
        mapofnotFoundISN = new Map<String,MACYS_BMLRecord__c>();
        mapforCaseandBaml = new Map<MACYS_BMLRecord__c,Integer>();
        listAcc = [select id,name from Account where name like '%MACY%' limit 1];
        
        for(MACYS_BMLRecord__c recs: bamlInserted){
            if(recs.MACYS_ISN__c!=null && recs.MACYS_ISN__c!=''){
                mapofISN.put(recs.MACYS_ISN__c,recs);
                mapforCaseandBaml.put(recs,bamlcounter++);
                 // remember to check for duplicate ISN, if duplicate comes previous will get erase.
            }
        }
        system.debug('----mapofISN key ----- '+mapofISN.keyset());
        system.debug('----mapofISN ----- '+mapofISN);
        system.debug(mapofISN.get('11122211222'));
        system.debug(mapofISN.get('A05577410'));
        if(mapofISN.size()>0){
            listOfFoundISNcontacts=[select Id,MACYS_ISN__c from contact where MACYS_ISN__c in: mapofISN.keyset()];
        }
        system.debug('----listOfFoundISNcontacts---'+listOfFoundISNcontacts);
        
        for(Contact con: listOfFoundISNcontacts ){
            isnList.add(con.MACYS_ISN__c);
            contactISNMap.put(con.MACYS_ISN__c,con.Id);
            mapofnotFoundISN.remove(con.MACYS_ISN__c);
            mapofFoundISN.put(con.MACYS_ISN__c, mapofISN.get(String.valueOf(con.MACYS_ISN__c)));
        }
        for(String isnKey: mapofISN.keyset()){
            if(!mapofFoundISN.containsKey(isnKey)){
                mapofnotFoundISN.put(isnKey,mapofISN.get(isnKey));
            }
        }
        
        system.debug('----mapofnotFoundISN ----- '+mapofnotFoundISN);
        system.debug('----mapofFoundISN ----- '+mapofFoundISN);
        
        for(String key: mapofFoundISN.keyset()){
            system.debug('---For ISN --'+key+'----value is ---'+mapofFoundISN.get(key));
        }
        
        //create case for contact, whose ISN found
        
        for(String isnKey: mapofFoundISN.keyset()){
            
            bamlRecordObj = new MACYS_BMLRecord__c();
            bamlRecordObj = mapofFoundISN.get(isnKey);
            
            createCase(contactISNMap,bamlRecordObj,isnKey,custSetObj,mapofFoundISN);
              
        }
        
         //create case for contact, whose ISN not found
         
         for(String isnKey: mapofnotFoundISN.keyset()){
         
            bamlRecordObj = new MACYS_BMLRecord__c();
            bamlRecordObj = mapofnotFoundISN.get(isnKey);
            
            if(listAcc.size()>0){
                Contact con = new Contact(FirstName='Sample',LastName=isnKey,MACYS_CreateMode__c = 'Case Import',
                    MACYS_EmployeeNumber__c = isnKey,MACYS_ISN__c = isnKey,AccountId = listAcc[0].Id);
                lsContactToInsert.add(con);
            }
          }
            
            try{
                if(lsContactToInsert.size()>0)
                    insert lsContactToInsert;
            }catch(Exception e){
                system.debug('---Error in creating Contact----'+e);
            }
            dummyContactISNMap = new Map<String,Id>();
            for(Contact c: lsContactToInsert){
                dummyContactISNMap.put(c.MACYS_ISN__c,c.Id);
            
            }
            system.debug('---dummyContactISNMap---'+dummyContactISNMap);
            for(String isnKey: mapofnotFoundISN.keyset()){
                createCase(dummyContactISNMap,bamlRecordObj,isnKey,custSetObj,mapofnotFoundISN);
            }
            for(Case c : listcasesToInsert){
                system.debug('---case created ---'+c);
            }
            
            delete lsContactToInsert;
        
         
         
   
    }
    
    public void createCase(Map<String,Id> contactISNMap,MACYS_BMLRecord__c bamlRecordObj,String isnKey,MACYS_CaseSLAMappingWithPriority__c  custSetObj,Map<String,MACYS_BMLRecord__c> mapofISN){
        
        Database.DMLOptions dmo = new Database.DMLOptions();
        dmo.assignmentRuleHeader.useDefaultRule = true;
            
        Case caseinsta=new Case();
        caseinsta.contactid = contactISNMap.get(isnKey); 
        caseinsta.description = bamlRecordObj.MACYS_Description__c ;// check for :: senario too
        system.debug('--mapforCaseandBaml--'+mapforCaseandBaml);
        system.debug('--mapofISN--'+mapofISN);
        system.debug('--isnKey--'+isnKey);
        if(mapforCaseandBaml.get(mapofISN.get(isnKey)) != null)
            caseinsta.description +='::'+ mapforCaseandBaml.get(mapofISN.get(isnKey));    
        caseinsta.MACYS_CaseCategory__c = bamlRecordObj.MACYS_CaseCategory__c;
        caseinsta.MACYS_CaseSubCategory1__c = bamlRecordObj.MACYS_CaseSubCategory1__c;
        caseinsta.MACYS_CaseSubCategory2__c = bamlRecordObj.MACYS_CaseSubCategory2__c;
        caseinsta.Subject = bamlRecordObj.MACYS_Subject__c;
        caseinsta.Origin ='Case Import';
        caseinsta.recordtypeid=custSetObj.MACYS_CaseGeneralInquiryID__c;
        if(mapofISN.get(isnKey).MACYS_CaseClosedStatus__c == true){
            caseinsta.Status ='Closed';
            caseinsta.MACYS_CaseResolutionNotes__c = 'Closing Case';
        }
        caseinsta.setOptions(dmo);
        listcasesToInsert.add(caseinsta);
       
    }


}