/******************************************************************************************
    * Create By     :     Deloitte Consulting.
    * Create Date   :     8/23/2016         
    * Description   :     Class to fetch Contact ID associated to the loggedIn User.

    *     Modification Log:
    *    -----------------------------------------------------------------------------
    *    * Developer                   Date               Description
    *    * ----------------------------------------------------------------------------                 
    *    * Puneet Jalota               8/23/2016           
******************************************************************************************/
global with sharing class MACYS_ContactAssociatedToUser{

    public static List<User> userlst;
    public static ID conid;
    public static ID userid;
    
/*******************************************************************************************
    * Method         :  contactID
    * Description    :  This test methood is used to get Contact ID associated with loggedin User.
    * Parameter      :  None
    * Return Type    :  ID
*******************************************************************************************/
    
    @RemoteAction
    global static ID contactID() {
        userid=Userinfo.getUserId();
        userlst=[select id ,(select id,name from contacts__r) from User where id=:userid];
        system.debug('ascas'+userlst+userid);
        
        for(User userrec: userlst){
            if(!userrec.contacts__r.isEmpty()){
                for(Contact con:userrec.contacts__r){
                    conid=con.id;
                    system.debug('qsqsc'+conid);
                }
            }
        }
        
        
        return conid;
    }
}