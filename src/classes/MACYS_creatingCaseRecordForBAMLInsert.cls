/******************************************************************************************
    * Create By     :     Deloitte Consulting.
    * Create Date   :     06/09/2016         
    * Description   :     Helper class for BAML Record object trigger.

    *     Modification Log:
    *    -----------------------------------------------------------------------------
    *    * Developer                   Date               Description
    *    * ----------------------------------------------------------------------------                 
    *    * Puneet Jalota             06/09/2016          Initial version.
******************************************************************************************/
public class MACYS_creatingCaseRecordForBAMLInsert{

    List<Case> listcasesToInsert;
    List<MACYS_BMLRecord__c> listcaseNotCreatedForThese;// BAML records for which Case is not created.  
    List<MACYS_BMLRecord__c> listcaseCreatedForThese;  
    map<String, List<MACYS_BMLRecord__c>> mapcontactNameWithBAMLList;
    MACYS_CaseSLAMappingWithPriority__c  custSetObj;
    List<Contact> listcontacts;
    boolean flag=false;
    Map<MACYS_BMLRecord__c,Integer> mapforCaseandBaml = new Map<MACYS_BMLRecord__c,Integer>();
    Integer bamlcounter=0;
    List<Account> listAcc =  new List<Account>();
    Map<String, Contact> contactproxyISNMap = new Map<String, Contact>();
    Map<String, Contact> contactISNMap = new Map<String, Contact>();
    
    
/*******************************************************************************************
* Method         :  creatingCase
* Description    :  This methood is used to create Case records per BAML record inserted with Contact populated as ISN on BAML record.
* Parameter      :  List of BAML Records 
* Return Type    :  list of BAML records records
******************************************************************************************/

    public List<Id> creatingCase(List<MACYS_BMLRecord__c> bamlInserted){
    
        custSetObj= MACYS_CaseSLAMappingWithPriority__c.getOrgDefaults(); 
        mapcontactNameWithBAMLList=new map<String, List<MACYS_BMLRecord__c>>();    // map holding Contact ISN corresponding to list of BML Records.
        listcaseNotCreatedForThese=new List<MACYS_BMLRecord__c>();      // BAML records for which Case is not created.  
        listcaseCreatedForThese=new List<MACYS_BMLRecord__c>();         // BAML records for which Case is created.
        
        List<String> listIsnTotal = new List<String>();
        List<String> listIsnFoundinContact = new List<String>();
        Set<String> listIsnNotExistInContact = new Set<String>();
        List<MACYS_BMLRecord__c> listBAMLwithproxyISN = new List<MACYS_BMLRecord__c>();
        listAcc = [select id,name from Account where name like '%MACY%' limit 1];
        
        for(MACYS_BMLRecord__c recs: bamlInserted){
            if(recs.MACYS_ISN__c!=null && recs.MACYS_ISN__c!=''){
                String empId=recs.MACYS_ISN__c;
                
                if(mapcontactNameWithBAMLList.containsKey(empId)){
                    List<MACYS_BMLRecord__c> bamlRecs=mapcontactNameWithBAMLList.get(empId);
                    bamlRecs.add(recs);
                    mapcontactNameWithBAMLList.put(empId,bamlRecs);
                }
                else{
                    List<MACYS_BMLRecord__c> bamlRecs=new List<MACYS_BMLRecord__c>();
                    bamlRecs.add(recs);
                    mapcontactNameWithBAMLList.put(empId,bamlRecs);
                }
                mapforCaseandBaml.put(recs,bamlcounter++);
                listIsnTotal.add(empId);
            }
        }
        
        // mapcontactNameWithBAMLList contain all {Id,BML Record} from csv file.
        
        listcontacts=[select id,name,MACYS_ISN__c from contact where MACYS_ISN__c in: mapcontactNameWithBAMLList.keyset()];
        listcasesToInsert=new List<Case>();
        //List<Note> notesToInsert=new List<Note>();
        
        
        for(Contact c:listcontacts){
            if(c.MACYS_ISN__c!=null){
                listIsnFoundinContact.add(c.MACYS_ISN__c);
                contactISNMap.put(c.MACYS_ISN__c,c);
            }
        }
        system.debug('listIsnTotal---'+listIsnTotal);
        system.debug('listIsnFoundinContact---'+listIsnFoundinContact);
        
        listIsnNotExistInContact =  findDifferent(listIsnTotal,listIsnFoundinContact);
        
        Set<string> setIsnNotInContact = new Set<string>(); 
        for(String isn:listIsnNotExistInContact){ 
            setIsnNotInContact.add(isn);    // set contains those ISN which contact doesn't exist.
        } 
        for(MACYS_BMLRecord__c bamlrecs : bamlInserted)
        {
            if(setIsnNotInContact.contains(bamlrecs.MACYS_ISN__c))
            {
                listBAMLwithproxyISN.add(bamlrecs);
            }
        }
        system.debug('listBAMLwithproxyISN---------'+listBAMLwithproxyISN);
        List<contact> lsContact = [select id,MACYS_ISN__c from contact where MACYS_ISN__c in: setIsnNotInContact];
       
        for(Contact c: lsContact){
            if(c.MACYS_ISN__c!=null){
                contactproxyISNMap.put(c.MACYS_ISN__c,c);
            }
        }
        // Create a dummy Case when ISN doesn't exist in Salesforce when compared from the csv file
        
        
        //code to insert if contact not found
        populateCaseWithProxyContact(listIsnNotExistInContact,listBAMLwithproxyISN,lsContact);
        
        
        
        system.debug('--mapcontactNameWithBAMLList--'+mapcontactNameWithBAMLList);
        system.debug('--listcontacts--'+listcontacts);
        system.debug('--contactISNMap--'+contactISNMap);
        // code to insert if contact found
        populateCase(mapcontactNameWithBAMLList,listcontacts);
        
        
        List<Id> caseIds = new List<Id>{}; 
        Database.SaveResult[] srList;
        List<Case> lstSuccessCase= new List<Case>();
        
        if(!listcasesToInsert.isEmpty()){
            srList = Database.insert(listcasesToInsert, false);
            for(Database.SaveResult sr : srList)
            {
                  if (sr.isSuccess()) 
                  {
                     system.debug('=====inserted====='+sr.getId());
                     system.debug('=====object======='+sr);
                     caseIds.add(sr.getId());
                     
                  }else {
                      system.debug('----Error in inserting Case----');
                      
                  }
             }
             // logic added on 24/8 for modifying Case import records after case insert
             system.debug('======caseIds======'+caseIds);
             lstSuccessCase = [SELECT casenumber, description FROM Case WHERE id IN:caseIds];
             map<String, Id> mapCaseIdCnt = new map<String, Id>();
             for(Case c:lstSuccessCase){
                 if(c.description!=null && c.description.contains('::')){
                     String desp = c.description;
                     String caseIdentifier = desp.substring(desp.indexOf('::')+2,desp.length());
                     mapCaseIdCnt.put(caseIdentifier, c.Id);
                     
                  }
             }
             
             List<MACYS_BMLRecord__c> caseimportList = trigger.new;
             for(MACYS_BMLRecord__c recs: caseimportList){
                 if(mapforCaseandBaml.get(recs)!=null){ 
                     //recs.MACYS_Comments__c+= '; Case Created:'+c.casenumber;
                     if(mapCaseIdCnt != null && mapCaseIdCnt.containskey(String.valueOf(mapforCaseandBaml.get(recs))))
                     {
                         recs.MACYS_CaseOnBaml__c = mapCaseIdCnt.get(String.valueOf(mapforCaseandBaml.get(recs)));
                         recs.MACYS_CaseCreated__c=true;
                     }
                 }
             
             }
             
            List<MACYS_BMLRecord__c> bamlRecsToUpdate=new List<MACYS_BMLRecord__c>();
        
            if(!listcaseCreatedForThese.isEmpty()){
                for(MACYS_BMLRecord__c bamlrecs: listcaseCreatedForThese){
                    bamlrecs.MACYS_ImportSuccess__c=true;
                    bamlRecsToUpdate.add(bamlrecs);
                }
            }
            
            if(!listcaseNotCreatedForThese.isEmpty()){
                for(MACYS_BMLRecord__c bamlrecs:listcaseNotCreatedForThese){
                    bamlRecsToUpdate.add(bamlrecs);
                }
            }
             
             //truncate the description since Case Import update done
             for(Case c:lstSuccessCase){
                if(c.description!=null){
                    c.description = c.description.substring(0,c.description.indexOf('::'));
                }
             }
             try{
                 if(lstSuccessCase.size()>0)
                     update lstSuccessCase;
             }catch(Exception e){
                 system.debug('-----update lstSuccessCase----'+lstSuccessCase);
             }
             
        }
   
      
        return caseIds ; //bamlRecsToUpdate
    }
    
    private void populateCase(map<String, List<MACYS_BMLRecord__c>> mapcontactNameWithBAMLList,List<Contact> listcontacts)
    {
        if(!mapcontactNameWithBAMLList.isEmpty()){
        
            for(String empId : mapcontactNameWithBAMLList.keyset()){
            
                for(MACYS_BMLRecord__c bamlrecs : mapcontactNameWithBAMLList.get(empId)){
                
                    for(Contact con : listcontacts){
                        
                        
                        if(con.MACYS_ISN__c ==empId){
                        
                            Database.DMLOptions dmo = new Database.DMLOptions();
                            dmo.assignmentRuleHeader.useDefaultRule = true;
                            Case caseinsta=new Case();
                            caseinsta.contactid=con.id;
                            
                            if(bamlrecs.MACYS_Description__c != null)
                                caseinsta.description=bamlrecs.MACYS_Description__c;
                            
                            if(mapforCaseandBaml.get(bamlrecs) != null)
                                caseinsta.description+='::'+ mapforCaseandBaml.get(bamlrecs);  
                                 
                            if(bamlrecs.MACYS_CaseCategory__c != null)
                                caseinsta.MACYS_CaseCategory__c =bamlrecs.MACYS_CaseCategory__c;
                                
                            if(bamlrecs.MACYS_CaseSubCategory1__c != null)
                                caseinsta.MACYS_CaseSubCategory1__c =bamlrecs.MACYS_CaseSubCategory1__c;
                                
                            if(bamlrecs.MACYS_CaseSubCategory2__c != null)
                                caseinsta.MACYS_CaseSubCategory2__c =bamlrecs.MACYS_CaseSubCategory2__c;
                                
                            if(bamlrecs.MACYS_Subject__c != null)
                                caseinsta.Subject =bamlrecs.MACYS_Subject__c;
                                
                            caseinsta.Origin ='Case Import';
                            
                            caseinsta.recordtypeid=custSetObj.MACYS_CaseGeneralInquiryID__c;
                            
                            if(bamlrecs.MACYS_CaseClosedStatus__c == true){
                                caseinsta.Status ='Closed';
                                caseinsta.MACYS_CaseResolutionNotes__c = 'Closing Case';
                            }
                            caseinsta.setOptions(dmo);
                            listcasesToInsert.add(caseinsta);
                            listcaseCreatedForThese.add(bamlrecs);
                            
                            system.debug('======listcasesToInsert====='+listcasesToInsert);
                            
                        }
                        else{
                            listcaseNotCreatedForThese.add(bamlrecs);
                        }
                        
                    }
                    
                }
            }
        }
    }
    
    
    private void populateCaseWithProxyContact(Set<String> listIsnNotExistInContact,List<MACYS_BMLRecord__c> listBAMLwithproxyISN,List<contact> lsContact){
       List<Contact> lsContactToInsert = new List<Contact>();
       Map<String,Id> mapOfContact;
       
       
        if(listIsnNotExistInContact.size()>0){
        
            for(MACYS_BMLRecord__c bamlrecs : listBAMLwithproxyISN){
                flag=false;
                for(Contact c:lsContact){
                    if(c.MACYS_ISN__c == bamlrecs.MACYS_ISN__c)
                    {
                      flag = true;
                    }
                }
                if(flag == false){
                    
                    system.debug('--------listAcc--------'+listAcc);
                    if(listAcc.size()>0){
                        Contact con = new Contact(FirstName='Sample',LastName=bamlrecs.MACYS_ISN__c,MACYS_CreateMode__c = 'Case Import',MACYS_EmployeeNumber__c = bamlrecs.MACYS_ISN__c,MACYS_ISN__c = bamlrecs.MACYS_ISN__c,AccountId = listAcc[0].Id);
                        lsContactToInsert.add(con);
                        //insert con;
                        
                    }
                }
                
            }
            system.debug('---lsContactToInsert ---'+lsContactToInsert);
            try{
                if(lsContactToInsert.size()>0)
                    insert lsContactToInsert;
            }catch(Exception e)
            {
                system.debug('Error in creating Contact----'+e);
            }
            mapOfContact = new Map<String,Id>();
            for(Contact c: lsContactToInsert){
                mapOfContact.put(c.LastName,c.Id);
            
            }
            
            
            
            for(MACYS_BMLRecord__c bamlrecs : listBAMLwithproxyISN){
                Database.DMLOptions dmo = new Database.DMLOptions();
                dmo.assignmentRuleHeader.useDefaultRule = true;
                
                Case caseinsta=new Case();
                flag=false;
                for(Contact c:lsContact){
                    if(c.MACYS_ISN__c == bamlrecs.MACYS_ISN__c)
                    {
                       caseinsta.contactid=c.id;
                       flag = true;
                    }
                }
                if(flag == false){
                    
                    system.debug('--------listAcc--------'+listAcc);
                    if(listAcc.size()>0){
                        //Contact con = new Contact(FirstName='Sample',LastName=bamlrecs.MACYS_ISN__c,MACYS_CreateMode__c = 'Case Import',MACYS_EmployeeNumber__c = bamlrecs.MACYS_ISN__c,MACYS_ISN__c = bamlrecs.MACYS_ISN__c,AccountId = listAcc[0].Id);
                        // lsContact.add(con);
                        //insert con;
                        //caseinsta.contactid=con.id;
                        system.debug('mapOfContact.get(bamlrecs.MACYS_ISN__c)----'+mapOfContact.get(bamlrecs.MACYS_ISN__c));
                        if(mapOfContact.size()>0 && bamlrecs.MACYS_ISN__c != null)
                             caseinsta.contactid = mapOfContact.get(bamlrecs.MACYS_ISN__c);
                    }
                    
                }
                caseinsta.Origin ='Case Import';
                caseinsta.recordtypeid=custSetObj.MACYS_CaseGeneralInquiryID__c;
                if(bamlrecs.MACYS_Description__c != null)
                    caseinsta.description=bamlrecs.MACYS_Description__c;
                system.debug('-----mapforCaseandBaml.get(bamlrecs)-------'+mapforCaseandBaml.get(bamlrecs));
                if(mapforCaseandBaml.get(bamlrecs) != null)
                    caseinsta.description+='::'+ mapforCaseandBaml.get(bamlrecs);    
                if(bamlrecs.MACYS_CaseCategory__c != null)
                    caseinsta.MACYS_CaseCategory__c =bamlrecs.MACYS_CaseCategory__c;
                if(bamlrecs.MACYS_CaseSubCategory1__c != null)
                    caseinsta.MACYS_CaseSubCategory1__c =bamlrecs.MACYS_CaseSubCategory1__c;
                if(bamlrecs.MACYS_CaseSubCategory2__c != null)
                    caseinsta.MACYS_CaseSubCategory2__c =bamlrecs.MACYS_CaseSubCategory2__c;
                if(bamlrecs.MACYS_Subject__c != null)
                    caseinsta.Subject =bamlrecs.MACYS_Subject__c;
                
                if(bamlrecs.MACYS_CaseClosedStatus__c == true){
                    caseinsta.Status ='Closed';
                    caseinsta.MACYS_CaseResolutionNotes__c = 'Closing Case';
                }
                caseinsta.setOptions(dmo);
                listcasesToInsert.add(caseinsta);
                //listcaseCreatedForThese.add(bamlrecs);
                
                system.debug('=======listcasesToInsert with ISN====='+listcasesToInsert);
            
            }
        }
    
    }
    
    private Set<String> findDifferent(List<String> list1, List<String> list2)
    {
         Set<String> differences = new Set<String>();
         differences.addAll(list1);
         for(String item : list2) //This assumes list2 doesn't not contain dupes, if it might then first convert it to a set
         {
            if( !differences.add(item) ) //if add returns false it means the set did NOT change, which means is already contains the item
            {
                 differences.remove(item); //if the item was already there then remove it, it's not different...
            }
         }
    
         return differences;
    }
}